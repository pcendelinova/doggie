package cz.mendelu.xcendel1.dogapp.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.adapters.ViewPagerAdapter;
import cz.mendelu.xcendel1.dogapp.fragments.DogDiaryFragment;
import cz.mendelu.xcendel1.dogapp.fragments.DogFriendsFragment;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.utils.DateUtils;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import cz.mendelu.xcendel1.dogapp.utils.UserUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOGS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOG_STATUS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_FRIENDSHIPS_REQUESTS;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_EDIT;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_MY_PROFILE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_DOG;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_DOG_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_DOG_FRIENDS;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOGS_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOG_FRIENDS_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_MEMORIES_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.UPDATED_DOG;

/**
 * A screen that includes information about the user's current myDog
 */
public class MyDogProfileActivity extends AppCompatActivity {
    private static final String TAG = MyDogProfileActivity.class.getSimpleName();

    @BindView(R.id.dog_photo)
    CircleImageView dogPhoto;
    @BindView(R.id.dog_name)
    TextView dogName;
    @BindView(R.id.dog_breed)
    TextView dogBreed;
    @BindView(R.id.hobbies)
    TextView hobbies;
    @BindView(R.id.weight)
    TextView weight;
    @BindView(R.id.age)
    TextView age;
    @BindView(R.id.gender)
    ImageView gender;

    @BindView(R.id.btt_status)
    RelativeLayout tvStatus;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    private static Dog myDog;
    private DogDiaryFragment dogDiaryFragment;
    private DogFriendsFragment dogFriendsFragment;
    private static final int EDIT_DOG_REQUEST_CODE = 113;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_dog_profile);
        ButterKnife.bind(this);

        if (getIntent().hasExtra(PARCEL_DOG)) {
            Bundle data = getIntent().getExtras();
            myDog = data.getParcelable(PARCEL_DOG);
            setTitle(getString(R.string.my_dog) + " " + myDog.getName());
        }

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupData(false);

    }

    /**
     * Setups view pager
     * @param viewPager
     */
    private void setupViewPager(final ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        if (dogDiaryFragment == null) {
            dogDiaryFragment = DogDiaryFragment.newInstance(myDog.getId(), myDog.getUserId(), PARAM_MY_PROFILE);
        }
        if (dogFriendsFragment == null) {
            dogFriendsFragment = DogFriendsFragment.newInstance(myDog.getId(), PARAM_MY_PROFILE, myDog.getName());
        }


        adapter.addFragment(dogDiaryFragment, getString(R.string.dog_diary_tab));
        adapter.addFragment(dogFriendsFragment, getString(R.string.dog_friends_tab));
        viewPager.setAdapter(adapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    /**
     * Setups data for dog profile
     *
     * @param invalidate - if the dog has been edited, the photo has to be invalidate because of Picasso
     */
    private void setupData(boolean invalidate) {
        dogName.setText(myDog.getName());
        if (!myDog.getBreed().equals("")) {
            dogBreed.setVisibility(View.VISIBLE);
            dogBreed.setText(myDog.getBreed());
        }

        if (!myDog.getHobbies().equals("")) {
            hobbies.setVisibility(View.VISIBLE);
            hobbies.setText(myDog.getHobbies());
        }

        if (!myDog.getWeight().equals("")) {
            weight.setText(myDog.getWeight() + " kg");
        }

        if (!myDog.getStatus().equals("")) {
            ((TextView) tvStatus.getChildAt(1)).setText(myDog.getStatus());
        }

        try {
            age.setText(DateUtils.getDogAge(myDog.getBirthday()));
        } catch (ParseException e) {
            Log.e(TAG, "Error during parsing myDog age" + e);
        }

        if (myDog.isGender()) {
            gender.setImageResource(R.drawable.ic_gender_female);
        } else {
            gender.setImageResource(R.drawable.ic_gender_male);
        }

        String userId = ((FirebaseApplication) getApplication()).getFirebaseUserId();
        String imagePath = userId + STORAGE_IMAGES_DOGS_PATH + myDog.getId();
        ImageUtils.setImageView(MyDogProfileActivity.this, imagePath, myDog.getPhoto(), dogPhoto, invalidate, SET_DOG_PHOTO);

    }

    @OnClick(R.id.btt_status)
    public void showStatusDialog() {
        createStatusDialog();
    }

    @OnClick(R.id.status)
    public void showStatusDialogFromTextView() {
        createStatusDialog();
    }

    @OnClick(R.id.ic_status)
    public void showStatusDialogFromImageView() {
        createStatusDialog();
    }



    /**
     * Creates status dialog related to the myDog profile
     */
    public void createStatusDialog() {
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.status_tip_item, R.id.status_tip, getResources().getStringArray(R.array.tips_status));

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.status_popup, null);

        final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) view.findViewById(R.id.input_status);
        autoCompleteTextView.setAdapter(adapter);

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setView(view)
                .setCancelable(true)
                .setTitle(R.string.status)
                .setPositiveButton(R.string.publish_status, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (!autoCompleteTextView.getText().toString().equals("")) {
                            ((TextView) tvStatus.getChildAt(1)).setText(autoCompleteTextView.getText());
                        } else {
                            ((TextView) tvStatus.getChildAt(1)).setText(autoCompleteTextView.getText().toString());
                        }

                        ((FirebaseApplication) getApplication()).saveOrEditSingleValue(((TextView) tvStatus.getChildAt(1)).getText().toString(), getDogId(),
                                FIREBASE_CHILD_DOGS, FIREBASE_CHILD_DOG_STATUS);

                    }
                })
                .setNegativeButton(android.R.string.cancel, null);
        builder.create().show();
    }

    public static String getDogId() {
        return myDog.getId();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.menu_more, menu);

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().setTitle(getString(R.string.my_dog) + " " + myDog.getName());
        Log.d(TAG, "onResume");
        viewPager.getCurrentItem();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_more) {
            View menuItemView = findViewById(R.id.action_more);
            PopupMenu popupMenu = DialogHelper.createPopupMenu(this, menuItemView, R.menu.popup_my_profile_menu);

            popupMenu.show();
            registerPopupActions(popupMenu);
            return true;
        }

        return false;
    }

    /**
     * Register popup actions
     * @param popupMenu
     */
    private void registerPopupActions(PopupMenu popupMenu) {
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_remove:
                        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                removeDogCascade();
                            }
                        };
                        DialogHelper.showQuestionDialog(MyDogProfileActivity.this, getString(R.string.remove_dog_title), getString(R.string.remove_dog), onClickListener);
                        return true;
                    case R.id.action_edit:
                        Intent intent = new Intent(MyDogProfileActivity.this, NewDogActivity.class);
                        intent.putExtra(PARAM_EDIT, PARAM_EDIT);
                        intent.putExtra(PARCEL_DOG, myDog);
                        startActivityForResult(intent, EDIT_DOG_REQUEST_CODE);
                        return true;
                    case R.id.action_friendships:
                        Intent intent1 = new Intent(MyDogProfileActivity.this, FriendshipRequestActivity.class);
                        intent1.putExtra(PARAM_DOG_ID, myDog.getId());
                        startActivity(intent1);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    /**
     * Removes a dog, its memories, friendship requests and friendships from the external storage and database
     *
     * @return
     */
    private void removeDogCascade() {
        final List<String> friendshipRequests = new ArrayList<>();
        final List<String> dogfriendIds = dogFriendsFragment.getDogFriendIds();
        final List<String> memoriesIds = dogDiaryFragment.getDogMemoriesIds();
        //removing dog friends
        final SharedPreferences.Editor editor = UserUtils.getSharedPreferencesEditor(MyDogProfileActivity.this);
        editor.remove(SHARED_PREFERENCES_DOG_FRIENDS + myDog.getId());

        final ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String friendRequest = postSnapshot.getKey();

                    if (friendRequest.contains(myDog.getId())) {
                        friendshipRequests.add(friendRequest);
                    }
                }
                //removing from database
                ((FirebaseApplication) getApplication()).removeDogCascade(MyDogProfileActivity.this, myDog.getId(), dogfriendIds, friendshipRequests);
                //removing memories
                for (String memoryId : memoriesIds) {
                    ImageUtils.removeImageFromExternalStorage(MyDogProfileActivity.this, STORAGE_IMAGES_MEMORIES_PATH, memoryId + ".jpg");
                }
                //removing dog photo
                ImageUtils.removeImageFromExternalStorage(MyDogProfileActivity.this, STORAGE_IMAGES_DOGS_PATH, myDog.getId() + ".jpg");

                //removing itself from friendship
                ImageUtils.removeImageFromExternalStorage(MyDogProfileActivity.this, STORAGE_IMAGES_DOG_FRIENDS_PATH, myDog.getId() + ".jpg");

                dogFriendsFragment.setDogFriendIds(Collections.<String>emptyList());
                Set<String> myDogIds = UserUtils.getMyDogs(MyDogProfileActivity.this, myDog.getUserId());
                myDogIds.remove(myDog);
                UserUtils.saveMyDogs(MyDogProfileActivity.this, myDog.getUserId(), myDogIds);

                startActivity(new Intent(MyDogProfileActivity.this, MainActivity.class));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        ((FirebaseApplication) getApplication()).getFirebaseDatabase().child(FIREBASE_CHILD_FRIENDSHIPS_REQUESTS).addListenerForSingleValueEvent(valueEventListener);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_DOG_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data.hasExtra(UPDATED_DOG)) {
                if (data.getParcelableExtra(UPDATED_DOG) != null) {
                    myDog = data.getParcelableExtra(UPDATED_DOG);
                    setupData(true);
                }

            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");

    }
}
