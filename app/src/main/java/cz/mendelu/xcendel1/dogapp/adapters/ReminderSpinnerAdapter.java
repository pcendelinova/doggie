package cz.mendelu.xcendel1.dogapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.models.ReminderCategoryEnum;

/**
 * Created by Petra Cendelinova on 10/30/2016.
 *
 * It is an adapter for a new reminder category with images
 */

public class ReminderSpinnerAdapter extends ArrayAdapter<ReminderCategoryEnum> {

    LayoutInflater inflater;
    List<ReminderCategoryEnum> list;
    int resourceString = Integer.MIN_VALUE;
    int resourceImage = Integer.MIN_VALUE;

    public ReminderSpinnerAdapter(Context context, int resourceString, List<ReminderCategoryEnum> categoryEnumList) {
        super(context, resourceString, categoryEnumList);
        this.resourceString = resourceString;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = categoryEnumList;
    }
    public ReminderSpinnerAdapter(Context context, int resourceString, List<ReminderCategoryEnum> categoryEnumList, int resourceImage) {
        super(context, resourceString, categoryEnumList);
        this.resourceString = resourceString;
        this.resourceImage = resourceImage;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = categoryEnumList;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       return getItemView(position, convertView, parent);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getItemView(position, convertView, parent);
    }

    private View getItemView(int position, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(R.layout.reminder_spinner_item, parent, false);
        ImageView imageView = ButterKnife.findById(itemView, R.id.icon);
        TextView textView = ButterKnife.findById(itemView, R.id.category_name);
        if (resourceString != Integer.MIN_VALUE && resourceImage != Integer.MIN_VALUE) {
            imageView.setImageResource(resourceImage);
            textView.setText(getContext().getResources().getText(resourceString));
            resourceString = Integer.MIN_VALUE;
            resourceImage = Integer.MIN_VALUE;
        } else {
            imageView.setImageResource(list.get(position).getIcon());
            textView.setText(getContext().getResources().getText(list.get(position).getCategoryName()));
        }


        return itemView;
    }

}
