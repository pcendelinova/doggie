package cz.mendelu.xcendel1.dogapp.models;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Petra Cendelinova on 10/29/2016.
 *
 * Dog breed model
 */

public class DogBreed implements Parcelable {
    private String id, name, description, character, care, type, usage, age, weight, photoLink;

    public DogBreed(){}

    protected DogBreed(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        character = in.readString();
        care = in.readString();
        type = in.readString();
        usage = in.readString();
        age = in.readString();
        weight = in.readString();
        photoLink = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static final Creator<DogBreed> CREATOR = new Creator<DogBreed>() {
        @Override
        public DogBreed createFromParcel(Parcel in) {
            return new DogBreed(in);
        }

        @Override
        public DogBreed[] newArray(int size) {
            return new DogBreed[size];
        }
    };

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getCare() {
        return care;
    }

    public void setCare(String care) {
        this.care = care;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getPhotoLink() {
        return photoLink;
    }

    public void setPhotoLink(String photoLink) {
        this.photoLink = photoLink;
    }

    @Override
    public String toString() {
        return "DogBreed{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", character='" + character + '\'' +
                ", care='" + care + '\'' +
                ", type='" + type + '\'' +
                ", usage='" + usage + '\'' +
                ", age='" + age + '\'' +
                ", weight='" + weight + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(character);
        parcel.writeString(care);
        parcel.writeString(type);
        parcel.writeString(usage);
        parcel.writeString(age);
        parcel.writeString(weight);
        parcel.writeString(photoLink);
    }

}
