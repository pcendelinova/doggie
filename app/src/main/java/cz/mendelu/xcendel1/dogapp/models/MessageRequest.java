package cz.mendelu.xcendel1.dogapp.models;

/**
 * Created by admin on 18.12.2016.
 */

public class MessageRequest {
    String to;
    Data data;

    public MessageRequest(String receiverId, String senderId, String dogId, String title, String message) {
        this.to = "/topics/" + receiverId;
        this.data = new Data(receiverId, senderId, dogId, title, message);
    }


    public class Data {
        String receiverId;
        String senderId;
        String dogId;
        String title;
        String message;

        public String getDogId() {
            return dogId;
        }

        public void setDogId(String dogId) {
            this.dogId = dogId;
        }

        public Data(String receiverId, String senderId, String dogId, String title, String message) {
            this.receiverId = receiverId;
            this.message = message;
            this.title = title;
            this.senderId = senderId;
            this.dogId = dogId;
        }

        public String getReceiverId() {
            return receiverId;
        }

        public void setReceiverId(String receiverId) {
            this.receiverId = receiverId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getSenderId() {
            return senderId;
        }

        public void setSenderId(String senderId) {
            this.senderId = senderId;
        }
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
