//package cz.mendelu.xcendel1.dogapp.models;
//
//import java.util.Date;
//
///**
// * Created by admin on 08.11.2016.
// */
//
//public class Message {
//    private String senderId, text;
//    private long timestamp;
//
//    public Message() {
//    }
//
//    public Message(String senderId, String text) {
//        this.senderId = senderId;
//        this.text = text;
//        timestamp = new Date().getTime();
//    }
//
//    public String getSenderId() {
//        return senderId;
//    }
//
//    public void setSenderId(String senderId) {
//        this.senderId = senderId;
//    }
//
//    public long getTimestamp() {
//        return timestamp;
//    }
//
//    public void setTimestamp(long timestamp) {
//        this.timestamp = timestamp;
//    }
//
//    public String getText() {
//        return text;
//    }
//
//    public void setText(String text) {
//        this.text = text;
//    }
//}
