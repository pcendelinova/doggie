package cz.mendelu.xcendel1.dogapp.constants;

/**
 * Created by admin on 12.12.2016.
 */

public class SettingsConstants {
    public static final String DATE_FORMAT_DMY = "dd. MM. yyyy";
    public static final String DATE_TIME_FORMAT = "dd. MM. yyyy HH:mm";
    public static final String FORMAT_TIME = "HH:mm";
    public static final int SPLASH_DISPLAY_LENGTH = 1000;
    public static final String CUSTOM_FONT = "Pacifico.ttf";
    public static final int PROFILE_PHOTO_SIZE = 800;
}
