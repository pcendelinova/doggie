package cz.mendelu.xcendel1.dogapp.viewholders;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import cz.mendelu.xcendel1.dogapp.models.ActionType;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_DOG_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOGS_PATH;

/**
 * Created by admin on 13.12.2016.
 */

public class FriendshipRequestViewHolder extends BaseViewHolder<Dog> {

    @BindView(R.id.dog_name)
    TextView dogName;
    @BindView(R.id.dog_breed)
    TextView dogBreedName;
    @BindView(R.id.dog_photo)
    CircleImageView dogPhoto;
    @BindView(R.id.gender)
    ImageView gender;
    @BindView(R.id.acccept_friendship)
    RelativeLayout bttAcceptFriendship;
    @BindView(R.id.decline_friendship)
    RelativeLayout bttDeclineFriendship;

    Activity activity;


    public FriendshipRequestViewHolder(Activity activity, ViewGroup parent, Interfaces.ItemClickCallback itemClickCallback, final Interfaces.DogButtonClickCallback dogButtonClickCallback) {
        super(parent, R.layout.requesting_dog_list_row, itemClickCallback);
        this.activity = activity;

        bttAcceptFriendship.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dogButtonClickCallback.clickOnButton(getAdapterPosition(), ActionType.ACCEPT_FRIENDSHIP_REQUEST);
            }
        });

        bttDeclineFriendship.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dogButtonClickCallback.clickOnButton(getAdapterPosition(), ActionType.DECLINE_FRIENDSHIP_REQUEST);
            }
        });
    }

    @Override
    protected void onBind(Dog item, int position) {
        dogBreedName.setText(item.getBreed());
        dogName.setText(item.getName());
        String imagePath = item.getUserId() + STORAGE_IMAGES_DOGS_PATH+ item.getId();
        ImageUtils.setImageView(activity, imagePath, item.getPhoto(), dogPhoto, false, SET_DOG_PHOTO);

        if (item.isGender()) {
            gender.setImageResource(R.drawable.ic_gender_female);
        } else {
            gender.setImageResource(R.drawable.ic_gender_male);
        }

    }
}
