package cz.mendelu.xcendel1.dogapp.adapters;

import android.view.ViewGroup;

import java.util.List;

import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.models.Reminder;
import cz.mendelu.xcendel1.dogapp.viewholders.ReminderViewHolder;

/**
 * Created by Petra Cendelinova on 10/30/2016.
 */

public class ReminderAdapter extends BaseAdapter<Reminder, ReminderViewHolder> {

    public ReminderAdapter(List<Reminder> genericList) {
        super(genericList);
    }
    private Interfaces.ReminderCheckBoxCallBack reminderCheckBoxCallBack;

    @Override
    public ReminderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReminderViewHolder(parent, getItemClickCallback(), getReminderCheckBoxCallBack());
    }

    public Interfaces.ReminderCheckBoxCallBack getReminderCheckBoxCallBack() {
        return reminderCheckBoxCallBack;
    }

    public void setReminderCheckBoxCallBack(Interfaces.ReminderCheckBoxCallBack reminderCheckBoxCallBack) {
        this.reminderCheckBoxCallBack = reminderCheckBoxCallBack;
    }

}
