package cz.mendelu.xcendel1.dogapp.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 08.11.2016.
 */
//
//public class Chat extends GenericModel implements Parcelable {
//    private String lastMessage;
//    private long timestamp;
//    private ChatPartner chatPartner1;
//    private ChatPartner chatPartner2;
//
//    public Chat() {
//    }
//
//    public Chat(String id, String lastMessage, long timestamp, ChatPartner chatPartner1, ChatPartner chatPartner2) {
//        super(id);
//        this.lastMessage = lastMessage;
//        this.timestamp = timestamp;
//        this.chatPartner1 = chatPartner1;
//        this.chatPartner2 = chatPartner2;
//    }
//
//    protected Chat(Parcel in) {
//        super(in);
//        lastMessage = in.readString();
//        timestamp = in.readLong();
//        chatPartner1 = in.readParcelable(ChatPartner.class.getClassLoader());
//        chatPartner2 = in.readParcelable(ChatPartner.class.getClassLoader());
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        super.writeToParcel(dest, flags);
//        dest.writeString(lastMessage);
//        dest.writeLong(timestamp);
//        dest.writeParcelable(chatPartner1, flags);
//        dest.writeParcelable(chatPartner2, flags);
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    public static final Creator<Chat> CREATOR = new Creator<Chat>() {
//        @Override
//        public Chat createFromParcel(Parcel in) {
//            return new Chat(in);
//        }
//
//        @Override
//        public Chat[] newArray(int size) {
//            return new Chat[size];
//        }
//    };
//
//    public void setLastMessage(String lastMessage) {
//        this.lastMessage = lastMessage;
//    }
//
//    public void setChatPartner2(ChatPartner chatPartner2) {
//        this.chatPartner2 = chatPartner2;
//    }
//
//    public void setChatPartner1(ChatPartner chatPartner1) {
//        this.chatPartner1 = chatPartner1;
//    }
//
//    public void setTimestamp(long timestamp) {
//        this.timestamp = timestamp;
//    }
//
//    public String getLastMessage() {
//        return lastMessage;
//    }
//
//    public long getTimestamp() {
//        return timestamp;
//    }
//
//    public ChatPartner getChatPartner1() {
//        return chatPartner1;
//    }
//
//    public ChatPartner getChatPartner2() {
//        return chatPartner2;
//    }
//}
