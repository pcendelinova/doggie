package cz.mendelu.xcendel1.dogapp.helpers;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by Petra Cendelinova on 10/29/2016.
 */

public class FontHelper {

    public static void setFont(Context context, TextView textview, String font) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), font);
        textview.setTypeface(typeface);
    }

}
