package cz.mendelu.xcendel1.dogapp.services;



import android.util.Log;

import com.applozic.mobicomkit.api.account.register.RegisterUserClientService;
import com.applozic.mobicomkit.api.account.user.MobiComUserPreference;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class FirebaseIDService extends FirebaseInstanceIdService {

    private static final String TAG = FirebaseIDService.class.getSimpleName();
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "onTokenRefresh: Refreshed token: " + refreshedToken);


        if (MobiComUserPreference.getInstance(this).isRegistered()) {
            try {
                // message notification
                new RegisterUserClientService(this).updatePushNotificationId(refreshedToken);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
