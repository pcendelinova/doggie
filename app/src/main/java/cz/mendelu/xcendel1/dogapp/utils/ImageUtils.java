package cz.mendelu.xcendel1.dogapp.utils;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.ui.IconGenerator;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.helpers.NotificationHelper;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOGS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_DOG_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_MEMORY_PHOTO;

/**
 * Created by Petra Cendelinova on 10/30/2016.
 *
 * Utils class for working with images
 */

public class ImageUtils {

    public static final String TAG = ImageUtils.class.getSimpleName();

    /**
     * Encodes the given bitmap to String
     * @param bitmap
     * @return
     */
    public static String encodeBitmap(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        String imageEncoded = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);

        return imageEncoded;
    }

    /**
     * Decodes the given String to the bitmap
     * @param encodedImage
     * @return
     */
    public static Bitmap decodeBitmap(String encodedImage) {
        Bitmap imageDecoded = null;
        if (encodedImage != null && !encodedImage.isEmpty()) {
            byte[] decodedBytes = Base64.decode(encodedImage, Base64.DEFAULT);
            imageDecoded = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
        }

        return imageDecoded;
    }

    /**
     * Resizes markers
     * @param iconName
     * @param context
     * @return
     */
    public static Bitmap resizeMarkers(int iconName, Context context) {
        IconGenerator iconGen = new IconGenerator(context);
        Drawable shapeDrawable = ResourcesCompat.getDrawable(context.getResources(), iconName, null);
        iconGen.setBackground(shapeDrawable);

        View view = new View(context);
        view.setLayoutParams(new ViewGroup.LayoutParams(100, 100));
        iconGen.setContentView(view);

        Bitmap bitmap = iconGen.makeIcon();

        return bitmap;
    }

    /**
     * Returns an encoded image as array of bytes
     * @param encodedImage
     * @return
     */
    public static byte[] getImageAsBytes(String encodedImage) {
        byte[] dataBAOS = null;
        if (!encodedImage.isEmpty() && encodedImage != null) {
            Bitmap bitmap = ImageUtils.decodeBitmap(encodedImage);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            dataBAOS = baos.toByteArray();
        }

        return dataBAOS;

    }

    /**
     * If the storage is available, it saves the image into the external storage
     * @param context
     * @param finalBitmap
     * @param path
     * @param fileName
     */
    public static void saveImageToExternalStorage(Context context, Bitmap finalBitmap, String path, String fileName) {
        if (checkExternalStorage() && finalBitmap != null) {
            File filePath = context.getExternalCacheDir();
            String userFolder = ((FirebaseApplication) context.getApplicationContext()).getFirebaseUserId() + "/";
            File myDir = new File(filePath.getAbsolutePath() + "/" + userFolder + path);
            myDir.mkdirs();
            File file = new File(myDir, fileName);
            if (file.exists()) file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
                Log.d(TAG, "File at path " + myDir.getAbsolutePath() + " was saved");

            } catch (Exception e) {
                Log.e(TAG, "Unsuccessful storing image in the External Storage " + e.getMessage());
            }

        }

    }

    /**
     * Returns a file at a given path
     * @param context
     * @param path
     * @return
     */
    public static File getImageFile(Context context, String path) {
        File filePath = context.getExternalCacheDir();
        File myDir = new File(filePath.getAbsolutePath() + "/" + path);

        return myDir;
    }

    /**
     * Return a bitmap stores in the external storage
     * @param context
     * @param path
     * @return
     */
    public static Bitmap getImageFromStorage(Context context, String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        final Bitmap bitmap = BitmapFactory.decodeFile(getImageFile(context, path).getAbsolutePath(), options);

        return bitmap;
    }

    /**
     * If the image exits in the external storage, it removes it from the external storage
     * @param context
     * @param path
     * @param fileName
     */
    public static void removeImageFromExternalStorage(Context context, String path, String fileName) {
        String imagePath = ((FirebaseApplication) context.getApplicationContext()).getFirebaseUserId() + path + fileName;
        if (checkExternalStorage() && checkIfImageExits(context, imagePath)) {
            File filePath = context.getExternalCacheDir();
            File myDir = new File(filePath.getAbsolutePath() + "/" + imagePath);
            myDir.delete();
            Log.d(TAG, "File at path " + myDir.getAbsolutePath() + " was deleted");

        } else {
            Log.d(TAG, "removeImageFromExternalStorage - file doesn't exist or storage is not available");
        }
    }

    /**
     * Checks if the external storage is available
     * @return
     */
    public static boolean checkExternalStorage(){
        return Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    /**
     * Checks if the image exits in the external storage
     *
     * @param context
     * @param path
     * @return
     */
    public static boolean checkIfImageExits(Context context, String path) {
        if (context != null) {
            File filePath = context.getExternalCacheDir();
            File myDir;
            if (path.contains(".jpg")) {
                myDir = new File(filePath.getAbsolutePath() + "/" + path);
            } else {
                myDir = new File(filePath.getAbsolutePath() + "/" + path + ".jpg");
            }

            return myDir.exists();
        }

        return true;
    }

    /**
     * Sets a image from the external storage if exits. If doesn't parse string as bitmap
     *
     * @param context
     * @param imagePath
     * @param photo
     * @param imageView
     * @param invalidate
     * @param type
     */
    public static void setImageView(Context context, String imagePath, String photo, ImageView imageView, boolean invalidate, String type) {

        if (ImageUtils.checkExternalStorage() && ImageUtils.checkIfImageExits(context, imagePath)) {
            File filePath = context.getExternalCacheDir();
            File myDir = new File(filePath.getAbsolutePath() + "/" + imagePath + ".jpg");
            if (invalidate) {
                Picasso.with(context).invalidate(myDir);
            }
            Picasso.with(context).load(myDir).into(imageView);
        } else {
            if (photo != null) {
                if (photo.isEmpty()) {
                    switch (type) {
                        case SET_DOG_PHOTO:
                            imageView.setImageResource(R.drawable.dog_placeholder3);
                            break;
                        case SET_MEMORY_PHOTO:
                            imageView.setImageResource(R.drawable.no_image);
                            break;
                    }

                } else {
                    Bitmap bitmap = ImageUtils.decodeBitmap(photo);
                    imageView.setImageBitmap(bitmap);
                }

            }
        }
    }

    /**
     * Loads dog profile photo or user profile photo stored in the Firebase Storage or in the external storage
     *
     * @param context
     * @param url
     * @return
     */
    public static void loadUserPhoto(final Context context, String url, final NotificationCompat.Builder builder) {
        if (url != null && !url.isEmpty()) {
            Picasso.with(context)
                    .load(url)
                    .into(new Target() {
                              @Override
                              public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                  try {
                                      builder.setLargeIcon(Bitmap.createScaledBitmap(bitmap , 64, 64, false));
                                      NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                                      notificationManager.notify(NotificationHelper.NotificationID.getID(), builder.build());
                                  } catch (Exception e) {
                                      Log.e(TAG, "onBitmapLoaded " + e.getMessage());
                                  }
                              }

                              @Override
                              public void onBitmapFailed(Drawable errorDrawable) {
                              }

                              @Override
                              public void onPrepareLoad(Drawable placeHolderDrawable) {
                              }
                          }
                    );
        } else {
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.no_dog);
            builder.setLargeIcon(Bitmap.createScaledBitmap(bitmap, 64, 64, false));
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(NotificationHelper.NotificationID.getID(), builder.build());
        }

    }

    /**
     * Creates square bitmap
     *
     * @param bitmap
     * @return
     */
    public static Bitmap createSquaredBitmap(Bitmap bitmap) {
        int dim = Math.max(bitmap.getWidth(), bitmap.getHeight());
        Bitmap dstBmp = Bitmap.createBitmap(dim, dim, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(dstBmp);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, (dim - bitmap.getWidth()) / 2, (dim - bitmap.getHeight()) / 2, null);

        return dstBmp;
    }

    public static class DownloadingPhoto {
        private static boolean isDownloadDone = false;
        /**
         * Downloads dog photo from firebase database if doesn't exist in the external storage
         * @param context
         * @param userId
         * @param dogId
         * @param builder
         */
        public static void downloadDogPhoto(final Context context, String userId, String dogId, final NotificationCompat.Builder builder) {

            DatabaseReference databaseReference = ((FirebaseApplication) context.getApplicationContext()).getFirebaseDatabase();
            databaseReference.child(FIREBASE_CHILD_DOGS).child(userId).child(dogId).child(FIREBASE_CHILD_PHOTO).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String photo = dataSnapshot.getValue(String.class);
                    builder.setLargeIcon(Bitmap.createScaledBitmap(ImageUtils.decodeBitmap(photo) , 64, 64, false));

                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(NotificationHelper.NotificationID.getID(), builder.build());
                    isDownloadDone = true;

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        public static boolean isDownloadingDone() {
            return isDownloadDone;
        }
    }

}
