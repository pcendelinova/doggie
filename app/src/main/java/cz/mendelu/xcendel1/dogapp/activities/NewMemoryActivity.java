package cz.mendelu.xcendel1.dogapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.exception.ConversionException;
import com.mvc.imagepicker.ImagePicker;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.constants.SettingsConstants;
import cz.mendelu.xcendel1.dogapp.annotation.PastDate;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.models.Memory;
import cz.mendelu.xcendel1.dogapp.models.ModelFactory;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import cz.mendelu.xcendel1.dogapp.utils.MyTextWatcherUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_MEMORIES;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_EDIT;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_MEMORY;
import static cz.mendelu.xcendel1.dogapp.constants.SettingsConstants.SPLASH_DISPLAY_LENGTH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_MEMORY_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_MEMORIES_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.UPDATED_MEMORY;

/**
 * Represents a form to create a new memory
 */
public class NewMemoryActivity extends AppCompatActivity implements Validator.ValidationListener, View.OnTouchListener {
    private static final String TAG = NewMemoryActivity.class.getSimpleName();

    @NotEmpty(messageResId = R.string.error_required_field)
    @BindView(R.id.input_layout_name)
    TextInputLayout inputLayoutName;
    @BindView(R.id.new_memory_photo)
    CircleImageView memoryPhoto;

    @PastDate(messageResId = R.string.memory_date_must_be_in_past, dateFormat = SettingsConstants.DATE_FORMAT_DMY)
    @BindView(R.id.input_layout_date)
    TextInputLayout inputLayoutDate;
    @BindView(R.id.input_layout_place)
    TextInputLayout inputLayoutPlace;
    @BindView(R.id.input_layout_note)
    TextInputLayout inputLayoutNote;

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.btn_add_memory)
    AppCompatButton addMemory;


    private String imageResource;
    private Validator validator;
    private List<TextInputLayout> inputLayoutList;
    private Memory memory;
    private boolean isEdit = false;
    private String dogId;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_memory);

        ButterKnife.bind(this);

        if (getIntent().hasExtra(PARAM_EDIT) && getIntent().hasExtra(PARCEL_MEMORY)) {
            isEdit = true;
            Bundle bundle = getIntent().getExtras();
            memory = bundle.getParcelable(PARCEL_MEMORY);
            prepareData(memory);
            addMemory.setText(getString(R.string.edit_memory));
            dogId = bundle.getString(PARAM_DOG_ID);
            getSupportActionBar().setTitle(getString(R.string.edit_memory));
        } else {
            if (getIntent().hasExtra(PARAM_DOG_ID)) {
                dogId = getIntent().getExtras().getString(PARAM_DOG_ID);
            }
            addMemory.setEnabled(false);
            addMemory.setAlpha(.5f);
        }

        validator = new Validator(this);
        validator.setValidationListener(this);

        validator.registerAdapter(TextInputLayout.class,
                new ViewDataAdapter<TextInputLayout, String>() {
                    @Override
                    public String getData(TextInputLayout flet) throws ConversionException {
                        return flet.getEditText().getText().toString();
                    }
                }
        );

        Validator.registerAnnotation(PastDate.class);
        inputLayoutList = Arrays.asList(inputLayoutName, inputLayoutDate);
        MyTextWatcherUtils.setTextWatcherListeners(inputLayoutList, validator);
        addMemory.setOnTouchListener(this);

    }

    private void prepareData(Memory memory) {
        inputLayoutName.getEditText().setText(memory.getName());
        inputLayoutDate.getEditText().setText(memory.getDate());
        inputLayoutNote.getEditText().setText(memory.getNote());
        inputLayoutPlace.getEditText().setText(memory.getPlace());

        String imagePath = ((FirebaseApplication) getApplication()).getFirebaseUserId() + STORAGE_IMAGES_MEMORIES_PATH + memory.getId();
        ImageUtils.setImageView(NewMemoryActivity.this, imagePath, memory.getPhoto(), memoryPhoto, false, SET_MEMORY_PHOTO);
    }

    @OnClick(R.id.new_memory_photo)
    public void pickImage() {
        ImagePicker.pickImage(NewMemoryActivity.this, getString(R.string.image_picker));
    }

    @OnFocusChange({R.id.input_date, R.id.input_layout_date})
    public void showCalendar() {
        DialogHelper.showDatePicker(NewMemoryActivity.this, inputLayoutDate);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
        if (bitmap != null) {
            memoryPhoto.setImageBitmap(bitmap);
            imageResource = ImageUtils.encodeBitmap(bitmap);
        }

    }

    @OnClick(R.id.btn_add_memory)
    public void addNewMemory() {
        String memoryId = null;
        Memory createdMemory = ModelFactory.createMemory(null, inputLayoutName, inputLayoutDate, inputLayoutPlace, inputLayoutNote, imageResource);
        if (isEdit) {
            createdMemory.setUserId(memory.getUserId());
            createdMemory.setId(memory.getId());
            createdMemory.setUpdatedPhoto(true);
            createdMemory.setDogId(dogId);
            ((FirebaseApplication) getApplication()).editExistingRecord(createdMemory, dogId);
            DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.successful_edit_memory));
            memoryId = memory.getId();
            memory = createdMemory;
        } else {
            createdMemory.setDogId(dogId);
            memoryId = ((FirebaseApplication) getApplication()).saveNewRecord(createdMemory, FIREBASE_CHILD_MEMORIES);
            DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.successful_add_memory));
        }

        if (bitmap != null && memoryId != null) {
            String imageName = memoryId + ".jpg";
            ImageUtils.saveImageToExternalStorage(this, bitmap, STORAGE_IMAGES_MEMORIES_PATH, imageName);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent profileIntent = new Intent(NewMemoryActivity.this, MyDogProfileActivity.class);
                startActivity(profileIntent);

            }
        }, SPLASH_DISPLAY_LENGTH);

    }

    @Override
    public void onValidationSucceeded() {
        MyTextWatcherUtils.clearAllInputs(inputLayoutList);
        addMemory.setAlpha(1);
        addMemory.setEnabled(true);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        // validation
        addMemory.setEnabled(false);
        addMemory.setAlpha(.5f);
        MyTextWatcherUtils.clearAllInputs(inputLayoutList);
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof TextInputLayout) {
                ((TextInputLayout) view).setError(message);
            }
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (view.getId() == R.id.dog_profile_photo) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                view.setAlpha(.5f);
            } else {
                view.setAlpha(1f);
            }
            return false;
        } else if (view.getId() == R.id.btn_add_memory) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                view.setAlpha(.5f);
            } else {
                view.setAlpha(1f);
            }
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home && isEdit) {
            Intent intent = new Intent(NewMemoryActivity.this, MemoryDetailActivity.class);
            intent.putExtra(UPDATED_MEMORY, memory);
            setResult(Activity.RESULT_OK);
            finish();
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
