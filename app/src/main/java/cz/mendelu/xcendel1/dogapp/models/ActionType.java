package cz.mendelu.xcendel1.dogapp.models;

/**
 * Created by admin on 27.11.2016.
 *
 * Enum presents actions among dogs
 */

public enum ActionType {
    REQUEST_FRIENDSHIP,
    BARK,
    INVITE_TO_WALK,
    SEND_MESSAGE,
    DECLINE_FRIENDSHIP_REQUEST,
    ACCEPT_FRIENDSHIP_REQUEST;
}
