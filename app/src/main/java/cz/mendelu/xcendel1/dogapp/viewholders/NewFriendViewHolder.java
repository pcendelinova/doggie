package cz.mendelu.xcendel1.dogapp.viewholders;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindString;
import butterknife.BindView;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.models.ActionType;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_DOG_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOGS_PATH;

/**
 * Created by admin on 26.11.2016.
 */

public class NewFriendViewHolder extends BaseViewHolder<Dog> {
    @BindView(R.id.dog_name)
    TextView dogName;
    @BindView(R.id.dog_breed)
    TextView dogBreedName;
    @BindView(R.id.dog_photo)
    CircleImageView dogPhoto;
    @BindView(R.id.gender)
    ImageView gender;
    @BindView(R.id.send_message)
    RelativeLayout bttSendMessage;
    @BindView(R.id.bark)
    RelativeLayout bttBark;
    @BindView(R.id.request_friendship)
    RelativeLayout bttRequestFriendship;

    @BindString(R.string.cancel_friendship_request)
    String cancelFriendshipRequest;
    @BindString(R.string.request_friendship)
    String requestFriendship;
    @BindString(R.string.decline_friendship_request)
    String declineFriendshipRequest;

    private Interfaces.FriendshipRequestCallback friendshipRequestCallback;
    private Activity activity;


    public NewFriendViewHolder(Activity activity, ViewGroup parent, Interfaces.ItemClickCallback itemClickCallback, final Interfaces.DogButtonClickCallback dogButtonClickCallback,
                               final Interfaces.FriendshipRequestCallback friendshipRequestCallback) {
        super(parent, R.layout.new_friend_list_row, itemClickCallback);
        this.friendshipRequestCallback = friendshipRequestCallback;
        this.activity = activity;

        bttBark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dogButtonClickCallback.clickOnButton(getAdapterPosition(), ActionType.BARK);
            }
        });
        bttSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dogButtonClickCallback.clickOnButton(getAdapterPosition(), ActionType.SEND_MESSAGE);
            }
        });

    }

    @Override
    protected void onBind(final Dog item, int position) {
        if (item.getFriendshipState() != null) {
            switch (item.getFriendshipState()) {
                case NEW_REQUEST_STATE:
                    ((TextView) bttRequestFriendship.getChildAt(1)).setText(requestFriendship);
                    bttRequestFriendship.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            friendshipRequestCallback.requestClickCallback(getAdapterPosition(), FriendshipState.NEW_REQUEST_STATE);
                            ((TextView) bttRequestFriendship.getChildAt(1)).setText(cancelFriendshipRequest);
                            item.setFriendshipState(FriendshipState.CANCEL_STATE);
                        }
                    });

                    break;
                case CANCEL_STATE:
                    ((TextView) bttRequestFriendship.getChildAt(1)).setText(cancelFriendshipRequest);
                    bttRequestFriendship.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            friendshipRequestCallback.requestClickCallback(getAdapterPosition(), FriendshipState.CANCEL_STATE);
                            ((TextView) bttRequestFriendship.getChildAt(1)).setText(requestFriendship);
                            item.setFriendshipState(FriendshipState.NEW_REQUEST_STATE);

                        }
                    });

                    break;
                case DECLINE_STATE:
                    ((TextView) bttRequestFriendship.getChildAt(1)).setText(declineFriendshipRequest);
                    bttRequestFriendship.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            friendshipRequestCallback.requestClickCallback(getAdapterPosition(), FriendshipState.DECLINE_STATE);
                            item.setFriendshipState(FriendshipState.NEW_REQUEST_STATE);

                        }
                    });
                    break;
                default:
            }
        }
        if (!item.getBreed().isEmpty() && item.getBreed() != null) {
            dogBreedName.setText(item.getBreed());
        } else {
            dogBreedName.setVisibility(View.GONE);
        }

        dogName.setText(item.getName());
        String imagePath = item.getUserId() + STORAGE_IMAGES_DOGS_PATH+ item.getId();

        ImageUtils.setImageView(activity, imagePath, item.getPhoto(), dogPhoto, false, SET_DOG_PHOTO);

        if (item.isGender()) {
            gender.setImageResource(R.drawable.ic_gender_female);
        } else {
            gender.setImageResource(R.drawable.ic_gender_male);
        }
    }

}
