package cz.mendelu.xcendel1.dogapp.viewholders;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.models.Memory;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_MEMORIES;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_MEMORY_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_MEMORIES_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.UPDATED_PHOTO;

/**
 * Created by admin on 14.11.2016.
 */

public class MemoryViewHolder extends BaseViewHolder<Memory> {
    Activity activity;
    @BindView(R.id.memory_photo)
    ImageView memoryPhoto;
    @BindView(R.id.memory_title)
    TextView memoryTitle;

    public MemoryViewHolder(Activity activity, ViewGroup parent, Interfaces.ItemClickCallback itemClickCallback) {
        super(parent, R.layout.memory_card_view_item, itemClickCallback);
        this.activity = activity;
    }

    @Override
    protected void onBind(Memory item, int position) {
        if (item.getDate() != null) {
            memoryTitle.setText(item.getDate() + " " + item.getName());
        } else {
            memoryTitle.setText(item.getName());
        }

        String imagePath = item.getUserId() + STORAGE_IMAGES_MEMORIES_PATH + item.getId();
        if (item.isUpdatedPhoto()) {
            ImageUtils.setImageView(activity, imagePath, item.getPhoto(), memoryPhoto, true, SET_MEMORY_PHOTO);
            ((FirebaseApplication) activity.getApplication()).saveOrEditSingleValue(false, item.getDogId(), FIREBASE_CHILD_MEMORIES, item.getId() + "/" + UPDATED_PHOTO);
        } else {
            ImageUtils.setImageView(activity, imagePath, item.getPhoto(), memoryPhoto, false, SET_MEMORY_PHOTO);
        }

    }
}
