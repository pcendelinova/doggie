package cz.mendelu.xcendel1.dogapp.viewholders;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import cz.mendelu.xcendel1.dogapp.models.ActionType;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_DOG_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOG_FRIENDS_PATH;

/**
 * Created by admin on 24.11.2016.
 */

public class DogFriendViewHolder extends BaseViewHolder<Dog> {
    @BindView(R.id.dog_name)
    TextView dogName;
    @BindView(R.id.dog_breed)
    TextView dogBreedName;
    @BindView(R.id.dog_photo)
    CircleImageView dogPhoto;
    @BindView(R.id.gender)
    ImageView gender;
    @BindView(R.id.send_message)
    RelativeLayout bttSendMessage;
    @BindView(R.id.bark)
    RelativeLayout bttBark;
    @BindView(R.id.status)
    RelativeLayout bttStatus;
    @BindView(R.id.invite_to_walk)
    RelativeLayout bttWalk;

    private Activity activity;


    public DogFriendViewHolder(Activity activity, ViewGroup parent, Interfaces.ItemClickCallback itemClickCallback, final Interfaces.DogButtonClickCallback dogButtonClickCallback) {
        super(parent, R.layout.dog_friend_profile_item, itemClickCallback);
        this.activity = activity;

        bttBark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dogButtonClickCallback.clickOnButton(getAdapterPosition(), ActionType.BARK);
            }
        });
        bttSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dogButtonClickCallback.clickOnButton(getAdapterPosition(), ActionType.SEND_MESSAGE);
            }
        });
        bttWalk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dogButtonClickCallback.clickOnButton(getAdapterPosition(), ActionType.INVITE_TO_WALK);
            }
        });
    }

    @Override
    protected void onBind(Dog item, int position) {
        if (!item.getBreed().isEmpty() && item.getBreed() != null) {
            dogBreedName.setText(item.getBreed());
        } else {
            dogBreedName.setVisibility(View.GONE);
        }

        dogName.setText(item.getName());
        String imagePath = item.getUserId() + STORAGE_IMAGES_DOG_FRIENDS_PATH + item.getId();
        if (item.isUpdatedPhoto()) {
            ImageUtils.setImageView(activity, imagePath, item.getPhoto(), dogPhoto, true, SET_DOG_PHOTO);
        } else {
            ImageUtils.setImageView(activity, imagePath, item.getPhoto(), dogPhoto, false, SET_DOG_PHOTO);
        }

        if (item.isGender()) {
            gender.setImageResource(R.drawable.ic_gender_female);
        } else {
            gender.setImageResource(R.drawable.ic_gender_male);
        }

        if (!item.getStatus().isEmpty()) {
            ((TextView) bttStatus.getChildAt(1)).setText(item.getStatus());
        }

    }


}
