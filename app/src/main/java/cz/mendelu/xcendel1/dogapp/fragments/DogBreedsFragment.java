package cz.mendelu.xcendel1.dogapp.fragments;

import android.app.SearchManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.activities.DogBreedDetailActivity;
import cz.mendelu.xcendel1.dogapp.adapters.DogBreedAdapter;
import cz.mendelu.xcendel1.dogapp.comparators.DogBreedComparator;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.helpers.InitializationHelper;
import cz.mendelu.xcendel1.dogapp.models.DogBreed;

import static android.content.Context.SEARCH_SERVICE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_DOG_BREED;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOG_BREEDS;

/**
 * A screen that shows dog breed list
 */
public class DogBreedsFragment extends Fragment implements Interfaces.ItemClickCallback, SearchView.OnQueryTextListener {

    private static final String TAG = DogBreedsFragment.class.getSimpleName();
    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    private DogBreedAdapter dogBreedAdapter;
    private List<DogBreed> dogBreedList = new ArrayList<>();
    private DatabaseReference databaseReference;
    private ValueEventListener valueEventListener;
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dog_breeds, container, false);
        unbinder = ButterKnife.bind(this, view);

        databaseReference = ((FirebaseApplication) getActivity().getApplication()).getFirebaseDatabase();

        new DownloadTask().execute();

        dogBreedAdapter = new DogBreedAdapter(getActivity(), dogBreedList);
        dogBreedAdapter.setItemClickCallback(this);

        InitializationHelper.setupRecyclerView(getContext(), dogBreedAdapter, recyclerView);

        return view;
    }


    @Override
    public void onItemClick(int pos) {
        DogBreed dogBreed = dogBreedAdapter.getGenericList().get(pos);
        Intent intent = new Intent(getActivity(), DogBreedDetailActivity.class);
        intent.putExtra(PARCEL_DOG_BREED, dogBreed);
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dog_breeds, menu);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(true);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (valueEventListener != null) {
            databaseReference.removeEventListener(valueEventListener);
        }
        unbinder.unbind();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        dogBreedAdapter.getFilter().filter(query);
        dogBreedAdapter.setGenericList(dogBreedList);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        dogBreedAdapter.getFilter().filter(newText);
        dogBreedAdapter.setGenericList(dogBreedList);
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (valueEventListener != null) {
            databaseReference.removeEventListener(valueEventListener);
        }
    }

    public class DownloadTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            DialogHelper.showProgressDialog(getActivity(), getString(R.string.loading_data));
        }

        @Override
        protected Void doInBackground(Void... voids) {
            valueEventListener = databaseReference.child(FIREBASE_CHILD_DOG_BREEDS).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        DogBreed dogBreed = postSnapshot.getValue(DogBreed.class);
                        dogBreedList.add(dogBreed);
                    }
                    Collections.sort(dogBreedList, new DogBreedComparator());
                    dogBreedAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d(TAG, "onCancelled", databaseError.toException());
                    DialogHelper.showNoDataRetrieved(getContext());
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            recyclerView.setVisibility(View.VISIBLE);
            DialogHelper.hideProgressDialog();
        }
    }
}
