package cz.mendelu.xcendel1.dogapp.utils;

import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.applozic.mobicomkit.api.account.user.UserService;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.helpers.NotificationHelper;
import cz.mendelu.xcendel1.dogapp.models.User;
import de.hdodenhof.circleimageview.CircleImageView;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_USERS;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_DOG_FRIENDS;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_EMAIL;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_MY_DOGS;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_NAME;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_PHOTO_LINK;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_MY_PROFILE_PHOTO;

/**
 * Created by admin on 13.12.2016.
 *
 * Utils class that is use with user operations
 */

public class UserUtils {
    private static final String TAG = UserUtils.class.getSimpleName();

    /**
     * Returns SharedPreferences Editor
     * @param context
     * @return
     */
    public static SharedPreferences.Editor getSharedPreferencesEditor(Context context) {
        if (context != null) {
            SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            return editor;
        }

        return null;
    }

    /**
     * Check if information about the logged user is saved in the shared preferences and profile photo is saved in the external storage
     * @param context
     * @param uid - ID of the user
     * @return
     */
    public static boolean checkIfUserInfoIsSaved(Context context, String uid) {
        SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String profilePhotoPath = ((FirebaseApplication) context.getApplicationContext()).getFirebaseUserId() + STORAGE_IMAGES_PATH + STORAGE_MY_PROFILE_PHOTO;
        return sharedPref.contains(SHARED_PREFERENCES_EMAIL + uid) && sharedPref.contains(SHARED_PREFERENCES_NAME + uid) &&
                sharedPref.contains(SHARED_PREFERENCES_PHOTO_LINK + uid) && ImageUtils.checkIfImageExits(context, profilePhotoPath);

    }

    /**
     * Stores information about user to the shared preferences
     *
     * @param context
     * @param name
     * @param firebaseUser
     * @param photo
     */
    public static void storeInfoAboutUser(Context context, String name, FirebaseUser firebaseUser, String photo) {
        SharedPreferences.Editor editor = getSharedPreferencesEditor(context);
        editor.putString(SHARED_PREFERENCES_EMAIL + firebaseUser.getUid(), firebaseUser.getEmail());
        editor.putString(SHARED_PREFERENCES_NAME + firebaseUser.getUid(), name);
        editor.apply();

        if (photo != null) {
            // saves to the external storage
            Bitmap decodedPhoto = ImageUtils.decodeBitmap(photo);
            ImageUtils.saveImageToExternalStorage(context, decodedPhoto, STORAGE_IMAGES_PATH, STORAGE_MY_PROFILE_PHOTO);
        } else {
            // it must be facebook photo
            saveFacebookProfilePhoto(context, firebaseUser);
        }

    }

    /**
     * Stores a single record about user to shared preferences
     *
     * @param context
     * @param record
     */
    public static void storeSingleRecordAboutUser(Context context, String nameOfPreferences, String record) {
        SharedPreferences.Editor editor = getSharedPreferencesEditor(context);
        editor.putString(nameOfPreferences, record);
        editor.commit();
    }

    /**
     * Gets facebook profile photo and saves it to external storage
     *
     * @param firebaseUser
     */
    private static void saveFacebookProfilePhoto(final Context context, final FirebaseUser firebaseUser) {
        String facebookUserId = "";
        for (UserInfo profile : firebaseUser.getProviderData()) {
            if (profile.getProviderId().equals("facebook.com")) {
                facebookUserId = profile.getUid();
            }
        }

        final String photoUrl = "https://graph.facebook.com/" + facebookUserId + "/picture?type=large";
        storeSingleRecordAboutUser(context, SHARED_PREFERENCES_PHOTO_LINK + firebaseUser.getUid(), photoUrl);

        Picasso.with(context)
                .load(photoUrl)
                .into(new Target() {
                          @Override
                          public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                              try {
                                  // saves the photo to the external storage
                                  ImageUtils.saveImageToExternalStorage(context, bitmap, STORAGE_IMAGES_PATH, STORAGE_MY_PROFILE_PHOTO);
                                  // saves the user to Firebase database
                                  ((FirebaseApplication) context.getApplicationContext()).saveNewUser(firebaseUser.getDisplayName(), firebaseUser, photoUrl);
                                  // update photo in the Applozic chat
                                  UserService.getInstance(context).updateDisplayNameORImageLink(null, photoUrl, null, null);
                                  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                  View view = inflater.inflate(R.layout.nav_header_main, null);
                                  // sets the profile photo
                                  CircleImageView profilePhoto = (CircleImageView) view.findViewById(R.id.user_photo);
                                  profilePhoto.setImageBitmap(bitmap);
                              } catch (Exception e) {
                                  Log.e(TAG, "onBitmapLoaded " + e.getMessage());
                              }
                          }

                          @Override
                          public void onBitmapFailed(Drawable errorDrawable) {
                          }

                          @Override
                          public void onPrepareLoad(Drawable placeHolderDrawable) {
                          }
                      }
                );
    }

    /**
     * Saves dog friends ids to shared preferences to easy access them
     *
     * @param dogId
     * @param dogFriendIds
     */
    public static void saveDogFriends(Context context, String dogId, Collection<String> dogFriendIds) {
        if (context != null) {
            SharedPreferences.Editor editor = getSharedPreferencesEditor(context);
            Set<String> set = new HashSet<>();
            set.addAll(dogFriendIds);
            String key = SHARED_PREFERENCES_DOG_FRIENDS + dogId;
            editor.putStringSet(key, set);
            editor.apply();
        }
    }

    /**
     * Saves my dogs to the shared preferences
     *
     * @param context
     * @param userId
     * @param myDogIds
     */
    public static void saveMyDogs(Context context, String userId, Collection<String> myDogIds) {
        if (context != null) {
            SharedPreferences.Editor editor = getSharedPreferencesEditor(context);
            Set<String> set = new HashSet<>();
            set.addAll(myDogIds);
            String key = SHARED_PREFERENCES_MY_DOGS + userId;
            editor.putStringSet(key, set);
            editor.apply();
        }
    }

    /**
     * Returns my dogs saved in the shared preferences
     *
     * @param context
     * @param userId
     * @return
     */
    public static Set<String> getMyDogs(Context context, String userId) {
        Set<String> setOfMyDogs = new HashSet<>();
        if (context != null) {
            SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);

            if (sharedPref.contains(SHARED_PREFERENCES_MY_DOGS + userId)) {
                setOfMyDogs = sharedPref.getStringSet(SHARED_PREFERENCES_MY_DOGS + userId, new HashSet<String>());
            }
        }
        return setOfMyDogs;
    }

    /**
     * Sets user profile if dog doesn't exits
     *
     * @return user photo link in Firebase Storage
     */
    public static void setProfilePhoto(final Context context, String userId, final NotificationCompat.Builder builder) {
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String url =  dataSnapshot.getValue(String.class);
                ImageUtils.loadUserPhoto(context, url, builder);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        ((FirebaseApplication) context.getApplicationContext()).getFirebaseDatabase().child(FIREBASE_CHILD_USERS).child(userId).child(FIREBASE_CHILD_PHOTO).addListenerForSingleValueEvent(valueEventListener);

    }
}
