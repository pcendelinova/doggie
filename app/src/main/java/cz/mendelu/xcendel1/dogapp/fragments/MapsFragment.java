package cz.mendelu.xcendel1.dogapp.fragments;

import android.Manifest;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.LocationProvider;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.ApiClient;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.models.FilterMapPlace;
import cz.mendelu.xcendel1.dogapp.models.MarkerEnum;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.SEARCH_SERVICE;

/**
 * The fragment represents the map with markers of types
 */
public class MapsFragment extends Fragment implements LocationProvider.LocationCallback,
        OnMapReadyCallback,
        SearchView.OnClickListener,
        PlaceSelectionListener,
        Interfaces.FilterPlacesCallback {

    @BindView(R.id.fab)
    FloatingActionButton fab;
    private static final String TAG = MapsFragment.class.getSimpleName();
    private static final int MY_LOCATION_REQUEST_CODE = 0;

    private LocationProvider locationProvider;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int REQUEST_SELECT_PLACE = 1000;
    private GoogleMap map;
    private Location currentLocation;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        locationProvider = new LocationProvider(getContext(), this);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, view);
        fab.setImageResource(R.drawable.ic_filter_24dp);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onResume() {
        super.onResume();
        locationProvider.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        locationProvider.disconnect();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        checkLocationPermissions();
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);

    }


    private void checkLocationPermissions() {
        boolean checkedPermissions = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED;
        if (!checkedPermissions) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                map.setMyLocationEnabled(true);

            } else {

            }
        }
    }

    @OnClick(R.id.fab)
    public void filterPlaces() {
        DialogHelper.showFilterMapOptions(getContext(), this);

    }


    @Override
    public void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());
        currentLocation = location;
        map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
        map.animateCamera(CameraUpdateFactory.zoomTo(16));

        ApiClient.retrieveMapData(MarkerEnum.PET_SHOP, true, map, location, getContext());
        ApiClient.retrieveMapData(MarkerEnum.PARK, true, map, location, getContext());
        ApiClient.retrieveMapData(MarkerEnum.VET, false, map, location, getContext());
        ApiClient.retrieveMapData(MarkerEnum.EXERCISE_AREA, false, map, currentLocation, getContext());
        ApiClient.retrieveMapData(MarkerEnum.SHELTER, false, map, currentLocation, getContext());
        ApiClient.retrieveMapData(MarkerEnum.HOTEL, false, map, currentLocation, getContext());
        ApiClient.retrieveMapData(MarkerEnum.GROOMER, false, map, currentLocation, getContext());

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_map, menu);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getContext().getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(true);
        searchView.setOnSearchClickListener(this);
    }


    @Override
    public void onPlaceSelected(Place place) {
        Log.i(TAG, "Place Selected: " + place.getName());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.snippet(place.getAddress().toString());
        markerOptions.title(place.getName().toString());
        markerOptions.position(place.getLatLng());

        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageUtils.resizeMarkers(R.drawable.ic_place_24dp, getContext())));
        map.addMarker(markerOptions);
        map.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
        map.animateCamera(CameraUpdateFactory.zoomTo(11));

    }

    @Override
    public void onError(Status status) {
        Log.e(TAG, "onError: Status = " + status.toString());
        DialogHelper.showAlertDialog(getContext(), getString(R.string.unsuccess_searching));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SELECT_PLACE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                this.onPlaceSelected(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                this.onError(status);
            }
        }

    }


    @Override
    public void onClick(View view) {
        if (currentLocation != null) {
            try {
                Intent intent = new PlaceAutocomplete.IntentBuilder
                        (PlaceAutocomplete.MODE_FULLSCREEN)
                        .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                        .build(getActivity());
               getActivity().startActivityForResult(intent, REQUEST_SELECT_PLACE);
            } catch (GooglePlayServicesRepairableException |
                    GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void filterByType(List<FilterMapPlace> filterTypes) {
        map.clear();
        for (FilterMapPlace filterType : filterTypes) {
            switch (filterType) {
                case VET:
                    ApiClient.retrieveMapData(MarkerEnum.VET, true, map, currentLocation, getContext());
                    break;
                case PARK:
                    ApiClient.retrieveMapData(MarkerEnum.PARK, true, map, currentLocation, getContext());
                    break;
                case EXERCISE_AREA:
                    ApiClient.retrieveMapData(MarkerEnum.EXERCISE_AREA, false, map, currentLocation, getContext());
                    break;
                case PET_SHOP:
                    ApiClient.retrieveMapData(MarkerEnum.PET_SHOP, true, map, currentLocation, getContext());
                    break;
                case GROOMER:
                    ApiClient.retrieveMapData(MarkerEnum.GROOMER, false, map, currentLocation, getContext());
                    break;
                case HOTEL:
                    ApiClient.retrieveMapData(MarkerEnum.HOTEL, false, map, currentLocation, getContext());
                    break;
                case SHELTER:
                    ApiClient.retrieveMapData(MarkerEnum.SHELTER, false, map, currentLocation, getContext());
                    break;

            }
        }
    }
}

