package cz.mendelu.xcendel1.dogapp.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.mendelu.xcendel1.dogapp.R;

/**
 * Created by Petra Cendelinova on 10/31/2016.
 *
 * Reminder category enum
 */

public enum ReminderCategoryEnum {

    FUN(R.string.category_fun, R.drawable.ic_cirle_fun),
    VET(R.string.category_vet, R.drawable.ic_cirle_hospital),
    CLEANING(R.string.category_cleaning, R.drawable.ic_cirle_cleaning),
    PET_SHOP(R.string.category_pet_shop, R.drawable.ic_cirle_petshop),
    OTHERS(R.string.category_others, R.drawable.ic_cirle_others),
    HYGIENE(R.string.category_hygiene, R.drawable.ic_cirle_hygiene),
    BIRTHDAY(R.string.category_birthday, R.drawable.ic_cirle_birthday),
    WALK(R.string.category_walk, R.drawable.ic_cirle_walk);

    private int categoryName, icon;

    ReminderCategoryEnum(int categoryName, int icon) {
        this.categoryName = categoryName;
        this.icon = icon;
    }

    public int getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(int categoryName) {
        this.categoryName = categoryName;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }


    public static List<ReminderCategoryEnum> getCategoryEnumList() {
        List<ReminderCategoryEnum> categoryEnumList = new ArrayList<>();
        categoryEnumList.add(HYGIENE);
        categoryEnumList.add(VET);
        categoryEnumList.add(BIRTHDAY);
        categoryEnumList.add(PET_SHOP);
        categoryEnumList.add(CLEANING);
        categoryEnumList.add(WALK);
        categoryEnumList.add(FUN);
        categoryEnumList.add(OTHERS);

        return categoryEnumList;
    }

    public static int getReminderResource(String type, boolean typeOfResource) {

        int resource;
        ReminderCategoryEnum enumType = ReminderCategoryEnum.valueOf(type);

        switch (enumType) {
            case BIRTHDAY:
                resource = getResource(BIRTHDAY, typeOfResource);
                break;
            case CLEANING:
                resource = getResource(CLEANING, typeOfResource);
                break;
            case FUN:
                resource = getResource(FUN, typeOfResource);
                break;
            case VET:
                resource = getResource(VET, typeOfResource);
                break;
            case PET_SHOP:
                resource = getResource(PET_SHOP, typeOfResource);
                break;
            case OTHERS:
                resource = getResource(OTHERS, typeOfResource);
                break;
            case HYGIENE:
                resource = getResource(HYGIENE, typeOfResource);
                break;
            case WALK:
                resource = getResource(WALK, typeOfResource);
                break;
            default:
                resource = getResource(OTHERS, typeOfResource);
        }

        return resource;
    }

    private static int getResource(ReminderCategoryEnum reminderCategoryEnum, boolean typeOfResource) {
        if (typeOfResource) {
            return reminderCategoryEnum.getIcon();
        } else return reminderCategoryEnum.getCategoryName();
    }

}
