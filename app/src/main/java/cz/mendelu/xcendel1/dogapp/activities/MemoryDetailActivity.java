package cz.mendelu.xcendel1.dogapp.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.models.Memory;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;

import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_EDIT;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_MEMORY;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_MEMORY_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_MEMORIES_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.UPDATED_MEMORY;

public class MemoryDetailActivity extends AppCompatActivity {

    private static final String TAG = MemoryDetailActivity.class.getSimpleName();
    private static final int MEMORY_REQUEST_CODE = 115;
    private Memory memory;
    private String dogId;
    private boolean isUpdated = false;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_day_memory)
    TextView dayOfMemory;
    @BindView(R.id.tv_place_memory)
    TextView placeOfMemory;
    @BindView(R.id.tv_memory_description)
    TextView memoryDescription;
    @BindView(R.id.memory_photo)
    ImageView memoryPhoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory_detail);

        ButterKnife.bind(this);
        if (getIntent().hasExtra(PARCEL_MEMORY) && getIntent().hasExtra(PARAM_DOG_ID)) {
            Bundle data = getIntent().getExtras();
            memory = data.getParcelable(PARCEL_MEMORY);
            dogId = data.getString(PARAM_DOG_ID);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            setTitle(memory.getName());
            setupData();
        } else {
            DialogHelper.showAlertDialog(this, getString(R.string.no_data_retrieved));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_more, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_more) {
            View menuItemView = findViewById(R.id.action_more);
            PopupMenu popupMenu = DialogHelper.createPopupMenu(this, menuItemView, R.menu.popup_menu_memory_detail);
            popupMenu.show();
            registerPopupActions(popupMenu);
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            Intent intent = getIntent();
            intent.putExtra(UPDATED_MEMORY, memory);
            setResult(Activity.RESULT_OK);
            finish();
            return true;
        }


        return false;
    }

    /**
     * Setups data for memory detail screen. If the memory has been updated it updates and invalidates photo because of the library Picasso
     */
    private void setupData() {

        if (memory.getPlace().isEmpty()) {
            placeOfMemory.setVisibility(View.GONE);
        }
        dayOfMemory.setText(memory.getDate());
        placeOfMemory.setText(memory.getPlace());
        memoryDescription.setText(memory.getNote());

        String imagePath = ((FirebaseApplication) getApplication()).getFirebaseUserId() + STORAGE_IMAGES_MEMORIES_PATH + memory.getId();
        if (isUpdated) {
            ImageUtils.setImageView(MemoryDetailActivity.this, imagePath, memory.getPhoto(), memoryPhoto, true, SET_MEMORY_PHOTO);
        } else {
            ImageUtils.setImageView(MemoryDetailActivity.this, imagePath, memory.getPhoto(), memoryPhoto, false, SET_MEMORY_PHOTO);
        }

    }

    /**
     * Register popup actions
     * @param popupMenu
     */
    private void registerPopupActions(PopupMenu popupMenu) {
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_share:
                        String imagePath = ((FirebaseApplication) getApplication()).getFirebaseUserId() + STORAGE_IMAGES_MEMORIES_PATH + memory.getId() + ".jpg";
                        String infoAboutMemory = memory.getName() + "\n" + memory.getDate() + "\n" + memory.getPlace();
                        DialogHelper.sharePhoto(MemoryDetailActivity.this, "mojeVzpominka", ImageUtils.getImageFromStorage(MemoryDetailActivity.this, imagePath), infoAboutMemory);
                        return true;
                    case R.id.action_remove:
                        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ((FirebaseApplication) getApplication()).removeExistingRecord(memory, dogId);
                                ImageUtils.removeImageFromExternalStorage(MemoryDetailActivity.this, STORAGE_IMAGES_MEMORIES_PATH, memory.getId() + ".jpg");
                                startActivity(new Intent(MemoryDetailActivity.this, MyDogProfileActivity.class));
                            }
                        };
                        DialogHelper.showQuestionDialog(MemoryDetailActivity.this, getString(R.string.remove_memory_title), getString(R.string.remove_memory_question), onClickListener);
                        return true;
                    case R.id.action_edit:
                        Intent intent = new Intent(MemoryDetailActivity.this, NewMemoryActivity.class);
                        intent.putExtra(PARAM_EDIT, PARAM_EDIT);
                        intent.putExtra(PARCEL_MEMORY, memory);
                        intent.putExtra(PARAM_DOG_ID, dogId);
                        startActivityForResult(intent, MEMORY_REQUEST_CODE);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        if (requestCode == MEMORY_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data.hasExtra(UPDATED_MEMORY)) {
                memory = data.getParcelableExtra(UPDATED_MEMORY);
                isUpdated = true;
                setupData();
            }
        }
    }

}
