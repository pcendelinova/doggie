package cz.mendelu.xcendel1.dogapp;

import java.util.List;

import cz.mendelu.xcendel1.dogapp.models.ActionType;
import cz.mendelu.xcendel1.dogapp.models.FilterFriendEnum;
import cz.mendelu.xcendel1.dogapp.models.FilterMapPlace;
import cz.mendelu.xcendel1.dogapp.models.FilterNewFriendEnum;
import cz.mendelu.xcendel1.dogapp.models.MessageRequest;
import cz.mendelu.xcendel1.dogapp.models.MessageResponse;
import cz.mendelu.xcendel1.dogapp.models.PlaceResponse;
import cz.mendelu.xcendel1.dogapp.viewholders.FriendshipState;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by admin on 29.11.2016.
 */

public class Interfaces {
    public interface DogButtonClickCallback {
        void clickOnButton(int pos, ActionType actionType);
    }

    public interface FriendshipRequestCallback {
        void requestClickCallback(int pos, FriendshipState friendshipState);
    }

    public interface FilterNewFriendOptionsCallback {
        void filterByType(FilterNewFriendEnum filterType);
    }

    public interface FilterTypeOfFriendCallback {
        void filterByType(FilterFriendEnum filterType);
    }

    public interface FilterPlacesCallback {
        void filterByType(List<FilterMapPlace> filterTypes);
    }

    public interface ReminderCheckBoxCallBack {
        void addCheckBox(int pos);
    }

    public interface ItemClickCallback {
        void onItemClick(int pos);
    }

    public interface PlacesInterface {
        @GET("api/place/nearbysearch/json?sensor=true&key=AIzaSyCptbM4MoqORG5f0Ol4kgEsFU2eqUwHhoM")
        Call<PlaceResponse> getNearbyPlaces(@Query("type") String type, @Query("location") String location, @Query("radius") int radius);

        @GET("api/place/textsearch/json?key=AIzaSyCptbM4MoqORG5f0Ol4kgEsFU2eqUwHhoM")
        Call<PlaceResponse> getNearbyPlacesByText(@Query("query") String query, @Query("location") String location, @Query("radius") int radius);
    }

    public interface MessageInterface {
        @Headers("Authorization: key=AAAARxpEXM0:APA91bE5zSo3o4Bfj4gxdcmwzfnwc1Ff6HgBfxmHU7R6bHhmtlWLOcKefC_3QJjApXEoWfdvqULd7X57QReJTkfFaV69ApTj4LhmTI0d3s5jnnrXaZuldSeD46QJ6aOOW4HGHQO1B4bnZIqsPAHD7zy_6-_oAKzeGw")
        @POST("send")
        Call<MessageResponse> sendDogNotification(@Body MessageRequest messageRequest);
    }
}
