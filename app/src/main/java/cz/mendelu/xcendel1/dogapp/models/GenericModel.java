package cz.mendelu.xcendel1.dogapp.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 04.11.2016.
 *
 * Generic model that has one property
 */

public class GenericModel implements Parcelable {
    public String id;

    public GenericModel(String id) {
        this.id = id;
    }

    public GenericModel() {

    }

    protected GenericModel(Parcel in) {
        id = in.readString();
    }

    public static final Creator<GenericModel> CREATOR = new Creator<GenericModel>() {
        @Override
        public GenericModel createFromParcel(Parcel in) {
            return new GenericModel(in);
        }

        @Override
        public GenericModel[] newArray(int size) {
            return new GenericModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
    }
}

