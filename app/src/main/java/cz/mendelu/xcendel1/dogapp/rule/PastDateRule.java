package cz.mendelu.xcendel1.dogapp.rule;

import android.text.format.DateUtils;

import com.mobsandgeeks.saripaar.AnnotationRule;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.mendelu.xcendel1.dogapp.constants.SettingsConstants;
import cz.mendelu.xcendel1.dogapp.annotation.PastDate;

/**
 * Created by admin on 26.11.2016.
 */

public class PastDateRule extends AnnotationRule<PastDate, String> {

    protected PastDateRule(PastDate pastDate) {
        super(pastDate);
    }

    @Override
    public boolean isValid(String dateString) {
        DateFormat dateFormat = new SimpleDateFormat(SettingsConstants.DATE_FORMAT_DMY);
        dateFormat.setLenient(false);
        Date parsedDate = null;
        try {
            parsedDate = dateFormat.parse(dateString);
        } catch (ParseException ignored) {}

        String datePattern = "\\d{1,2}. \\d{1,2}. \\d{4}";

        return parsedDate != null && (parsedDate.before(new Date()) || DateUtils.isToday(parsedDate.getTime())) && dateString.matches(datePattern);
    }

}
