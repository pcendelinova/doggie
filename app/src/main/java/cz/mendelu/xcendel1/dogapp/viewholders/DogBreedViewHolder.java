package cz.mendelu.xcendel1.dogapp.viewholders;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.models.DogBreed;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by admin on 14.11.2016.
 */

public class DogBreedViewHolder extends BaseViewHolder<DogBreed> {
    private Activity activity;
    @BindView(R.id.dog_breed_name)
    TextView dogBreedName;
    @BindView(R.id.dog_breed_photo)
    CircleImageView dogBreedPhoto;

    public DogBreedViewHolder(Activity activity, ViewGroup parent, Interfaces.ItemClickCallback itemClickCallback) {
        super(parent, R.layout.dog_breed_list_item, itemClickCallback);
        this.activity = activity;
    }


    @Override
    protected void onBind(DogBreed item, int position) {

        dogBreedName.setText(item.getName());
        Picasso.with(activity)
                .load(item.getPhotoLink())
                .placeholder(R.drawable.dog_placeholder3)
                .into(dogBreedPhoto);

    }
}
