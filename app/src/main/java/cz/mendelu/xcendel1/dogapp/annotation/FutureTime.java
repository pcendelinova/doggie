package cz.mendelu.xcendel1.dogapp.annotation;

import android.support.annotation.StringRes;

import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import cz.mendelu.xcendel1.dogapp.constants.SettingsConstants;
import cz.mendelu.xcendel1.dogapp.rule.FutureTimeRule;

/**
 * Created by admin on 10.12.2016.
 *
 * Annotation for validation of future time
 */


@ValidateUsing(FutureTimeRule.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FutureTime {

    String dateFormat() default SettingsConstants.FORMAT_TIME;

    @StringRes int messageResId() default -1;

    String message() default "Čas musí být v budoucnosti ve formátu hh:mm";

    public int sequence() default -1;

}
