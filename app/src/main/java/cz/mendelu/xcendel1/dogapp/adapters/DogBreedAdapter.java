package cz.mendelu.xcendel1.dogapp.adapters;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.xcendel1.dogapp.models.DogBreed;
import cz.mendelu.xcendel1.dogapp.viewholders.DogBreedViewHolder;


/**
 * Created by admin on 04.11.2016.
 *
 * Dog breed adapter including filtering by dog name
 */

public class DogBreedAdapter extends BaseAdapter<DogBreed, DogBreedViewHolder> implements Filterable {

    private List<DogBreed> originalList;
    private List<DogBreed> list;
    private Activity activity;

    public DogBreedAdapter(Activity activity, List<DogBreed> genericList) {
        super(genericList);
        this.list = genericList;
        this.activity = activity;
    }

    @Override
    public DogBreedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DogBreedViewHolder(activity, parent, getItemClickCallback());
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (List<DogBreed>) results.values;
                setGenericList(list);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                originalList = getGenericList();
                List<DogBreed> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = originalList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

        protected List<DogBreed> getFilteredResults(String constraint) {
        List<DogBreed> results = new ArrayList<>();

        for (DogBreed item : originalList) {
            if (item.getName().toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}