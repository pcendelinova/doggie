package cz.mendelu.xcendel1.dogapp.adapters;

import android.app.Activity;
import android.view.ViewGroup;

import java.util.List;

import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.viewholders.FriendshipRequestViewHolder;

/**
 * Created by admin on 13.12.2016.
 */

public class FriendshipRequestAdapter extends BaseAdapter<Dog, FriendshipRequestViewHolder> {
    private Interfaces.DogButtonClickCallback dogButtonClickCallback;
    private Activity activity;

    public FriendshipRequestAdapter(Activity activity, List<Dog> genericList) {
        super(genericList);
        this.activity = activity;
    }

    @Override
    public FriendshipRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FriendshipRequestViewHolder(activity, parent, getItemClickCallback(), getDogButtonClickCallback());
    }

    public Interfaces.DogButtonClickCallback getDogButtonClickCallback() {
        return dogButtonClickCallback;
    }

    public void setDogButtonClickCallback(Interfaces.DogButtonClickCallback dogButtonClickCallback) {
        this.dogButtonClickCallback = dogButtonClickCallback;
    }
}
