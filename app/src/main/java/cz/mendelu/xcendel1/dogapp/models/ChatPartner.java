//package cz.mendelu.xcendel1.dogapp.models;
//
//import android.os.Parcel;
//import android.os.Parcelable;
//
//import java.util.Arrays;
//
///**
// * Created by admin on 08.11.2016.
// */
//
//public class ChatPartner extends GenericModel implements Parcelable {
//    private String name;
//    private String photo;
//
//    public ChatPartner() {
//    }
//
//    public ChatPartner(String id, String name, String photo) {
//        super(id);
//        this.photo = photo;
//        this.name = name;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getPhoto() {
//        return photo;
//    }
//
//    public void setPhoto(String photo) {
//        this.photo = photo;
//    }
//
//    public static Creator<ChatPartner> getCREATOR() {
//        return CREATOR;
//    }
//
//    protected ChatPartner(Parcel in) {
//        super(in);
//        name = in.readString();
//        photo = in.readString();
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        super.writeToParcel(dest, flags);
//        dest.writeString(name);
//        dest.writeString(photo);
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    public static final Creator<ChatPartner> CREATOR = new Creator<ChatPartner>() {
//        @Override
//        public ChatPartner createFromParcel(Parcel in) {
//            return new ChatPartner(in);
//        }
//
//        @Override
//        public ChatPartner[] newArray(int size) {
//            return new ChatPartner[size];
//        }
//    };
//}
