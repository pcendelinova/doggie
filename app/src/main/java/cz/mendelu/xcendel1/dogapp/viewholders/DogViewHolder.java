package cz.mendelu.xcendel1.dogapp.viewholders;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.activities.MyDogProfileActivity;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOGS;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_DOG;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_DOG_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOGS_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.UPDATED_PHOTO;

/**
 * Created by admin on 14.11.2016.
 */

public class DogViewHolder extends BaseViewHolder<Dog> {
    @BindView(R.id.card_view)
    CardView cardView;
    @BindView(R.id.dog_name) TextView dogName;
    @BindView(R.id.dog_breed) TextView dogBreed;
    @BindView(R.id.dog_profile_photo) ImageView dogPhoto;
    Activity activity;

    public int position;
    public Dog dog;

    public DogViewHolder(Activity activity, ViewGroup parent, Interfaces.ItemClickCallback itemClickCallback) {
        super(parent, R.layout.my_dog_list_item, itemClickCallback);
        this.activity = activity;
    }

    @Override
    protected void onBind(Dog item, int position) {
        dogName.setText(item.getName());
        dogBreed.setText(item.getBreed());

        String imagePath = item.getUserId() + STORAGE_IMAGES_DOGS_PATH + item.getId();
        if (item.isUpdatedPhoto()) {
            ImageUtils.setImageView(activity, imagePath, item.getPhoto(), dogPhoto, true, SET_DOG_PHOTO);
            ((FirebaseApplication) activity.getApplication()).saveOrEditSingleValue(false, item.getId(), FIREBASE_CHILD_DOGS, UPDATED_PHOTO);
        } else {
            ImageUtils.setImageView(activity, imagePath, item.getPhoto(), dogPhoto, false, SET_DOG_PHOTO);
        }

    }

    @OnClick(R.id.dog_profile_photo)
    public void showDogDetail() {
        Intent intent = new Intent(activity, MyDogProfileActivity.class);
        intent.putExtra(PARCEL_DOG, dog);
        activity.startActivity(intent);
    }

}
