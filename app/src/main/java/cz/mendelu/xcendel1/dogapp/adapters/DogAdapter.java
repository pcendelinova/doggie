package cz.mendelu.xcendel1.dogapp.adapters;

import android.app.Activity;
import android.view.ViewGroup;

import java.util.List;

import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.viewholders.DogViewHolder;

/**
 * Created by Petra Cendelinova on 10/27/2016.
 */

public class DogAdapter extends BaseAdapter<Dog, DogViewHolder> {

    Activity activity;
    List<Dog> dogList;

    public DogAdapter(Activity activity, List<Dog> genericList) {
        super(genericList);
        this.activity = activity;
        this.dogList = genericList;
    }

    @Override
    public DogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DogViewHolder(activity, parent, getItemClickCallback());
    }

    @Override
    public final void onBindViewHolder(DogViewHolder dogViewHolder, int position) {
        Dog item = dogList.get(position);
        // display data at position
        dogViewHolder.performBind(item, position);
        dogViewHolder.position = position;
        dogViewHolder.dog = item;

    }
}
