package cz.mendelu.xcendel1.dogapp.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.helpers.FontHelper;

import static cz.mendelu.xcendel1.dogapp.constants.SettingsConstants.CUSTOM_FONT;
import static cz.mendelu.xcendel1.dogapp.constants.SettingsConstants.SPLASH_DISPLAY_LENGTH;

/**
 * A screen that is triggered when the app is started
 */
public class SplashActivity extends AppCompatActivity {

    public static final String TAG = SplashActivity.class.getSimpleName();

    @BindView(R.id.app_name)
    TextView appName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean isLogged = ((FirebaseApplication) getApplication()).checkUserLogin(SplashActivity.this);

                if (isLogged) {
                    String userId = ((FirebaseApplication) getApplication()).getFirebaseUserId();
                    FirebaseMessaging.getInstance().subscribeToTopic(userId);
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));

                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                }

            }
        }, SPLASH_DISPLAY_LENGTH);


        FontHelper.setFont(this, appName, CUSTOM_FONT);

    }
}
