package cz.mendelu.xcendel1.dogapp.adapters;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.viewholders.BaseViewHolder;

/**
 * Created by admin on 14.11.2016.
 *
 * Base Adapter
 */

public abstract class BaseAdapter<T, VH extends BaseViewHolder<T>> extends RecyclerView.Adapter<VH> {
    private List<T> genericList = new ArrayList<>();
    private Interfaces.ItemClickCallback itemClickCallback;

    public BaseAdapter(List<T> genericList) {
        this.genericList = genericList;
    }
    public Interfaces.ItemClickCallback getItemClickCallback() {
        return itemClickCallback;
    }

    public void setItemClickCallback(Interfaces.ItemClickCallback itemClickCallback) {
        this.itemClickCallback = itemClickCallback;
    }

    public List<T> getGenericList() {
        return ((genericList != null) ? genericList : null);
    }

    public T getItem(int pos) {
        return ((genericList != null && pos < genericList.size()) ? genericList.get(pos) : null);
    }

    public void setGenericList(List<T> genericList) {
        this.genericList = genericList;
    }

    @Override
    public void onBindViewHolder(VH vh, int position) {
        T item = genericList.get(position);
        // display data at position
        vh.performBind(item, position);
    }

    @Override
    public int getItemCount() {
        return ((genericList != null ? genericList.size() : 0));
    }

}
