package cz.mendelu.xcendel1.dogapp.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.applozic.mobicomkit.api.account.user.UserService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.mvc.imagepicker.ImagePicker;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_MY_PROFILE_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.UPDATED_PHOTO;


/**
 * A settings screen that creates user's preferences such as visibility of the profile, notifications
 */

public class SettingsActivity extends AppCompatPreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String PREFERENCE_EMAIL = "email";
    public static final String PREFERENCE_PASSWORD = "password";
    private static final String PREFERENCE_PHOTO = "profile_photo";
    private static final String PREFERENCE_NOTIFICATION = "notifications";
    private static final String PREFERENCE_NOTIFICATION_VIBRATE = "notification_vibrate";
    private static final String PREFERENCE_NOTIFICATION_RINGTONE = "notification_ringtone";

    private static FirebaseAuth auth;
    private static DatabaseReference databaseReference;
    private static CoordinatorLayout coordinatorLayout;
    private Intent intent;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (intent == null) {
                intent = getIntent();
            }
            setResult(Activity.RESULT_OK, intent);
            finish();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();

        auth = ((FirebaseApplication) getApplication()).getFirebaseAuth();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        SharedPreferences prefs = getSharedPreferences("setting.dogapp",
                Context.MODE_WORLD_READABLE);
        SharedPreferences.Editor editor = prefs.edit();
        if (key.equals(PREFERENCE_NOTIFICATION)) {
            boolean isAllowed = sharedPreferences.getBoolean(key, true);
            editor.putBoolean(PREFERENCE_NOTIFICATION, isAllowed);
            editor.commit();
        }

        if (key.equals(PREFERENCE_NOTIFICATION_VIBRATE)) {
            boolean isAllowed = sharedPreferences.getBoolean(key, true);
            editor.putBoolean(PREFERENCE_NOTIFICATION_VIBRATE, isAllowed);
            editor.commit();
        }

        if (key.equals(PREFERENCE_NOTIFICATION_RINGTONE)) {
            String ringtonePreference = sharedPreferences.getString("notification_ringtone", "DEFAULT_SOUND");
            editor.putString(PREFERENCE_NOTIFICATION_RINGTONE, ringtonePreference);
            editor.commit();
        }

    }

    public static class MyPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);

            Preference profilePreference = findPreference(PREFERENCE_PHOTO);
            profilePreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ImagePicker.pickImage(getActivity(), getString(R.string.choose_profile_photo));
                    return true;
                }
            });

            Preference passwordPreference = findPreference(PREFERENCE_PASSWORD);
            passwordPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    auth.sendPasswordResetEmail(auth.getCurrentUser().getEmail())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.reset_password_instructions_sent));
                                        DialogHelper.hideProgressDialog();
                                    } else {
                                        DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.reset_password_instructions_not_sent));
                                    }

                                }
                            });
                    return true;
                }
            });


            //TODO udpate email in next version
//            Preference emailPreference = findPreference(PREFERENCE_EMAIL);
//            emailPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
//                @Override
//                public boolean onPreferenceChange(Preference preference, Object o) {
//                    ((FirebaseApplication) getActivity().getApplication()).loginUser(getActivity(), "julca2@gmail.com", "aaaaaa");
//                    final String newEmail = (String) o;
//                    auth.getCurrentUser().updateEmail(newEmail.trim())
//                            .addOnCompleteListener(new OnCompleteListener<Void>() {
//                                @Override
//                                public void onComplete(@NonNull Task<Void> task) {
//                                    if (task.isSuccessful()) {
//                                        databaseReference.child(FIREBASE_USER_FIELD_EMAIL).setValue(newEmail);
//                                        SharedPreferences.Editor editor = sharedPref.edit();
//                                        editor.putString(SHARED_PREFERENCES_EMAIL, newEmail);
//                                        Toast.makeText(getActivity(), "Email address is updated.", Toast.LENGTH_LONG).show();
//
//                                    } else {
//                                        Toast.makeText(getActivity(), "Failed to update email!", Toast.LENGTH_LONG).show();
//                                        auth.signInWithEmailAndPassword("julca2@gmail.com", "aaaaaa");
//                                    }
//                                }
//                            });
//                    return true;
//                }
//            });


        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
        if (bitmap != null) {
            ImageUtils.saveImageToExternalStorage(this, bitmap, STORAGE_IMAGES_PATH, STORAGE_MY_PROFILE_PHOTO);
            String imageResource = ImageUtils.encodeBitmap(bitmap);
            String userId = ((FirebaseApplication) getApplication()).getFirebaseUserId();
            ((FirebaseApplication) getApplication()).storeUserProfilePhotoToStorage(SettingsActivity.this, userId, ImageUtils.getImageAsBytes(imageResource));

            intent = getIntent();
            intent.putExtra(UPDATED_PHOTO, UPDATED_PHOTO);
            DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.profile_photo_updated));
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreferences(MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferences(MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(this);
    }


}
