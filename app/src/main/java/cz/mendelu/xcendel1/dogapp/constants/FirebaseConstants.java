package cz.mendelu.xcendel1.dogapp.constants;

/**
 * Created by admin on 12.12.2016.
 *
 * Constans related to Firebase database
 */

public class FirebaseConstants {
    public static final String FIREBASE_CHILD_USERS = "users";
    public static final String FIREBASE_CHILD_DOGS = "dogs";
    public static final String FIREBASE_CHILD_REMINDERS = "reminders";
    public static final String FIREBASE_CHILD_DOG_BREEDS = "dogbreeds";
    public static final String FIREBASE_CHILD_FAVOURITE_DOG_BREEDS = "favourite";
    public static final String FIREBASE_CHILD_MEMORIES = "memories";
    public static final String FIREBASE_CHILD_DOG_STATUS = "status";
    public static final String FIREBASE_CHILD_FRIENDSHIPS = "friendships";
    public static final String FIREBASE_CHILD_FRIENDSHIPS_REQUESTS = "friendshiprequests";
    public static final String REQUESTING_ID = "requestingId";
    public static final String REQUESTED_ID = "requestedId";
    public static final String DOG_BREED_ID = "dogBreedId";
    public static final String FIREBASE_CHILD_PHOTO = "photo";
}
