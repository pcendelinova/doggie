package cz.mendelu.xcendel1.dogapp.adapters;

import android.app.Activity;
import android.view.ViewGroup;

import java.util.List;

import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.viewholders.DogFriendViewHolder;

/**
 * Created by admin on 24.11.2016.
 */

public class DogFriendAdapter extends BaseAdapter<Dog,DogFriendViewHolder> {
    private Interfaces.DogButtonClickCallback dogButtonClickCallback;
    Activity activity;

    public Interfaces.DogButtonClickCallback getDogButtonClickCallback() {
        return dogButtonClickCallback;
    }

    public void setDogButtonClickCallback(Interfaces.DogButtonClickCallback dogButtonClickCallback) {
        this.dogButtonClickCallback = dogButtonClickCallback;
    }


    public DogFriendAdapter(Activity activity, List<Dog> genericList) {
        super(genericList);
        this.activity = activity;
    }

    @Override
    public DogFriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DogFriendViewHolder(activity, parent, getItemClickCallback(), getDogButtonClickCallback());
    }


}
