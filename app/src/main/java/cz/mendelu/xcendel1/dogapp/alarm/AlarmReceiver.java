package cz.mendelu.xcendel1.dogapp.alarm;


import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import cz.mendelu.xcendel1.dogapp.helpers.NotificationHelper;
import cz.mendelu.xcendel1.dogapp.models.Reminder;

import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.REMINDER_ID;

/**
 * Created by admin on 06.11.2016.
 *
 * It receives broadcasts
 */

public class AlarmReceiver extends BroadcastReceiver {
    private static final String TAG = AlarmReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, "onReceive");
        int reminderId = Integer.parseInt(intent.getStringExtra(REMINDER_ID));
        ReminderDatabase reminderDatabase = new ReminderDatabase(context);
        Reminder reminder = reminderDatabase.getReminder(reminderId);

        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(reminderId, NotificationHelper.createReminderNotification(context, reminder));

    }


}
