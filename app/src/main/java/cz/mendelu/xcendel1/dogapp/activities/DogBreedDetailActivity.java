package cz.mendelu.xcendel1.dogapp.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.mendelu.xcendel1.dogapp.ExpandableTextView;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.models.DogBreed;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.DOG_BREED_ID;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOG_BREEDS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_FAVOURITE_DOG_BREEDS;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_FAVOURITE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_DOG_BREED;

/**
 * A screen that shows the dog breed detail such as character, description, care of the dog breed
 */
public class DogBreedDetailActivity extends AppCompatActivity {
    private static final String TAG = DogBreedDetailActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_character)
    ExpandableTextView tvCharacter;
    @BindView(R.id.tv_description)
    ExpandableTextView tvDescription;
    @BindView(R.id.tv_care)
    ExpandableTextView tvCare;

    @BindView(R.id.description_arrow)
    TextView descriptionArrow;
    @BindView(R.id.care_arrow)
    TextView careArrow;
    @BindView(R.id.character_arrow)
    TextView characterArrow;


    @BindView(R.id.tag_age)
    TextView tagAge;
    @BindView(R.id.tag_usage)
    TextView tagUsage;
    @BindView(R.id.tag_weight)
    TextView tagWeight;

    @BindView(R.id.dog_profile_photo)
    ImageView dogPhoto;

    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    private DogBreed dogBreed;
    private ValueEventListener valueEventListener;
    private DatabaseReference databaseReference;
    private boolean expanded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dog_breed_detail);

        ButterKnife.bind(this);

        if (getIntent().hasExtra(PARCEL_DOG_BREED)) {
            Bundle data = getIntent().getExtras();
            dogBreed = data.getParcelable(PARCEL_DOG_BREED);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"onClickBackArrow");
                onBackPressed();
            }
        });

        new DownloadTask().execute();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(dogBreed.getName());
        
    }

    @OnClick(R.id.fab)
    public void addDogBreedToFavourite() {
        int tag = (int) fab.getTag();
        if (tag == R.drawable.ic_favorite_24dp) {
            ((FirebaseApplication) getApplication()).removeFavoriteRecord(dogBreed, FIREBASE_CHILD_FAVOURITE_DOG_BREEDS);
            fab.setTag(R.drawable.ic_favorite_border_24dp);
            fab.setImageResource(R.drawable.ic_favorite_border_24dp);
            DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.dog_breed_removed));
        } else {
            Map<String, String> isFavourite = new HashMap<>();
            isFavourite.put(DOG_BREED_ID, dogBreed.getId());
            ((FirebaseApplication) getApplication()).saveFavouriteDogBred(isFavourite, FIREBASE_CHILD_FAVOURITE_DOG_BREEDS);
            setFavouriteDogBreed();
            DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.dog_breed_added));
        }

    }

    @OnClick(R.id.character_layout)
    public void showCharacterDetail() {
        toggleTextView(tvCharacter, characterArrow);
    }

    @OnClick(R.id.care_layout)
    public void showCareDetail() {
        toggleTextView(tvCare, careArrow);
    }

    @OnClick(R.id.description_layout)
    public void showDescriptionDetail() {
        toggleTextView(tvDescription, descriptionArrow);
    }

    private void prepareTags(DogBreed dogBreed) {
        tagAge.setText(dogBreed.getAge());
        tagUsage.setText(dogBreed.getUsage());
        tagWeight.setText(dogBreed.getWeight());
    }

    private void prepareData(DogBreed dogBreed) {
        prepareTags(dogBreed);

        if (getIntent().hasExtra(PARAM_FAVOURITE)) {
            setFavouriteDogBreed();
        } else {
            fab.setTag(R.drawable.ic_favorite_border_24dp);
        }

        tvCharacter.setText(dogBreed.getCharacter());
        tvDescription.setText(dogBreed.getDescription());
        tvCare.setText(dogBreed.getCare());
        Picasso.with(this).load(dogBreed.getPhotoLink()).placeholder(R.drawable.no_dog).into(dogPhoto);

    }

    private void setFavouriteDogBreed() {
        fab.setTag(R.drawable.ic_favorite_24dp);
        fab.setImageResource(R.drawable.ic_favorite_24dp);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (valueEventListener != null) {
            databaseReference.removeEventListener(valueEventListener);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class DownloadTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            DialogHelper.showProgressDialog(DogBreedDetailActivity.this, getString(R.string.loading_data));
        }

        @Override
        protected Void doInBackground(Void... voids) {
            databaseReference = ((FirebaseApplication) getApplication()).getFirebaseDatabase().child(FIREBASE_CHILD_DOG_BREEDS).child(dogBreed.getId());
            valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    dogBreed = dataSnapshot.getValue(DogBreed.class);
                    prepareData(dogBreed);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    DialogHelper.showAlertDialog(DogBreedDetailActivity.this, getString(R.string.no_data_retrieved));
                }
            };

            databaseReference.addListenerForSingleValueEvent(valueEventListener);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            coordinatorLayout.setVisibility(View.VISIBLE);
            DialogHelper.hideProgressDialog();
        }
    }

    private void toggleTextView(ExpandableTextView expandableTextView, TextView title) {
        if (expanded) {
            title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_right_24dp, 0);
            expanded = false;
            expandableTextView.setMaxLines(ExpandableTextView.MAX_LINES);
        } else {
            title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more_black_24dp, 0);
            expanded = true;
            expandableTextView.setMaxLines(Integer.MAX_VALUE);
        }
    }
}
