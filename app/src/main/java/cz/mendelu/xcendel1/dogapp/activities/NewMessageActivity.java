//package cz.mendelu.xcendel1.dogapp.activities;
//
//import android.app.SearchManager;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.view.MenuItemCompat;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.SearchView;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.widget.TextView;
//
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.ValueEventListener;
//
//import java.text.Collator;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//import java.util.Locale;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
//import cz.mendelu.xcendel1.dogapp.Interfaces;
//import cz.mendelu.xcendel1.dogapp.R;
//import cz.mendelu.xcendel1.dogapp.adapters.UserAdapter;
//import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
//import cz.mendelu.xcendel1.dogapp.helpers.InitializationHelper;
//import cz.mendelu.xcendel1.dogapp.models.ChatPartner;
//import cz.mendelu.xcendel1.dogapp.models.ModelFactory;
//import cz.mendelu.xcendel1.dogapp.models.User;
//
//import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_CHAT_PARTNER;
//import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_USERS;
//import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_USER_FIELD_VISIBLE;
//
////public class NewMessageActivity extends AppCompatActivity implements Interfaces.ItemClickCallback, SearchView.OnQueryTextListener {
//    private static final String TAG = NewMessageActivity.class.getSimpleName();
//
//    @BindView(R.id.recycler_view) RecyclerView recyclerView;
//    @BindView(R.id.tv_no_users) TextView emptyView;
//
//    private UserAdapter userAdapter;
//    private List<User> userList = new ArrayList<>();
//    private DatabaseReference databaseReference;
//    private ValueEventListener valueEventListener;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_new_message);
//        ButterKnife.bind(this);
//
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
//
//        databaseReference = ((FirebaseApplication) getApplication()).getFirebaseDatabase();
//
//        valueEventListener = new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
//                    User dogBreed = postSnapshot.getValue(User.class);
//                    userList.add(dogBreed);
//                }
//                userAdapter.notifyDataSetChanged();
//
//                final Collator c = Collator.getInstance(new Locale("cs"));
////                Collections.sort(userList, c);
//
//                Collections.sort(userList, new Comparator<User>() {
//                    @Override
//                    public int compare(User user, User user2) {
//                        return c.compare(user.getName(), user2.getName());
//                    }
//                });
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
//                DialogHelper.showNoDataRetrieved(NewMessageActivity.this);
//            }
//        };
//
//        databaseReference.child(FIREBASE_CHILD_USERS).orderByChild(FIREBASE_USER_FIELD_VISIBLE).equalTo(true).addValueEventListener(valueEventListener);
//
//        userAdapter = new UserAdapter(userList);
//        userAdapter.setItemClickCallback(this);
//        InitializationHelper.setupRecyclerView(this, userAdapter, recyclerView);
//
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_dog_breeds, menu);
//
//        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
//        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        searchView.setIconifiedByDefault(true);
//        searchView.setOnQueryTextListener(this);
//
//        return true;
//    }
//
//    @Override
//    public void onItemClick(int pos) {
//        User user = userList.get(pos);
//        ChatPartner chatPartner = ModelFactory.createChatPartner(user.getId(), user.getName(), "");
//        Intent intent = new Intent(this, ChatDetailActivity.class);
//
//        intent.putExtra(PARCEL_CHAT_PARTNER, chatPartner);
//        startActivity(intent);
//    }
//
//
//    @Override
//    public boolean onQueryTextSubmit(String query) {
//        userAdapter.getFilter().filter(query);
//        userAdapter.setGenericList(userList);
//        return true;
//    }
//
//    @Override
//    public boolean onQueryTextChange(String newText) {
//        userAdapter.getFilter().filter(newText);
//        userAdapter.setGenericList(userList);
//        return true;
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//
//        if (valueEventListener != null) {
//            databaseReference.removeEventListener(valueEventListener);
//        }
//    }
//}
