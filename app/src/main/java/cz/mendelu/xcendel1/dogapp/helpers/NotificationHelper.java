package cz.mendelu.xcendel1.dogapp.helpers;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import java.util.concurrent.atomic.AtomicInteger;

import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.NotificationRetrofitGenerator;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.MapRetrofitGenerator;
import cz.mendelu.xcendel1.dogapp.activities.MainActivity;
import cz.mendelu.xcendel1.dogapp.alarm.DismissReceiver;
import cz.mendelu.xcendel1.dogapp.alarm.SnoozeReceiver;
import cz.mendelu.xcendel1.dogapp.models.MessageRequest;
import cz.mendelu.xcendel1.dogapp.models.MessageResponse;
import cz.mendelu.xcendel1.dogapp.models.Reminder;
import cz.mendelu.xcendel1.dogapp.models.ReminderCategoryEnum;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_MODEL_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_MY_PROFILE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_REMINDER;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.REMINDER_ID;

/**
 * Created by admin on 05.12.2016.
 *
 * A helper class relative to notification. It creates notifications.
 */

public class NotificationHelper {
    public static final String TAG = NotificationHelper.class.getSimpleName();

    /**
     * Creates a notification for reminders
     * @param context
     * @param reminder
     * @return
     */
    public static Notification createReminderNotification(Context context, Reminder reminder) {
        Log.d(TAG, "createReminderNotification");

        int notificationId = reminder.getId();
        int resource = ReminderCategoryEnum.getReminderResource(reminder.getCategory(), true);
        Bitmap bigIcon = BitmapFactory.decodeResource(context.getResources(), resource);

        NotificationCompat.Builder builder = getNotificationBuilder(context, reminder.getName(), reminder.getNote());
        builder.setLargeIcon(bigIcon);
        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.putExtra(PARAM_REMINDER, PARAM_REMINDER);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(notificationId, PendingIntent.FLAG_UPDATE_CURRENT);

        // add actions such as snooze and dismiss
        builder.addAction(android.R.color.transparent, context.getResources().getString(R.string.dismiss), createDismissIntent(context, reminder));
        builder.addAction(android.R.color.transparent, context.getResources().getString(R.string.snooze), createSnoozeIntent(context, reminder));

        builder.setContentIntent(pendingIntent);

        return builder.build();
    }

    /**
     * Creates notifications for communication among users
     * @param context
     * @param title
     * @param message
     * @return
     */
    public static NotificationCompat.Builder createMessageNotification(Context context, String title, String message){
        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.putExtra(PARAM_MY_PROFILE, PARAM_MY_PROFILE);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(NotificationID.getID(), PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = getNotificationBuilder(context, title, message);
        builder.setContentIntent(pendingIntent);

        return builder;
    }

    /**
     * Retuns instance of builder
     * @param context
     * @param title
     * @param message
     * @return
     */
    private static NotificationCompat.Builder getNotificationBuilder(Context context, String title, String message) {
        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(context);
        // ask for sound
        String ringtonePreference = preference.getString("notification_ringtone", "DEFAULT_SOUND");

        NotificationCompat.Builder builder  =
                (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_paw_24dp)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setWhen(System.currentTimeMillis())
                        .setOnlyAlertOnce(true)
                        .setColor(ContextCompat.getColor(context, R.color.primary))
                        .setSound(Uri.parse(ringtonePreference))
                        .setAutoCancel(true);

        if (preference.getBoolean("notification_vibrate", true)) {
            // vibration
            builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
        }

        return builder;
    }

    /**
     * Creates dismiss intent
     * @param context
     * @param reminder
     * @return
     */
    private static PendingIntent createDismissIntent(Context context, Reminder reminder) {
        int notificationId = reminder.getId();
        Intent dismissIntent = new Intent(context, DismissReceiver.class);
        dismissIntent.putExtra(REMINDER_ID, notificationId);
        dismissIntent.putExtra(PARAM_MODEL_ID, reminder.getId());

        return PendingIntent.getBroadcast(context, notificationId, dismissIntent, 0);
    }

    /**
     * Creates snooze intent
     * @param context
     * @param reminder
     * @return
     */
    private static PendingIntent createSnoozeIntent(Context context, Reminder reminder) {
        int notificationId = reminder.getId();
        Intent snoozeIntent = new Intent(context, SnoozeReceiver.class);
        snoozeIntent.putExtra(REMINDER_ID, notificationId);

        return PendingIntent.getBroadcast(context, notificationId, snoozeIntent, 0);
    }


    public static class NotificationID {
        private final static AtomicInteger c = new AtomicInteger(0);
        public static int getID() {
            return c.incrementAndGet();
        }
    }

    /**
     * Sends a notification such as walk invitation, request dog's user attention
     *
     * @param context
     * @param senderId
     * @param dogId
     * @param title
     * @param body
     */
    public static void sendDogNotification(Context context, String receiverId, String senderId, String dogId, String title, String body) {
        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(context);
        if (preference.getBoolean("notifications", true)) {
            Interfaces.MessageInterface service = NotificationRetrofitGenerator.createService(Interfaces.MessageInterface.class);

            Call<MessageResponse> call = service.sendDogNotification(new MessageRequest(receiverId, senderId, dogId, title, body));

            call.enqueue(new Callback<MessageResponse>() {
                @Override
                public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                    Log.d(TAG, "Success invitation");

                }

                @Override
                public void onFailure(Call<MessageResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + call.toString());
                }

            });
        }

    }



}



