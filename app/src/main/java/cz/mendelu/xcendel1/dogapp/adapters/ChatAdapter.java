//package cz.mendelu.xcendel1.dogapp.adapters;
//
//import android.view.ViewGroup;
//
//import java.util.List;
//
//import cz.mendelu.xcendel1.dogapp.models.Chat;
//import cz.mendelu.xcendel1.dogapp.viewholders.ChatViewHolder;
//
///**
// * Created by admin on 08.11.2016.
// */
//
//public class ChatAdapter extends BaseAdapter<Chat, ChatViewHolder> {
//    public ChatAdapter(List<Chat> genericList) {
//        super(genericList);
//    }
//
//    @Override
//    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        return new ChatViewHolder(parent, getItemClickCallback());
//    }
//
////    private List<Chat> chatList;
////    private ItemClickCallback itemClickCallback;
////
////    public ChatAdapter(List<Chat> chatList) {
////        this.chatList = chatList;
////    }
////
////    @Override
////    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
////        View itemView = LayoutInflater.from(parent.getContext())
////                .inflate(R.layout.chat_list_item, parent, false);
////        return new ChatAdapter.MyViewHolder(itemView);
////    }
////
////    @Override
////    public void onBindViewHolder(MyViewHolder holder, int position) {
////        Chat chat = chatList.get(position);
////        ChatPartner chatPartner = chat.getChatPartner();
////        holder.userName.setText(chatPartner.getName());
////        holder.message.setText(chat.getLastMessage());
//////        holder.timeOfMessage.setText(DateUtils.getTimeOfMessage(chat.getLastMessageCreatedAt()));
////
////        if (chatPartner.getPhotoLink() != null) {
////
//////
//////
//////            Bitmap bitmap = ImageUtils.decodeBitmap(chatPartner.getPhotoLink());
//////            holder.userPhoto.setImageBitmap(bitmap);
////        }
////    }
////
////    public void setItemClickCallback(ItemClickCallback itemClickCallback) {
////        this.itemClickCallback = itemClickCallback;
////    }
////
////    @Override
////    public int getItemCount() {
////        return chatList.size();
////    }
////
////    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
////        @BindView(R.id.user_name) TextView userName;
////        @BindView(R.id.last_message) TextView message;
////        @BindView(R.id.user_photo) CircleImageView userPhoto;
////        @BindView(R.id.time_message) TextView timeOfMessage;
////
////        public MyViewHolder(View view) {
////            super(view);
////            ButterKnife.bind(this, view);
////);
////        }
////
////        @Override
////        public void onClick(View view) {
////            if (view.getId() == R.id.cont_item_root) {
////                itemClickCallback.onItemClick(getAdapterPosition());
////            }
////        }
////    }
//}
