package cz.mendelu.xcendel1.dogapp;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.ApplozicClient;
import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.PushNotificationTask;
import com.applozic.mobicomkit.api.account.user.UserLoginTask;
import com.applozic.mobicomkit.api.account.user.UserService;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.mendelu.xcendel1.dogapp.activities.LoginActivity;
import cz.mendelu.xcendel1.dogapp.activities.MainActivity;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.models.DogBreed;
import cz.mendelu.xcendel1.dogapp.models.GenericModel;
import cz.mendelu.xcendel1.dogapp.models.Memory;
import cz.mendelu.xcendel1.dogapp.models.ModelFactory;
import cz.mendelu.xcendel1.dogapp.models.User;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import cz.mendelu.xcendel1.dogapp.utils.UserUtils;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.DOG_BREED_ID;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOGS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_FRIENDSHIPS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_FRIENDSHIPS_REQUESTS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_MEMORIES;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_USERS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.REQUESTED_ID;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.REQUESTING_ID;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_DOG_FRIENDS;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_NAME;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_PHOTO_LINK;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOG_FRIENDS_PATH;

/**
 * Created by Petra Cendelinova on 10/29/2016.
 *
 * The base of this app. It covers work with Firebase.
 */

public class FirebaseApplication extends MultiDexApplication {
    private static final String TAG = FirebaseApplication.class.getSimpleName();

    public static final String STORAGE_URL = "gs://dogapp-60aef.appspot.com";
    private FirebaseAuth firebaseAuth;
    private DatabaseReference firebaseDatabase;
    private StorageReference firebaseStorage;
    private UserLoginTask authTask = null;
    boolean isLogged = false;

    /**
     * Returns FirebaseAuth
     *
     * @return FirebaseAuth
     */
    public FirebaseAuth getFirebaseAuth() {
        if (firebaseAuth == null) {
            firebaseAuth = FirebaseAuth.getInstance();
        }
        return firebaseAuth;
    }

    /**
     * Return Firebase database reference
     * @return DatabaseReference
     */
    public DatabaseReference getFirebaseDatabase() {
        if (firebaseDatabase == null) {
            firebaseDatabase = FirebaseDatabase.getInstance().getReference();
        }
        return firebaseDatabase;
    }

    /**
     * Returns Firebase Storage reference
     * @return StorageReference
     */
    public StorageReference getFirebaseStorage() {
        if (firebaseStorage == null) {
            firebaseStorage = FirebaseStorage.getInstance().getReferenceFromUrl(STORAGE_URL);
        }

        return firebaseStorage;
    }

    /**
     * Gets the UID of the current user
     * @return String
     */
    public String getFirebaseUserId() {
        String userId = null;
        if (getFirebaseAuth().getCurrentUser() != null) {
            userId = getFirebaseAuth().getCurrentUser().getUid();
        }
        return userId;
    }

    /**
     * Checks if the current user is logged or not. If does, it will add an Applozic user to inializate the chat
     *
     * @param context
     * @return boolean, true for a logged user
     */
    public boolean checkUserLogin(final Context context) {

        final FirebaseUser firebaseUser = getFirebaseAuth().getCurrentUser();
        isLogged = firebaseUser != null ? true : false;

        if (isLogged == false) {
            return isLogged;
        }
        final UserLoginTask.TaskListener listener = new UserLoginTask.TaskListener() {

            @Override
            public void onSuccess(RegistrationResponse registrationResponse, final Context context) {
                authTask = null;
                ApplozicClient.getInstance(context).setContextBasedChat(true).setHandleDial(true);
                PushNotificationTask.TaskListener pushNotificationTaskListener = new PushNotificationTask.TaskListener() {
                    @Override
                    public void onSuccess(RegistrationResponse registrationResponse) {
                    }

                    @Override
                    public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                    }
                };
                PushNotificationTask pushNotificationTask = new PushNotificationTask(Applozic.getInstance(context).getDeviceRegistrationId(), pushNotificationTaskListener, context);
                pushNotificationTask.execute((Void) null);

                isLogged = true;

            }

            @Override
            public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                isLogged = false;
                DialogHelper.showAlertDialog(context, context.getResources().getString(R.string.unsuccessful_login));
            }
        };

        SharedPreferences sharedPref = getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedPref.contains(SHARED_PREFERENCES_NAME + getFirebaseUserId())) {
            // get information about user from shared preferences
            String name = sharedPref.getString(SHARED_PREFERENCES_NAME + getFirebaseUserId(), "");
            String photoLink = "";
            if (sharedPref.contains(SHARED_PREFERENCES_PHOTO_LINK + getFirebaseUserId())) {
                photoLink = sharedPref.getString(SHARED_PREFERENCES_PHOTO_LINK + getFirebaseUserId(), "");
            }
            createApplozicUser(context, firebaseUser.getUid(), firebaseUser.getEmail(), name, photoLink, listener);
        } else {
            // if information isn't saved in shared preferences, get them from Firebase
            getFirebaseDatabase().child(FIREBASE_CHILD_USERS).child(getFirebaseUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User receivedUser = dataSnapshot.getValue(User.class);
                    createApplozicUser(context, firebaseUser.getUid(), firebaseUser.getEmail(), receivedUser.getName(), receivedUser.getPhoto(), listener);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


        return isLogged;
    }

    /**
     * Creates a new user and saves to database, stores user's profile photo
     *
     * @param context
     * @param email
     * @param password
     * @param name
     * @param photo
     */
    public void createNewUser(final Context context, String email, String password, final String name, final String photo) {
        DialogHelper.showProgressDialog(context, getString(R.string.progress_dialog_title_signup));
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener((Activity) context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail: onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            FirebaseExceptionsChecker.checkFirebaseAuth(context, task);
                        } else {
                            onAuthSuccess(context, name, task.getResult().getUser(), photo);
                        }

                        DialogHelper.hideProgressDialog();
                    }
                });

    }

    /**
     * Provides the basic saving user to the Firebase database, shared preferences and saving photos to the external storage
     * @param context
     * @param name
     * @param firebaseUser
     * @param photo
     */
    private void onAuthSuccess(Context context, String name, FirebaseUser firebaseUser, String photo) {
        saveNewUser(name, firebaseUser, photo);
        if (firebaseUser != null) {
            //Stores information to shared preferences
            UserUtils.storeInfoAboutUser(context, name, firebaseUser, photo);
        }
        storeUserProfilePhotoToStorage(context, firebaseUser.getUid(), ImageUtils.getImageAsBytes(photo));
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    /**
     * Logs an user with given email and password. Before that inputs of user are validated
     * @param context
     * @param email
     * @param password
     */
    public void loginUser(final Context context, String email, String password) {
        DialogHelper.showProgressDialog(context, getString(R.string.progress_dialog_title_login));
        getFirebaseAuth().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener((Activity) context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "signInWithEmail", task.getException());
                            FirebaseExceptionsChecker.checkFirebaseAuth(context, task);
                        } else {
                            if (checkUserLogin(context)) {
                                // if the logging was successful, it checks it and inializates chat
                                String userId = task.getResult().getUser().getUid();
                                Log.d(TAG, "loginUser: user with id " + userId + "was logged" );
                                // start FCM
                                FirebaseMessaging.getInstance().subscribeToTopic(userId);
                                context.startActivity(new Intent(context, MainActivity.class));
                            }
                        }
                        DialogHelper.hideProgressDialog();
                    }
                });
    }

    /**
     * Provides sign out of users
     * @param context
     */
    public void signOutUser(Context context) {
        getFirebaseAuth().signOut();
        LoginManager.getInstance().logOut();
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    /**
     * Saves a new record to the Firebase Database
     * @param model - generic model has ID property
     * @param typeChild - specifying of child in the JSON tree
     * @return
     */
    public String saveNewRecord(GenericModel model, String typeChild) {
        // get key
        String id = getFirebaseDatabase().child(typeChild).push().getKey();
        model.setId(id);
        getFirebaseDatabase().child(typeChild).child(getFirebaseUserId()).child(id).setValue(model);

        if (model instanceof Dog) {
            getFirebaseDatabase().child(typeChild).child(getFirebaseUserId()).child(id).child(FIREBASE_CHILD_PHOTO).setValue(((Dog) model).getPhoto());
        }

        if (model instanceof Memory) {
            ((Memory) model).setUserId(getFirebaseUserId());
            getFirebaseDatabase().child(typeChild).child(getFirebaseUserId()).child(((Memory) model).getDogId()).child(id).setValue(model);
            //saves the memory photo
            getFirebaseDatabase().child(typeChild).child(getFirebaseUserId()).child(((Memory) model).getDogId()).child(id).
                    child(FIREBASE_CHILD_PHOTO).setValue(((Memory) model).getPhoto());
        }
        return id;
    }


    /**
     * Removes a memory of the specified dog
     * @param memory
     * @param dogId
     */
    public void removeExistingRecord(Memory memory, String dogId) {
        getFirebaseDatabase().child(FIREBASE_CHILD_MEMORIES).child(getFirebaseUserId()).child(dogId).child(memory.getId()).removeValue();
    }

    /**
     * Saves or edits a single value of the specified dog of an user
     * @param value - saved or edited value
     * @param dogId
     * @param typeChild
     * @param typeChild2 which value is should be changed
     */
    public void saveOrEditSingleValue(Object value, String dogId, String typeChild, String typeChild2) {
        Map<String, Object> valueMap = new HashMap<String, Object>();
        valueMap.put(typeChild2, value);
        getFirebaseDatabase().child(typeChild).child(getFirebaseUserId()).child(dogId).updateChildren(valueMap);
    }

    /**
     * Saves or edits user's favourite dog breed, it saves in format ID_DOG_BREED, true
     * @param map
     * @param typeChild
     */
    public void saveFavouriteDogBred(Map<String, String> map, String typeChild) {
        getFirebaseDatabase().child(typeChild).child(getFirebaseUserId()).child(map.get(DOG_BREED_ID)).setValue(map);
    }

    /**
     * Removes the dog breed from the user's favourite list
     * @param dogBreed
     * @param typeChild
     */
    public void removeFavoriteRecord(DogBreed dogBreed, String typeChild) {
        getFirebaseDatabase().child(typeChild).child(getFirebaseUserId()).child(dogBreed.getId()).removeValue();
    }

    /**
     * Removes friendship between two dogs and removes its photo from the external storage
     * @param context
     * @param dogId
     * @param dogFriendId
     */
    public void removeFriendship(Context context, String dogId, String dogFriendId) {
        getFirebaseDatabase().child(FIREBASE_CHILD_FRIENDSHIPS).child(dogFriendId).child(dogId).removeValue();
        getFirebaseDatabase().child(FIREBASE_CHILD_FRIENDSHIPS).child(dogId).child(dogFriendId).removeValue();
        ImageUtils.removeImageFromExternalStorage(context, STORAGE_IMAGES_DOG_FRIENDS_PATH, dogFriendId + ".jpg");
    }

    /**
     *  Removes dog and its memories, friendship requests and friendships
     *
     * @param dogId
     * @param dogFriendIds
     * @param friendshipRequests
     */
    public void removeDogCascade(Context context, String dogId, List<String> dogFriendIds, List<String> friendshipRequests) {
        getFirebaseDatabase().child(FIREBASE_CHILD_FRIENDSHIPS).child(dogId).removeValue();
        SharedPreferences.Editor editor = UserUtils.getSharedPreferencesEditor(context);
        SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);

        for (String dogFriendId : dogFriendIds) {
            removeFriendship(context, dogId, dogFriendId);
            //retrieving my dog friend's friends if it is my dog
            Set<String> setOfFriendIds = sharedPref.getStringSet(SHARED_PREFERENCES_DOG_FRIENDS + dogFriendId, Collections.<String>emptySet());
            //ask if my dog contains, removes it
            setOfFriendIds.remove(dogId);
            editor.remove(SHARED_PREFERENCES_DOG_FRIENDS + dogFriendId);
            editor.commit();
            //update my dog friend's friends
            UserUtils.saveDogFriends(context, dogFriendId, setOfFriendIds);
        }

        for (String friendshipRequest : friendshipRequests) {
            cancelFriendshipRequest(friendshipRequest);
        }
        getFirebaseDatabase().child(FIREBASE_CHILD_MEMORIES).child(getFirebaseUserId()).child(dogId).orderByChild("dogId").equalTo(dogId).getRef().removeValue();
        getFirebaseDatabase().child(FIREBASE_CHILD_DOGS).child(getFirebaseUserId()).child(dogId).removeValue();
    }

    /**
     * Cancels friendship requests in cases when they become friends or requests is removed.
     *
     * @param whereClause - requestingDogId_requestedId (requestingDogId the dog that has sent the request,
     *                    requestedIdt he dog that has accept the the request )
     */
    public void cancelFriendshipRequest(String whereClause) {
        getFirebaseDatabase().child(FIREBASE_CHILD_FRIENDSHIPS_REQUESTS).child(whereClause).removeValue();
    }

    /**
     * Edits a model in database, if it is POJO dog then the photo has to be set separately because of the annotation exclude
     * @param model
     * @param typeChild
     */
    public void editExistingRecord(GenericModel model, String typeChild) {
        getFirebaseDatabase().child(typeChild).child(getFirebaseUserId()).child(model.getId()).setValue(model);
        if (model instanceof Dog) {
            getFirebaseDatabase().child(typeChild).child(getFirebaseUserId()).child(model.getId()).child(FIREBASE_CHILD_PHOTO).setValue(((Dog) model).getPhoto());
        }
        if (model instanceof Memory) {
            Memory memory = (Memory) model;
            getFirebaseDatabase().child(FIREBASE_CHILD_MEMORIES).child(getFirebaseUserId()).child(memory.getDogId()).child(memory.getId()).setValue(memory);
            getFirebaseDatabase().child(FIREBASE_CHILD_MEMORIES).child(getFirebaseUserId()).child(memory.getDogId()).child(memory.getId()).child(FIREBASE_CHILD_PHOTO).setValue(memory.getPhoto());
        }

    }


    /**
     * Saves to database a new friendship after accepting and removes it from requests.
     * It creates two records for requesting dog and requested dog for easier manipulation.
     *
     * @param child
     * @param requestingDogId - the dog that has sent the friendship request
     * @param requestedDogId - the dog that has been sent the friendship request
     */
    public void saveFriendship(String child, String requestingDogId, String requestedDogId) {
        Map<String, Object> friendshipMap = new HashMap<>();
        //save from requestingDog
        friendshipMap.put(requestedDogId, true);
        getFirebaseDatabase().child(child).child(requestingDogId).updateChildren(friendshipMap);
        friendshipMap.clear();
        //save from requestedDog
        friendshipMap.put(requestingDogId, true);
        getFirebaseDatabase().child(child).child(requestedDogId).updateChildren(friendshipMap);
        //cancel friendship request
        String whereClause = requestingDogId + "_" + requestedDogId;
        cancelFriendshipRequest(whereClause);

    }

    /**
     * Saves a new friendship request to database. It sets request id according to given requestedDogId and requestingDogId.
     *
     * @param child
     * @param requestedDogId
     * @param requestingDogId
     */
    public void requestNewFriendship(String child, String requestingDogId, String requestedDogId) {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put(REQUESTED_ID, requestedDogId);
        requestMap.put(REQUESTING_ID, requestingDogId);

        getFirebaseDatabase().child(child).child(requestingDogId + "_" + requestedDogId).setValue(requestMap);
    }

    /**
     * It creates Applozic User by given parameters from Firebase database or Shared preferences if they exit
     *  @param id
     * @param email
     * @param name
     * @param photoLink
     * @param listener
     */
    private void createApplozicUser(Context context, String id, String email, String name, String photoLink, UserLoginTask.TaskListener listener) {
        com.applozic.mobicomkit.api.account.user.User user = new com.applozic.mobicomkit.api.account.user.User();
        user.setUserId(id);
        user.setDisplayName(name);
        user.setEmail(email);
        if (!photoLink.isEmpty()) {
            user.setImageLink(photoLink);
        }

        authTask = new UserLoginTask(user, listener, getApplicationContext());
        authTask.execute((Void) null);
    }

    /**
     * Saves a new user to database
     *
     * @param name
     * @param firebaseUser
     * @param photo
     */
    public void saveNewUser(String name, FirebaseUser firebaseUser, String photo) {
        User user = ModelFactory.createUser(firebaseUser.getUid(), photo, firebaseUser.getEmail(), name);
        getFirebaseDatabase().child(FIREBASE_CHILD_USERS).child(firebaseUser.getUid()).setValue(user);
    }

    /**
     * Uploads user's profile photo to Firebase storage and sets photo link for the current user in database
     *
     * @param userId
     * @param photoBaos
     */
    public void storeUserProfilePhotoToStorage(final Context context, final String userId, final byte[] photoBaos) {
        StorageReference imagesRef = getFirebaseStorage().child(FIREBASE_CHILD_USERS).child(userId);

        if (photoBaos != null) {
            UploadTask uploadTask = imagesRef.putBytes(photoBaos);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.e(TAG, "Image was not saved " + exception.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    if (downloadUrl != null) {
                        Map<String, Object> photoMap = new HashMap<>();
                        photoMap.put(FIREBASE_CHILD_PHOTO, downloadUrl.toString());
                        getFirebaseDatabase().child(FIREBASE_CHILD_USERS).child(userId).updateChildren(photoMap);
                        UserUtils.storeSingleRecordAboutUser(context, SHARED_PREFERENCES_PHOTO_LINK + userId, downloadUrl.toString());
                        UserService.getInstance(context).updateDisplayNameORImageLink(null, downloadUrl.toString(), null, null);

                    }
                }
            });
        }


    }


}
