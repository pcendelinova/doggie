package cz.mendelu.xcendel1.dogapp.models;

/**
 * Created by Petra Cendelinova on 10/29/2016.
 *
 * User model
 */

public class User extends GenericModel {
    private String name;
    private String email;
    private String photo;

    public User() {
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User(String id, String photo, String email, String name) {
        this.id = id;
        this.photo = photo;
        this.email = email;
        this.name = name;
    }
}
