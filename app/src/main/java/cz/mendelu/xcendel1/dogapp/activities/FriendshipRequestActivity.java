package cz.mendelu.xcendel1.dogapp.activities;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.adapters.FriendshipRequestAdapter;
import cz.mendelu.xcendel1.dogapp.comparators.DogComparator;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.helpers.InitializationHelper;
import cz.mendelu.xcendel1.dogapp.models.ActionType;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.utils.UserUtils;
import cz.mendelu.xcendel1.dogapp.viewholders.FriendshipState;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOGS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_FRIENDSHIPS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_FRIENDSHIPS_REQUESTS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.REQUESTED_ID;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.REQUESTING_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_ID;

/**
 * It is a list of friendship requests.
 */
public class FriendshipRequestActivity extends AppCompatActivity implements Interfaces.DogButtonClickCallback, Interfaces.ItemClickCallback {
    private static final String TAG = FriendshipRequestActivity.class.getSimpleName();

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_no_requests)
    TextView emptyView;

    private FriendshipRequestAdapter friendshipRequestAdapter;
    private List<Dog> requestingFriendsList = new ArrayList<>();
    private List<String> requestingIds = new ArrayList<>();
    private DatabaseReference databaseReference;
    private ValueEventListener valueEventListener;
    private ValueEventListener valueEventListenerForDogs;
    private String dogId;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friendship_request);

        ButterKnife.bind(this);

        if (getIntent().hasExtra(PARAM_DOG_ID)) {
            // pass my dog id from MyProfileActivity
            dogId = getIntent().getExtras().getString(PARAM_DOG_ID);
        }

        DialogHelper.showProgressDialog(this, getString(R.string.loading_data));
        databaseReference = ((FirebaseApplication) getApplication()).getFirebaseDatabase();
        userId = ((FirebaseApplication) getApplication()).getFirebaseUserId();

        // gets the dog requesting ids
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                requestingIds.clear();
                if (!dataSnapshot.hasChildren()) {
                    emptyView.setVisibility(View.VISIBLE);
                    DialogHelper.hideProgressDialog();
                } else {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        String requestingId = (String) postSnapshot.child(REQUESTING_ID).getValue();
                        requestingIds.add(requestingId);
                    }
                    if (!requestingIds.isEmpty()) {
                        databaseReference.child(FIREBASE_CHILD_DOGS).addListenerForSingleValueEvent(createListenerForDogs());
                    } else {
                        DialogHelper.hideProgressDialog();
                    }

                }



                Log.i(TAG, "onDataChange: new friends requests retrieved");

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i(TAG, "onCancelled: no data retrieved " + databaseError.getMessage());
            }
        };


        friendshipRequestAdapter = new FriendshipRequestAdapter(this, requestingFriendsList);
        friendshipRequestAdapter.setDogButtonClickCallback(this);
        friendshipRequestAdapter.setItemClickCallback(this);

        databaseReference.child(FIREBASE_CHILD_FRIENDSHIPS_REQUESTS).orderByChild(REQUESTED_ID).equalTo(dogId).addValueEventListener(valueEventListener);

        // setups recycler view
        InitializationHelper.setupRecyclerView(this, friendshipRequestAdapter, recyclerView);
    }


    @Override
    public void clickOnButton(int pos, ActionType actionType) {
        Dog newFriendDog = requestingFriendsList.get(pos);
        switch (actionType){
            case ACCEPT_FRIENDSHIP_REQUEST:
                // saves a new friendship
                ((FirebaseApplication) getApplication()).saveFriendship(FIREBASE_CHILD_FRIENDSHIPS, newFriendDog.getId(), dogId);
                DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.new_friend_added));
                // removes from requesting friend list
                requestingFriendsList.remove(newFriendDog);
                friendshipRequestAdapter.notifyDataSetChanged();
                break;
            case DECLINE_FRIENDSHIP_REQUEST:
                String whereClause = newFriendDog.getId() + "_" + dogId;
                // cancels a friendship request
                ((FirebaseApplication) getApplication()).cancelFriendshipRequest(whereClause);
                DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.declined_friendship));
                requestingFriendsList.remove(newFriendDog);
                friendshipRequestAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onItemClick(int pos) {
        DialogHelper.showAlertDialog(this, getString(R.string.no_permission_see_dog_profile));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    private ValueEventListener createListenerForDogs() {
        valueEventListenerForDogs = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                requestingFriendsList.clear();
                if (requestingIds.isEmpty()) {
                    emptyView.setVisibility(View.VISIBLE);
                } else {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        for (DataSnapshot dogSnapshot : postSnapshot.getChildren()) {
                            Dog dog = dogSnapshot.getValue(Dog.class);
                            if (requestingIds.contains(dog.getId())) {
                                String photo = (String) dogSnapshot.child(FIREBASE_CHILD_PHOTO).getValue();
                                dog.setPhoto(photo);
                                dog.setFriendshipState(FriendshipState.DECLINE_STATE);
                                requestingFriendsList.add(dog);
                            }
                        }
                    }
                    Collections.sort(requestingFriendsList, new DogComparator());
                    friendshipRequestAdapter.notifyDataSetChanged();

                }

                DialogHelper.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        return valueEventListenerForDogs;
    }



}
