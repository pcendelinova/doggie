package cz.mendelu.xcendel1.dogapp.comparators;

import java.util.Comparator;

import cz.mendelu.xcendel1.dogapp.models.Reminder;
import cz.mendelu.xcendel1.dogapp.utils.DateUtils;

/**
 * Created by admin on 08.12.2016.
 *
 * Comparator for reminder according to date and time
 */

public class ReminderComparator implements Comparator<Reminder> {
    @Override
    public int compare(Reminder reminder, Reminder reminder2) {
        return DateUtils.compareTwoDates(reminder.getDate() + " " + reminder.getTime(),
                reminder2.getDate() + " " + reminder2.getTime(), ReminderComparator.class.getSimpleName());
    }
}
