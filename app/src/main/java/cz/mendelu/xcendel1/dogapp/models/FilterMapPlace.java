package cz.mendelu.xcendel1.dogapp.models;

/**
 * Created by admin on 19.12.2016.
 *
 * Enum that defines types of filtering on a map
 */

public enum FilterMapPlace {
    PET_SHOP("Chovatelské potřeby"),
    PARK("Park"),
    EXERCISE_AREA("Cvičiště"),
    GROOMER("Psí salón"),
    SHELTER("Psí útulek"),
    VET("Veterinář"),
    HOTEL("Psí hotel");

    private String name;

    FilterMapPlace(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String[] getValues() {

        FilterMapPlace[] options = values();
        String[] optionsString = new String[options.length];

        for (int i = 0; i < options.length; i++) {
            optionsString[i] = options[i].getName();
        }

        return optionsString;
    }
}
