package cz.mendelu.xcendel1.dogapp.alarm;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.models.Reminder;

import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.REMINDER_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_MODEL_ID;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_REMINDERS;

/**
 * Created by admin on 06.12.2016.
 *
 * It dismisses notification
 */

public class DismissReceiver extends BroadcastReceiver {
    public static final String TAG = DismissReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.hasExtra(REMINDER_ID) && intent.hasExtra(PARAM_MODEL_ID)) {
            int notificationId = intent.getIntExtra(REMINDER_ID, 0);
            ReminderDatabase reminderDatabase = new ReminderDatabase(context);
            reminderDatabase.deleteReminder(reminderDatabase.getReminder(notificationId));
            Log.d(TAG, "Cancel notification");
            // cancel notification from status bar
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationId);
            Intent notificationIntent = new Intent(context, AlarmReceiver.class);
            // cancel notifying
            PendingIntent cancelIntent = PendingIntent.getBroadcast(context, notificationId, notificationIntent, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(cancelIntent);

        }
    }
}
