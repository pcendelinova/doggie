package cz.mendelu.xcendel1.dogapp.annotation;

import android.support.annotation.StringRes;

import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import cz.mendelu.xcendel1.dogapp.constants.SettingsConstants;
import cz.mendelu.xcendel1.dogapp.rule.FutureDateRule;
import cz.mendelu.xcendel1.dogapp.rule.PastDateRule;

/**
 * Created by admin on 22.12.2016.
 */

@ValidateUsing(FutureDateRule.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FutureDate {

    String dateFormat() default SettingsConstants.DATE_FORMAT_DMY;

    @StringRes int messageResId() default -1;

    String message() default "Datum musí být v budoucnosti";

    public int sequence() default -1;

}
