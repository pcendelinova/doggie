package cz.mendelu.xcendel1.dogapp.viewholders;

import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.models.User;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by admin on 14.11.2016.
 * It will be user for implementing my own chat
 */

//public class UserViewHolder extends BaseViewHolder<User> {
//    @BindView(R.id.user_name) TextView userName;
//    @BindView(R.id.user_photo) CircleImageView userProfilePhoto;
//
//    public UserViewHolder(ViewGroup parent, Interfaces.ItemClickCallback itemClickCallback) {
//        super(parent, R.layout.user_list_item, itemClickCallback);
//    }
//
//    @Override
//    protected void onBind(User item, int position) {
//        userName.setText(item.getName());
//        if (!item.getPhoto().isEmpty()) {
//            Bitmap bitmap = ImageUtils.decodeBitmap(item.getPhoto());
//            userProfilePhoto.setImageBitmap(bitmap);
//        }
//    }
//}
