package cz.mendelu.xcendel1.dogapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.activities.MyDogProfileActivity;
import cz.mendelu.xcendel1.dogapp.activities.NewDogActivity;
import cz.mendelu.xcendel1.dogapp.adapters.DogAdapter;
import cz.mendelu.xcendel1.dogapp.comparators.DogComparator;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.helpers.InitializationHelper;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_DOG;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOGS;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOGS_PATH;

/**
 * A screen that presents the home page of the user
 */
public class HomeFragment extends Fragment implements Interfaces.ItemClickCallback {

    private static final String TAG = HomeFragment.class.getSimpleName();

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_no_dogs)
    TextView emptyView;

    private DogAdapter dogAdapter;
    private List<Dog> dogList = new ArrayList<>();
    private DatabaseReference databaseReference;
    private Unbinder unbinder;
    private ValueEventListener valueEventListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        DialogHelper.showProgressDialog(getContext(), getString(R.string.loading_data));

        databaseReference = ((FirebaseApplication) getActivity().getApplication()).getFirebaseDatabase();
        final String userId = ((FirebaseApplication) getActivity().getApplication()).getFirebaseUserId();

        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dogList.clear();
                if (!dataSnapshot.hasChildren()) {
                    emptyView.setVisibility(View.VISIBLE);
                } else {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Dog myDog = postSnapshot.getValue(Dog.class);
                        String imagePath = userId + STORAGE_IMAGES_DOGS_PATH + myDog.getId();
                        if(!ImageUtils.checkIfImageExits(getContext(), imagePath)) {
                            String photo = (String) postSnapshot.child(FIREBASE_CHILD_PHOTO).getValue();
                            myDog.setPhoto(photo);
                            ImageUtils.saveImageToExternalStorage(getActivity(), ImageUtils.decodeBitmap(myDog.getPhoto()),
                                    STORAGE_IMAGES_DOGS_PATH, myDog.getId() + ".jpg");
                        }
                        dogList.add(myDog);
                    }
                    emptyView.setVisibility(View.GONE);
                    Collections.sort(dogList, new DogComparator());
                    dogAdapter.notifyDataSetChanged();
                }
                DialogHelper.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        databaseReference.child(FIREBASE_CHILD_DOGS).child(userId).addValueEventListener(valueEventListener);

        dogAdapter = new DogAdapter(getActivity(), dogList);
        dogAdapter.setItemClickCallback(this);

        InitializationHelper.setupRecyclerView(getActivity(), dogAdapter, recyclerView);

        return view;
    }

    @OnClick(R.id.fab)
    public void createNewDog() {
        startActivity(new Intent(getActivity(), NewDogActivity.class));
    }


    @Override
    public void onItemClick(int pos) {
        Dog dog = dogAdapter.getItem(pos);
        Intent intent = new Intent(getActivity(), MyDogProfileActivity.class);
        intent.putExtra(PARCEL_DOG, dog);
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (valueEventListener != null) {
            databaseReference.removeEventListener(valueEventListener);
        }
    }

}
