package cz.mendelu.xcendel1.dogapp.adapters;

import android.app.Activity;
import android.view.ViewGroup;

import java.util.List;

import cz.mendelu.xcendel1.dogapp.models.Memory;
import cz.mendelu.xcendel1.dogapp.viewholders.MemoryViewHolder;

/**
 * Created by admin on 13.11.2016.
 */

public class MemoryAdapter extends BaseAdapter<Memory, MemoryViewHolder> {
    private Activity activity;
    public MemoryAdapter(Activity activity, List<Memory> genericList) {
        super(genericList);
        this.activity = activity;
    }


    @Override
    public MemoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MemoryViewHolder(activity, parent, getItemClickCallback());
    }
}
