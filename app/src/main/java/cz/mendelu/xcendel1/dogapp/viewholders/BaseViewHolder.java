package cz.mendelu.xcendel1.dogapp.viewholders;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import cz.mendelu.xcendel1.dogapp.Interfaces;

/**
 * Created by admin on 14.11.2016.
 */

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder implements View.OnClickListener {

    private T item;
    private Interfaces.ItemClickCallback itemClickCallback;

    protected BaseViewHolder(ViewGroup parent, @LayoutRes int itemLayoutId, Interfaces.ItemClickCallback itemClickCallback) {
        super(LayoutInflater.from(parent.getContext()).inflate(itemLayoutId, parent, false));

        this.itemClickCallback = itemClickCallback;
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);

    }

    public final void performBind(T item, int position) {
        this.item = item;
        onBind(item, position);
    }

    protected abstract void onBind(T item, int position);


    @Override
    public void onClick(View view) {
        itemClickCallback.onItemClick(getAdapterPosition());
    }
}
