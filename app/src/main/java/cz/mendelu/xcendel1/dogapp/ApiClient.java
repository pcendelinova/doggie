package cz.mendelu.xcendel1.dogapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import cz.mendelu.xcendel1.dogapp.models.MarkerEnum;
import cz.mendelu.xcendel1.dogapp.models.Place;
import cz.mendelu.xcendel1.dogapp.models.PlaceResponse;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cz.mendelu.xcendel1.dogapp.models.MarkerEnum.EXERCISE_AREA;
import static cz.mendelu.xcendel1.dogapp.models.MarkerEnum.GROOMER;
import static cz.mendelu.xcendel1.dogapp.models.MarkerEnum.HOTEL;
import static cz.mendelu.xcendel1.dogapp.models.MarkerEnum.PARK;
import static cz.mendelu.xcendel1.dogapp.models.MarkerEnum.PET_SHOP;
import static cz.mendelu.xcendel1.dogapp.models.MarkerEnum.SHELTER;
import static cz.mendelu.xcendel1.dogapp.models.MarkerEnum.VET;

/**
 * Created by admin on 21.11.2016.
 *
 * API client for work with the map
 */
public class ApiClient implements GoogleMap.OnInfoWindowClickListener {

    private static final String TAG = ApiClient.class.getSimpleName();

    public static void retrieveMapData(final MarkerEnum type, boolean isSearchedByText, final GoogleMap googleMap, Location currentLocation, final Context context) {
        int PROXIMITY_RADIUS = 50000;

        Interfaces.PlacesInterface service = MapRetrofitGenerator.createService(Interfaces.PlacesInterface.class);
        Call<PlaceResponse> call;
        if (isSearchedByText) {
//            todo current location cant be null
            call = service.getNearbyPlaces(type.getType(), currentLocation.getLatitude() + "," + currentLocation.getLongitude(), PROXIMITY_RADIUS);
        } else {
            call = service.getNearbyPlacesByText(type.getType(), currentLocation.getLatitude() + "," + currentLocation.getLongitude(), PROXIMITY_RADIUS);

        }

        call.enqueue(new Callback<PlaceResponse>() {
            @Override
            public void onResponse(Call<PlaceResponse> call, Response<PlaceResponse> response) {
                List<Place> places = response.body().getResults();
                Log.d(TAG, "Number of places received: " + places.size());
                setMarkers(type, places, googleMap, context);

            }

            @Override
            public void onFailure(Call<PlaceResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + call.toString());
            }

        });

    }

    private static void setMarkers(MarkerEnum type, List<Place> places, GoogleMap map, final Context context) {
        for (Place place : places) {
            MarkerOptions markerOptions = new MarkerOptions();
            double longitude = place.getGeometry().getLocation().getLng();
            double latitude = place.getGeometry().getLocation().getLat();
            LatLng latLng = new LatLng(latitude, longitude);

            switch (type) {
                case PARK:
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageUtils.resizeMarkers(PARK.getIcon(), context)));
                    break;
                case PET_SHOP:
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageUtils.resizeMarkers(PET_SHOP.getIcon(), context)));
                    break;
                case VET:
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageUtils.resizeMarkers(VET.getIcon(), context)));
                    break;
                case GROOMER:
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageUtils.resizeMarkers(GROOMER.getIcon(), context)));
                    break;
                case EXERCISE_AREA:
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageUtils.resizeMarkers(EXERCISE_AREA.getIcon(), context)));
                    break;
                case SHELTER:
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageUtils.resizeMarkers(SHELTER.getIcon(), context)));
                    break;
                case HOTEL:
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageUtils.resizeMarkers(HOTEL.getIcon(), context)));
                    break;
            }

            markerOptions.position(latLng);
            if (place.getName() != null && !place.getName().isEmpty()) {
                markerOptions.title(place.getName());
            }
            if (place.getVicinity() != null && !place.getVicinity().isEmpty()) {
                markerOptions.snippet(place.getVicinity() + "\n");
            }


            if (place.getOpeningHours() != null && place.getOpeningHours().getOpenNow() != null) {

                if (place.getOpeningHours().getOpenNow()) {
                    markerOptions.snippet("Právě otevřeno");
                } else {
                    markerOptions.snippet("Zavřeno");
                }
            }
            map.addMarker(markerOptions);

            map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    LinearLayout info = new LinearLayout(context);
                    info.setOrientation(LinearLayout.VERTICAL);

                    TextView title = new TextView(context);
                    title.setTextColor(Color.BLACK);
                    title.setGravity(Gravity.CENTER);
                    title.setTypeface(null, Typeface.BOLD);
                    title.setText(marker.getTitle());

                    TextView snippet = new TextView(context);
                    snippet.setTextColor(Color.GRAY);
                    snippet.setText(marker.getSnippet());

//                    AppCompatButton button = new AppCompatButton(context);
//                    button.setText(context.getString(R.string.show_place_detail));
//                    button.setBackgroundColor(Color.TRANSPARENT);
//                    button.setTextColor(ContextCompat.getColor(context, R.color.primary));

                    info.addView(title);
                    info.addView(snippet);
//                    info.addView(button);

                    return info;
                }
            });


        }


    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        marker.getSnippet();

    }


}
