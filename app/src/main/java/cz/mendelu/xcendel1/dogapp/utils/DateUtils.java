package cz.mendelu.xcendel1.dogapp.utils;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Months;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;

import cz.mendelu.xcendel1.dogapp.comparators.MemoryComparator;
import cz.mendelu.xcendel1.dogapp.comparators.ReminderComparator;
import cz.mendelu.xcendel1.dogapp.constants.SettingsConstants;

import static cz.mendelu.xcendel1.dogapp.constants.SettingsConstants.DATE_FORMAT_DMY;
import static cz.mendelu.xcendel1.dogapp.constants.SettingsConstants.DATE_TIME_FORMAT;

/**
 * Created by admin on 11.11.2016.
 *
 * Utils for work with date
 */

public class DateUtils {
    private static final String TAG = DateUtils.class.getSimpleName();

    /**
     * Returns a dog age in its profile, i.e. Ma dva roky, 3 mesice
     *
     * @param dogBirthday
     * @return dog age
     * @throws ParseException
     */
    public static String getDogAge(String dogBirthday) throws ParseException {
        String ageString = "";
        if (!dogBirthday.equals("")) {
            DateTimeFormatter dtf = DateTimeFormat.forPattern(SettingsConstants.DATE_FORMAT_DMY);
            LocalDate birthday = dtf.parseLocalDate(dogBirthday);
            LocalDate now = new LocalDate();

            ageString = hasBirthday(birthday, now);
            if (ageString.isEmpty()) {
                int months = Months.monthsBetween(birthday, now).getMonths();
                double age = (double) months / 12.0;
                if (age < 1) {
                    LocalDate newEnd = now.minusMonths(months);
                    int days = Days.daysBetween(birthday, newEnd).getDays();
                    if (months == 0) {
                        switch (days) {
                            case 1:
                                ageString = days + " den";
                                break;
                            case 2:case 3:case 4:
                                ageString = days + " dny";
                                break;
                            default:
                                ageString = days + " dnů";
                        }
                    } else if (months == 1) {
                        switch (days) {
                            case 0:
                                ageString = months + " měsíc";
                                break;
                            case 1:
                                ageString = months + " měsíc " + days + " den";
                                break;
                            case 2:case 3:case 4:
                                ageString = months + " měsíc " + days + " dny";
                                break;
                            default:
                                ageString = months + " měsíc " + days + " dnů";
                        }
                    } else if (months >= 2 && months <= 4) {
                        switch (days) {
                            case 0:
                                ageString = months + " měsíce ";
                                break;
                            case 1:
                                ageString = months + " měsíce " + days + " den";
                                break;
                            case 2:case 3:case 4:
                                ageString = months + " měsíce " + days + " dny";
                                break;
                            default:
                                ageString = months + " měsíce " + days + " dnů";
                        }
                    } else ageString = months + " měsíců " + days + " dnů";
                } else {
                    DecimalFormat df = new DecimalFormat("#.#");
                    String formattedAge = df.format(age);
                    switch (formattedAge) {
                        case "1":
                            ageString = formattedAge + " rok";
                            break;
                        case "2":
                        case "3":
                        case "4":
                            ageString = formattedAge + " roky";
                            break;
                        default:
                            ageString = df.format(age) + " roků";
                    }
                }
            }
        }



        return ageString;
    }

    public static boolean isToday(String date) {
        return android.text.format.DateUtils.isToday(convertStringToDate(date).getMillis());
    }

    public static boolean isAfterToday(String date) {
        return convertStringToDate(date).isAfterNow();

    }


    /**
     * Converts the given date string with time to Joda DateTime
     *
     * @param dateWithTime
     * @return
     */
    public static DateTime convertStringToDateTime(String dateWithTime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_TIME_FORMAT);
        DateTime dateTime = formatter.parseDateTime(dateWithTime);

        return dateTime;
    }

//    check if the time is in the future
    public static boolean isTimeAfterNow(String time) {
        DateTime date = new DateTime().withTime(LocalTime.parse(time));

//        my time is in future
        return date.isAfterNow();

    }


    /**
     * Convert the given date string to the date without time
     * @param date
     * @return
     */
    public static DateTime convertStringToDate(String date) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT_DMY);
        DateTime convertedDate = formatter.parseDateTime(date);

        return convertedDate;
    }

    /**
     * Returns time in millis
     * @param dateTime
     * @return
     */
    public static long getDateTimeInMillis(String dateTime) {
        return convertStringToDateTime(dateTime).getMillis();
    }

    /**
     * Creates the specified title for date and time for reminders, i.e. Zitra v 15:05
     *
     * @param date
     * @param time
     * @return
     */
    public static String createDateStringForReminder(String date, String time) {
        DateTime givenDateTime = convertStringToDateTime(date + " " + time);
        DateTime now = new DateTime();

        String dateTimeTitle = "";
        int days = Days.daysBetween(new LocalDate(now), new LocalDate(givenDateTime)).getDays();
        int years = givenDateTime.getYear() - now.getYear();

        if (days >= 0 && days <= 6 && years == 0) {
            switch (days) {
                case 0:
                    Period p = new Period(now, givenDateTime);
                    long hours = p.getHours();
                    long minutes = p.getMinutes();

                    if (hours > 0 || (hours == 0 && minutes >= 0 )) {
                        dateTimeTitle = "Dnes v " + time;
                    } else {
                        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd. MM.");
                        dateTimeTitle = "Už proběhlo - dnes " + " v " + time;
                    }
                    break;
                case 1:
                    dateTimeTitle = "Zítra v " + time;
                    break;
                default:
                    DateTime.Property dayOfWeek = givenDateTime.dayOfWeek();
                    String dayLongName = dayOfWeek.getAsText(new Locale("cs"));
                    dateTimeTitle = dayLongName + " v " + time;
                    break;
            }
        } else if (days >= 7 && years == 0) {
            DateTimeFormatter fmt = DateTimeFormat.forPattern("dd. MM.");
            dateTimeTitle = fmt.print(givenDateTime) + " v " + time;
        } else if (years > 0) {
            dateTimeTitle = date + " " + time;
        } else if (days < 0 && years == 0) {
            DateTimeFormatter fmt = DateTimeFormat.forPattern("dd. MM.");
            dateTimeTitle = "Už proběhlo - " + fmt.print(givenDateTime) + " v " + time;
        } else if (days < 0 && years > 0) {
            DateTimeFormatter fmt = DateTimeFormat.forPattern(DATE_TIME_FORMAT);
            dateTimeTitle = "Už proběhlo - " + fmt.print(givenDateTime);
        }

        return dateTimeTitle;
    }

    /**
     * Compares two dates
     * @param dateTime
     * @param dateTime2
     * @param typeComparator
     * @return
     */
    public static int compareTwoDates(String dateTime, String dateTime2, String typeComparator) {
        DateTime date1 = null, date2 = null;
        if (typeComparator.equals(ReminderComparator.class.getSimpleName())) {
            date1 = convertStringToDateTime(dateTime);
            date2 = convertStringToDateTime(dateTime2);
        } else if (typeComparator.equals(MemoryComparator.class.getSimpleName())) {
            date1 = convertStringToDate(dateTime);
            date2 = convertStringToDate(dateTime2);
        }

        return date2.compareTo(date1);
    }

    /**
     * Check if the dog has birthday
     * @param birthday
     * @param now
     * @return
     */
    private static String hasBirthday(LocalDate birthday, LocalDate now) {
        String ageString = "";
        int years = now.getYear() - birthday.getYear();
        int months = now.getMonthOfYear() - birthday.getMonthOfYear();
        int days = now.getDayOfMonth() - birthday.getDayOfMonth();
        if (years > 0 && months == 0 && days == 0) {
            switch (years) {
                case 1:
                    ageString = "Mám dnes narozeniny - " + years + " rok";
                    break;
                case 2:
                case 3:
                case 4:
                    ageString = "Mám dnes narozeniny - " + years + " roky";
                    break;
                default:
                    ageString = "Mám dnes narozeniny - " + years + " roků";
            }


        }

        if (years == 0 && months == 0 && days == 0) {
            ageString = "Právě se narodilo";
        }

        return ageString;
    }

    public static boolean isTheSameDay(DateTime day, DateTime day2) {
        boolean isSameDay = false;
        if (day.getYear() == day2.getYear() && day.getDayOfMonth() == day2.getDayOfMonth() && day.getMonthOfYear() == day2.getMonthOfYear()) {
            isSameDay = true;
        }

        return isSameDay;
    }
}
