package cz.mendelu.xcendel1.dogapp.models;

import cz.mendelu.xcendel1.dogapp.R;

/**
 * Created by admin on 23.11.2016.
 *
 * Enum for markers. First parameter presents what should be look for and second one is type of icon
 */

public enum MarkerEnum {
    PET_SHOP("pet_store", R.drawable.ic_shop_marker),
    HOTEL("psíhotel", R.drawable.ic_hotel_marker),
    GROOMER("psísalón", R.drawable.ic_groomer_marker),
    PARK("park", R.drawable.ic_park_marker),
    EXERCISE_AREA("cvičiště", R.drawable.ic_training_marker),
    VET("veterinary_care", R.drawable.ic_vet_marker),
    SHELTER("psíutulek", R.drawable.ic_shelter_marker),
    COMPETITION("competition", R.drawable.ic_competition_marker);

    private String type;
    private int icon;

    MarkerEnum(String type, int icon) {
        this.type = type;
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
