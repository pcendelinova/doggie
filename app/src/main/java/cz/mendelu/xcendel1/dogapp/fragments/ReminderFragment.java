package cz.mendelu.xcendel1.dogapp.fragments;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.activities.NewReminderActivity;
import cz.mendelu.xcendel1.dogapp.adapters.ReminderAdapter;
import cz.mendelu.xcendel1.dogapp.alarm.AlarmReceiver;
import cz.mendelu.xcendel1.dogapp.alarm.ReminderDatabase;
import cz.mendelu.xcendel1.dogapp.comparators.ReminderComparator;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.helpers.InitializationHelper;
import cz.mendelu.xcendel1.dogapp.models.Reminder;

import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_REMINDER;

/**
 * A screen that shows user's created reminders
 */
public class ReminderFragment extends Fragment implements Interfaces.ItemClickCallback, Interfaces.ReminderCheckBoxCallBack {

    private static final String TAG = ReminderFragment.class.getSimpleName();

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_no_reminders)
    TextView emptyView;
    @BindString(R.string.loading_data) String loadingMessage;

    private ReminderAdapter reminderAdapter;
    private List<Reminder> reminderList = new ArrayList<>();
    private Set<Reminder> checkedReminders = new HashSet<>();
    private ReminderDatabase reminderDatabase;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_reminder, container, false);
        ButterKnife.bind(this, view);

        PreferenceManager.setDefaultValues(getActivity(), R.xml.settings, false);


        reminderDatabase = new ReminderDatabase(getActivity());
        reminderList = reminderDatabase.getAllReminders();
        Collections.sort(reminderList, new ReminderComparator());

        reminderAdapter = new ReminderAdapter(reminderList);
        reminderAdapter.setItemClickCallback(this);
        reminderAdapter.setReminderCheckBoxCallBack(this);

        if (reminderList.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
        }

        InitializationHelper.setupRecyclerView(getActivity(), reminderAdapter, recyclerView);
        checkedReminders.clear();

        return view;
    }


    @OnClick(R.id.fab)
    public void addNewReminder() {
        startActivity(new Intent(getActivity(), NewReminderActivity.class));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_more:
                View menuItemView = getActivity().findViewById(R.id.action_more);
                PopupMenu popupMenu = DialogHelper.createPopupMenu(getContext(), menuItemView, R.menu.popup_menu_reminder);
                popupMenu.show();
                registerPopupActions(popupMenu);
                return true;
            default:
                break;
        }
        return false;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_reminder, menu);
    }

    private void registerPopupActions(PopupMenu popupMenu) {
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_share:
                        DialogHelper.showShareChooser(getContext(), getContentToShare());
                        return true;
                    case R.id.action_remove:
                        if (checkedReminders.size() >= 1) {
                            AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                            for (Reminder checkedReminder : checkedReminders) {
                                Intent notificationIntent = new Intent(getContext(), AlarmReceiver.class);
                                PendingIntent cancelIntent = PendingIntent.getBroadcast(getContext(), checkedReminder.getId(), notificationIntent, 0);
                                alarmManager.cancel(cancelIntent);
                                reminderDatabase.deleteReminder(checkedReminder);
                            }
                            reminderList.removeAll(checkedReminders);
                            reminderAdapter.setGenericList(reminderList);
                            reminderAdapter.notifyDataSetChanged();
                        }
                        DialogHelper.showMessageAboutReminders(coordinatorLayout, checkedReminders.size());
                        checkedReminders.clear();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    /**
     * Prepares content to share
     * @return
     */
    private String getContentToShare() {
        StringBuilder content = new StringBuilder();
        content.append(getResources().getText(R.string.my_reminders));
        content.append(System.getProperty("line.separator"));
        for (Reminder reminder : checkedReminders) {
            content.append(reminder.getName());
            content.append(" ");
            content.append(reminder.getDate());
            content.append(" ");
            content.append(reminder.getTime());
            content.append(System.getProperty("line.separator"));
        }

        return content.toString();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onItemClick(int pos) {
        Reminder reminder = reminderList.get(pos);
        Intent intent = new Intent(getActivity(), NewReminderActivity.class);
        intent.putExtra(PARCEL_REMINDER, reminder);
        startActivity(intent);
    }

    @Override
    public void addCheckBox(int pos) {
        Reminder reminder = reminderList.get(pos);
        if (checkedReminders.contains(reminder)) {
            checkedReminders.remove(reminder);
        } else {
            checkedReminders.add(reminder);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        reminderList = reminderDatabase.getAllReminders();

        if (reminderList.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }

        reminderAdapter.setGenericList(reminderList);
        reminderAdapter.notifyDataSetChanged();
    }


    @Override
    public void onPause() {
        super.onPause();

        reminderList = reminderDatabase.getAllReminders();
        reminderAdapter.setGenericList(reminderList);

    }
}
