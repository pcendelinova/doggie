package cz.mendelu.xcendel1.dogapp.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

/**
 * Created by admin on 13.11.2016.
 *
 * Memory model
 */

public class Memory extends GenericModel implements Parcelable {
    private String name, place, date, note, dogId, userId;
    private boolean updatedPhoto;
    @Exclude
    private String photo;


    public Memory() {
    }

    public Memory(String id, String name, String place, String date, String photo, String note) {
        super(id);
        this.name = name;
        this.place = place;
        this.date = date;
        this.photo = photo;
        this.note = note;
        this.updatedPhoto = false;
    }


    protected Memory(Parcel in) {
        super(in);
        name = in.readString();
        place = in.readString();
        date = in.readString();
        note = in.readString();
        dogId = in.readString();
        userId = in.readString();
        updatedPhoto = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(name);
        dest.writeString(place);
        dest.writeString(date);
        dest.writeString(note);
        dest.writeString(dogId);
        dest.writeString(userId);
        dest.writeByte((byte) (updatedPhoto ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Memory> CREATOR = new Creator<Memory>() {
        @Override
        public Memory createFromParcel(Parcel in) {
            return new Memory(in);
        }

        @Override
        public Memory[] newArray(int size) {
            return new Memory[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Exclude
    public String getPhoto() {
        return photo;
    }

    @Exclude
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDogId() {
        return dogId;
    }

    public void setDogId(String dogId) {
        this.dogId = dogId;
    }

    public boolean isUpdatedPhoto() {
        return updatedPhoto;
    }

    public void setUpdatedPhoto(boolean updatedPhoto) {
        this.updatedPhoto = updatedPhoto;
    }

    @Override
    public String toString() {
        return "Memory{" +
                "name='" + name + '\'' +
                ", place='" + place + '\'' +
                ", date='" + date + '\'' +
                ", photo='" + photo + '\'' +
                ", note='" + note + '\'' +
                '}';
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
