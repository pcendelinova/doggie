package cz.mendelu.xcendel1.dogapp.rule;

import com.mobsandgeeks.saripaar.AnnotationRule;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.mendelu.xcendel1.dogapp.annotation.FutureTime;
import cz.mendelu.xcendel1.dogapp.constants.SettingsConstants;
import cz.mendelu.xcendel1.dogapp.utils.DateUtils;

/**
 * Created by admin on 10.12.2016.
 */

public class FutureTimeRule extends AnnotationRule<FutureTime, String> {

    protected FutureTimeRule(FutureTime futureTime) {
        super(futureTime);
    }

    @Override
    public boolean isValid(String dateString) {
        DateFormat dateFormat = new SimpleDateFormat(SettingsConstants.FORMAT_TIME);
        Date parsedDate = null;

        try {
            parsedDate = dateFormat.parse(dateString);
        } catch (ParseException ignored) {}

        return parsedDate != null && dateString.equals(dateFormat.format(parsedDate)) && DateUtils.isTimeAfterNow(dateString);
    }

}
