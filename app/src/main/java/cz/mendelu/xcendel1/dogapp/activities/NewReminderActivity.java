package cz.mendelu.xcendel1.dogapp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.exception.ConversionException;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.adapters.ReminderSpinnerAdapter;
import cz.mendelu.xcendel1.dogapp.alarm.ReminderDatabase;
import cz.mendelu.xcendel1.dogapp.alarm.ReminderManager;
import cz.mendelu.xcendel1.dogapp.annotation.FutureDate;
import cz.mendelu.xcendel1.dogapp.annotation.FutureTime;
import cz.mendelu.xcendel1.dogapp.constants.SettingsConstants;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.models.ModelFactory;
import cz.mendelu.xcendel1.dogapp.models.Reminder;
import cz.mendelu.xcendel1.dogapp.models.ReminderCategoryEnum;
import cz.mendelu.xcendel1.dogapp.utils.DateUtils;
import cz.mendelu.xcendel1.dogapp.utils.ModelUtils;
import cz.mendelu.xcendel1.dogapp.utils.MyTextWatcherUtils;
import fr.ganfra.materialspinner.MaterialSpinner;

import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_REMINDER;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_REMINDER;
import static cz.mendelu.xcendel1.dogapp.constants.SettingsConstants.SPLASH_DISPLAY_LENGTH;

/**
 * A screen that creates a new user's reminder that will be triggered at a specified time
 */
public class NewReminderActivity extends AppCompatActivity implements Validator.ValidationListener, View.OnTouchListener {

    private static final String TAG = NewDogActivity.class.getSimpleName();
    public static final int NOTIFICATION_ID = 1;

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @NotEmpty(messageResId = R.string.error_required_field)
    @BindView(R.id.input_layout_name)
    TextInputLayout inputLayoutName;

    @FutureDate(dateFormat = SettingsConstants.DATE_FORMAT_DMY, messageResId = R.string.reminder_date_cant_be_in_past)
    @BindView(R.id.input_layout_date)
    TextInputLayout inputLayoutDate;

    @FutureTime(messageResId = R.string.time_format)
    @BindView(R.id.input_layout_time)
    TextInputLayout inputLayoutTime;
    @BindView(R.id.spinner)
    MaterialSpinner spinner;
    @BindView(R.id.input_layout_note)
    TextInputLayout inputLayoutNote;

    @BindView(R.id.btn_add_reminder)
    AppCompatButton addButton;

    private List<TextInputLayout> inputLayoutList;
    private Validator validator;
    private Reminder receivedReminder;
    boolean edit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_reminder);
        ButterKnife.bind(this);
        setupReminderSpinner();

        validator = new Validator(this);
        validator.setValidationListener(this);

        validator.registerAdapter(TextInputLayout.class,
                new ViewDataAdapter<TextInputLayout, String>() {
                    @Override
                    public String getData(TextInputLayout textInputLayout) throws ConversionException {
                        return ModelUtils.getProperty(textInputLayout);
                    }
                }
        );
        Validator.registerAnnotation(FutureDate.class);
        Validator.registerAnnotation(FutureTime.class);


        inputLayoutList = Arrays.asList(inputLayoutName, inputLayoutDate, inputLayoutTime);

        MyTextWatcherUtils.setTextWatcherListeners(inputLayoutList, validator);

        if (getIntent().hasExtra(PARCEL_REMINDER)) {
            Bundle data = getIntent().getExtras();
            receivedReminder = data.getParcelable(PARCEL_REMINDER);
            setupData(receivedReminder);
            edit = true;
            setTitle(getString(R.string.title_activity_edit_reminder));
            addButton.setText(getString(R.string.title_activity_edit_reminder));
        }

        addButton.setOnTouchListener(this);


    }

    /**
     * Setups reminder options such as walk, shopping (@see ReminderCategoryEnum)
     */
    private void setupReminderSpinner() {
        ReminderSpinnerAdapter reminderSpinnerAdapter = new ReminderSpinnerAdapter(this,
                R.layout.reminder_spinner_item, ReminderCategoryEnum.getCategoryEnumList());
        spinner.setAdapter(reminderSpinnerAdapter);
        spinner.setPaddingSafe(0, 0, 0, 0);

    }

    @OnFocusChange({R.id.input_layout_date, R.id.input_date})
    public void showCalendar() {
        DialogHelper.showDatePicker(NewReminderActivity.this, inputLayoutDate);
    }

    @OnClick({R.id.input_layout_date, R.id.input_date})
    public void showCalendarOnClick() {
        DialogHelper.showDatePicker(NewReminderActivity.this, inputLayoutDate);
    }


    @OnFocusChange({R.id.input_layout_time, R.id.input_time})
    public void showTimePicker() {
        DialogHelper.showTimePicker(NewReminderActivity.this, inputLayoutTime, inputLayoutDate);
    }

    @OnClick({R.id.input_time, R.id.input_layout_time})
    public void showTimePickerOnClick() {
        DialogHelper.showTimePicker(NewReminderActivity.this, inputLayoutTime, inputLayoutDate);
    }

    @OnClick(R.id.btn_add_reminder)
    public void addReminder() {
        if (DateUtils.isToday(ModelUtils.getProperty(inputLayoutDate)) && !DateUtils.isTimeAfterNow(ModelUtils.getProperty(inputLayoutTime))) {
            inputLayoutTime.setError(getString(R.string.reminder_time_must_be_future));
            return;
        }
        int reminderId = 0;
        Reminder reminder = ModelFactory.createReminder(inputLayoutName, inputLayoutDate, inputLayoutTime,
                inputLayoutNote, spinner);
        ReminderDatabase reminderDatabase = new ReminderDatabase(this);

        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(this);
        if (edit) {
            reminder.setId(receivedReminder.getId());
            reminderDatabase.updateReminder(reminder);
            reminderId = receivedReminder.getId();
            DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.success_edit_reminder));
        } else {
            reminderId = reminderDatabase.addReminder(reminder);
            DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.success_add_reminder));
        }


        if (preference.getBoolean("notification_reminders", true)) {
            String dateTimeString = reminder.getDate() + " " + reminder.getTime();
            new ReminderManager(this).setReminder(reminderId, DateUtils.getDateTimeInMillis(dateTimeString));
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent reminderIntent = new Intent(NewReminderActivity.this, MainActivity.class);
                reminderIntent.putExtra(PARAM_REMINDER, PARAM_REMINDER);
                startActivity(reminderIntent);

            }
        }, SPLASH_DISPLAY_LENGTH);


    }

    /**
     * If validation is succeeded, it enables the add button
     */
    @Override
    public void onValidationSucceeded() {
        addButton.setEnabled(true);
        addButton.setAlpha(1);
        MyTextWatcherUtils.clearAllInputs(inputLayoutList);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        addButton.setAlpha(.5f);
        addButton.setEnabled(false);

        MyTextWatcherUtils.clearAllInputs(inputLayoutList);
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof TextInputLayout) {
                ((TextInputLayout) view).setErrorEnabled(true);
                ((TextInputLayout) view).setError(message);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Prepares a filled form to edit the existing reminder
     *
     * @param reminder
     */
    private void setupData(Reminder reminder) {
        ModelUtils.setProperty(inputLayoutName, reminder.getName());
        ModelUtils.setProperty(inputLayoutDate, reminder.getDate());
        ModelUtils.setProperty(inputLayoutTime, reminder.getTime());
        ModelUtils.setProperty(inputLayoutNote, reminder.getNote());

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.reminder_list_row, null);

        ImageView icon = ButterKnife.findById(view, R.id.icon);
        icon.setImageResource(ReminderCategoryEnum.getReminderResource(reminder.getCategory(), true));
        ReminderSpinnerAdapter reminderSpinnerAdapter = new ReminderSpinnerAdapter(this,
                ReminderCategoryEnum.getReminderResource(reminder.getCategory(), false),
                ReminderCategoryEnum.getCategoryEnumList(), ReminderCategoryEnum.getReminderResource(reminder.getCategory(), true));
        spinner.setAdapter(reminderSpinnerAdapter);

    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (view.getId() == R.id.btn_add_reminder) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                addButton.setAlpha(.5f);
            } else {
                addButton.setAlpha(1f);
            }
            return false;
        }
        return true;
    }


}
