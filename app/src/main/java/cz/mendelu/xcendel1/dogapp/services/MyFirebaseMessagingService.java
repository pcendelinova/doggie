package cz.mendelu.xcendel1.dogapp.services;


import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.applozic.mobicomkit.api.notification.MobiComPushReceiver;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.File;

import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.helpers.NotificationHelper;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import cz.mendelu.xcendel1.dogapp.utils.UserUtils;

import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOG_FRIENDS_PATH;

/**
 * Represents FCM
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "onMessageReceived: From: " + remoteMessage.getData().get("senderId"));

        NotificationCompat.Builder builder = NotificationHelper.createMessageNotification(this, remoteMessage.getData().get("title"),
                remoteMessage.getData().get("message"));
        String userId = ((FirebaseApplication) getApplication()).getFirebaseUserId();
        String imagePath = userId + STORAGE_IMAGES_DOG_FRIENDS_PATH + remoteMessage.getData().get("dogId") + ".jpg";
        if (ImageUtils.checkIfImageExits(this, imagePath)) {
            // if dog image exists, use it from the external storage
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            File filePath = getExternalCacheDir();
            File myDir = new File(filePath.getAbsolutePath() + "/" + imagePath);
            Bitmap photo = BitmapFactory.decodeFile(myDir.getAbsolutePath(), options);
            builder.setLargeIcon(Bitmap.createScaledBitmap(photo, 64, 64, false));

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(NotificationHelper.NotificationID.getID(), builder.build());

        } else if (ImageUtils.DownloadingPhoto.isDownloadingDone()) {
            // otherwise download from Firebase
            ImageUtils.DownloadingPhoto.downloadDogPhoto(this, userId, remoteMessage.getData().get("dogId"), builder);
        } else {
            // otherwise set user profile photo
            UserUtils.setProfilePhoto(this, remoteMessage.getData().get("senderId"), builder);
        }


        if (MobiComPushReceiver.isMobiComPushNotification(remoteMessage.getData())) {
            // notify message notification
            MobiComPushReceiver.processMessageAsync(this, remoteMessage.getData());
            return;
        }
    }

}


