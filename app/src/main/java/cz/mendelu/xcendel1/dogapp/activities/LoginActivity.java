
package cz.mendelu.xcendel1.dogapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.exception.ConversionException;

import java.util.Arrays;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.helpers.FontHelper;
import cz.mendelu.xcendel1.dogapp.utils.ModelUtils;
import cz.mendelu.xcendel1.dogapp.utils.MyTextWatcherUtils;
import cz.mendelu.xcendel1.dogapp.utils.UserUtils;

import static cz.mendelu.xcendel1.dogapp.constants.SettingsConstants.CUSTOM_FONT;

;

/**
 * A login screen that offers login via email/password or via facebook
 */
public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener, View.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int REQUEST_SIGNUP = 0;
    private Validator validator;
    private List<TextInputLayout> inputLayoutList;
    private FirebaseAuth firebaseAuth;
    private CallbackManager callbackManager;

    @NotEmpty(messageResId = R.string.error_required_field)
    @Email(messageResId = R.string.error_invalid_email)
    @BindView(R.id.input_layout_email) TextInputLayout inputLayoutEmail;

    @NotEmpty(messageResId = R.string.error_required_field)
    @BindView(R.id.input_layout_password) TextInputLayout inputLayoutPassword;

    @BindView(R.id.btn_login) Button loginButton;
    @BindView(R.id.btn_facebook) LoginButton facebookButton;
    @BindView(R.id.action_signup) TextView signupLink;
    @BindView(R.id.action_forgotten_password) TextView forgottenPasswordLink;
    @BindView(R.id.app_name) TextView appName;

    @BindString(R.string.error_invalid_email) String invalidEmailMessage;
    @BindString(R.string.error_incorrect_password) String incorrectPasswordMessage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        firebaseAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        signupLink.setOnClickListener(this);
        forgottenPasswordLink.setOnClickListener(this);
        validator = new Validator(this);
        validator.setValidationListener(this);

        validator.registerAdapter(TextInputLayout.class,
                new ViewDataAdapter<TextInputLayout, String>() {
                    @Override
                    public String getData(TextInputLayout flet) throws ConversionException {
                        return flet.getEditText().getText().toString();
                    }
                }
        );


        inputLayoutList = Arrays.asList(inputLayoutEmail,inputLayoutPassword);
        MyTextWatcherUtils.setTextWatcherListeners(inputLayoutList, validator);

        FontHelper.setFont(this, appName, CUSTOM_FONT);

        loginButton.setEnabled(false);

        callbackManager = CallbackManager.Factory.create();
        facebookButton.setReadPermissions("email", "public_profile");
        facebookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                DialogHelper.showAlertDialog(LoginActivity.this, getString(R.string.occur_error));
            }
        });
    }


    @OnClick(R.id.btn_login)
    public void login() {
        ((FirebaseApplication) getApplication()).loginUser(LoginActivity.this, ModelUtils.getProperty(inputLayoutEmail), ModelUtils.getProperty(inputLayoutPassword));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        DialogHelper.showProgressDialog(this, getString(R.string.progress_dialog_title_login));
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential", task.getException());
                            DialogHelper.showAlertDialog(LoginActivity.this, getString(R.string.unsuccessful_login));
                        } else {
                            String userId = task.getResult().getUser().getUid();
                            FirebaseMessaging.getInstance().subscribeToTopic(userId);
                            if (!UserUtils.checkIfUserInfoIsSaved(LoginActivity.this, userId)) {
                                UserUtils.storeInfoAboutUser(LoginActivity.this, task.getResult().getUser().getDisplayName(),
                                        task.getResult().getUser(), null);
                            }
                            if (((FirebaseApplication) getApplication()).checkUserLogin(LoginActivity.this)) {
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            }
                        }

                        DialogHelper.hideProgressDialog();
                    }
                });
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    public void onValidationSucceeded() {
        loginButton.setAlpha(1);
        loginButton.setEnabled(true);
        MyTextWatcherUtils.clearAllInputs(inputLayoutList);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        MyTextWatcherUtils.clearAllInputs(inputLayoutList);
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof TextInputLayout) {
                ((TextInputLayout) view).setError(message);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.action_signup) {
            Intent intent = new Intent(this, SignupActivity.class);
            startActivityForResult(intent, REQUEST_SIGNUP);

        } else if (view.getId() == R.id.action_forgotten_password) {
            Intent intent = new Intent(this, ResetPasswordActivity.class);
            startActivity(intent);
        }
    }


}

