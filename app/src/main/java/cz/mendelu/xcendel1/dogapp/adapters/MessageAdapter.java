//package cz.mendelu.xcendel1.dogapp.adapters;
//
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import java.util.List;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import cz.mendelu.xcendel1.dogapp.R;
//import cz.mendelu.xcendel1.dogapp.models.Message;
//import cz.mendelu.xcendel1.dogapp.utils.DateUtils;
//import cz.mendelu.xcendel1.dogapp.viewholders.MessageViewHolder;
//import de.hdodenhof.circleimageview.CircleImageView;
//
///**
// * Created by admin on 08.11.2016.
// */
//
//public class MessageAdapter extends BaseAdapter<Message, MessageViewHolder>{
//
//    public MessageAdapter(List<Message> genericList) {
//        super(genericList);
//    }
//
//    @Override
//    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        return new MessageViewHolder(parent, getItemClickCallback());
//    }
//}
