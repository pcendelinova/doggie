package cz.mendelu.xcendel1.dogapp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.applozic.mobicomkit.api.account.user.UserClientService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.facebook.FacebookSdk;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.fragments.DogBreedsFragment;
import cz.mendelu.xcendel1.dogapp.fragments.FavouriteDogBreedsFragment;
import cz.mendelu.xcendel1.dogapp.fragments.HomeFragment;
import cz.mendelu.xcendel1.dogapp.fragments.MapsFragment;
import cz.mendelu.xcendel1.dogapp.fragments.ReminderFragment;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_USERS;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.CURRENT_FRAGMENT;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_MY_PROFILE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_REMINDER;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_EMAIL;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_NAME;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_PHOTO_LINK;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_MY_PROFILE_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.UPDATED_PHOTO;

/**
 * A main screen of the app. It includes the main navigation in form of the navigation drawer.
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private DatabaseReference databaseReference;
    private ValueEventListener valueEventListener;
    private Unbinder unbinder;
    private int currentFragment;
    String userId;
    private static final int SETTINGS_REQUEST_CODE = 111;
    private static final int CHAT_REQUEST_CODE = 112;
    private static final int REQUEST_SELECT_PLACE = 1000;
    CircleImageView profilePhoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");



        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClickBackArrow");
                onBackPressed();
            }
        });

        setTitle(getString(R.string.my_profile));
        navigationView.setCheckedItem(R.id.nav_profile);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(CURRENT_FRAGMENT)) {
                currentFragment = savedInstanceState.getInt(CURRENT_FRAGMENT);
            }
        } else {
            currentFragment = R.id.nav_profile;
        }

        if (getIntent().hasExtra(PARAM_REMINDER)) {
            currentFragment = R.id.nav_reminder;
            setTitle(getString(R.string.reminder));
            navigationView.setCheckedItem(R.id.nav_reminder);
        } else if (getIntent().hasExtra(PARAM_MY_PROFILE)) {
            currentFragment = R.id.nav_profile;
        }

        FacebookSdk.sdkInitialize(getApplicationContext());
        FirebaseInstanceId.getInstance().getToken();


        chooseFragment(currentFragment);
        setupDrawerContent(navigationView);
        databaseReference = ((FirebaseApplication) getApplication()).getFirebaseDatabase();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        userId = ((FirebaseApplication) getApplication()).getFirebaseUserId();

        setupNavigationHeader();


    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;

                    }
                });
    }

    private void selectDrawerItem(MenuItem menuItem) {
        chooseFragment(menuItem.getItemId());
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        drawerLayout.closeDrawers();

    }

    private void chooseFragment(int itemId) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = null;
        Class fragmentClass = null;
        switch (itemId) {
            case R.id.nav_profile:
                fragmentClass = HomeFragment.class;
                break;
            case R.id.nav_inbox:
                Intent intent = new Intent(this, ConversationActivity.class);
                startActivityForResult(intent, CHAT_REQUEST_CODE);
                break;
            case R.id.nav_map:
                fragmentClass = MapsFragment.class;
                break;
            case R.id.nav_dog_breed:
                fragmentClass = DogBreedsFragment.class;
                break;
            case R.id.nav_reminder:
                fragmentClass = ReminderFragment.class;
                break;
            case R.id.nav_favourite_dog_breeds:
                fragmentClass = FavouriteDogBreedsFragment.class;
                break;
            case R.id.nav_settings:
                Intent intent2 = new Intent(MainActivity.this, SettingsActivity.class);
                startActivityForResult(intent2, SETTINGS_REQUEST_CODE);
                break;
            case R.id.nav_logout:
                FirebaseMessaging.getInstance().unsubscribeFromTopic(((FirebaseApplication) getApplication()).getFirebaseUserId());
                ((FirebaseApplication) getApplication()).signOutUser(this);
                new UserClientService(this).logout();
                break;
            default:
                fragmentClass = HomeFragment.class;

        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            Log.e(TAG, "Failed to obtain the fragment " + e.getMessage());

        }

        if (fragment != null && fragmentClass != null) {
            fragmentManager.beginTransaction().replace(R.id.content_main, fragment).addToBackStack(null).commit();
        }

    }

    /**
     * Setups the navigation header, if data is not available from shared preferences and external storage, it downloads it from firebase database and storage.
     */
    public void setupNavigationHeader() {
        SharedPreferences sharedPref = getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedPref.contains(SHARED_PREFERENCES_EMAIL + userId) && sharedPref.contains(SHARED_PREFERENCES_NAME + userId)) {
            String name = sharedPref.getString(SHARED_PREFERENCES_NAME + userId, "");
            String email = sharedPref.getString(SHARED_PREFERENCES_EMAIL + userId, "");
            String photoLink = sharedPref.getString(SHARED_PREFERENCES_PHOTO_LINK + userId, "");
            setupDataForDrawer(name, email, photoLink);
        } else {
            valueEventListener = databaseReference.child(FIREBASE_CHILD_USERS).child(userId).addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "Retrieving user with uid " + userId);
                    cz.mendelu.xcendel1.dogapp.models.User user = dataSnapshot.getValue(cz.mendelu.xcendel1.dogapp.models.User.class);
                    setupDataForDrawer(user.getName(), user.getEmail(), user.getPhoto());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.w(TAG, "Failed to read value ", databaseError.toException());
                    DialogHelper.showAlertDialog(MainActivity.this, databaseError.toString());

                }

            });
        }
    }

    /**
     * It sets data for navigation drawer such as name, email and dogPhoto that can be taken from external storage or download from firebase storage
     *
     * @param name
     * @param email
     * @param photo
     */
    private void setupDataForDrawer(String name, String email, String photo) {
        if (navigationView != null) {
            View header = navigationView.getHeaderView(0);
            profilePhoto = (CircleImageView) header.findViewById(R.id.user_photo);

            if (photo != null) {
                String imagePath = userId + STORAGE_IMAGES_PATH + STORAGE_MY_PROFILE_PHOTO;
                if (ImageUtils.checkExternalStorage() && ImageUtils.checkIfImageExits(MainActivity.this, imagePath)) {
                    File filePath = getExternalCacheDir();
                    File myDir = new File(filePath.getAbsolutePath() + "/" + imagePath);
                    Picasso.with(this).load(myDir).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(profilePhoto);
                } else {
                    if (photo.contains("https")) {
                        Picasso.with(this).load(photo).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(profilePhoto);
                    } else if (!photo.isEmpty()) {
                        profilePhoto.setImageBitmap(ImageUtils.decodeBitmap(photo));
                    }
                }
            }

            TextView nameView = (TextView) header.findViewById(R.id.name_view);
            nameView.setText(name);
            TextView emailView = (TextView) header.findViewById(R.id.email_view);
            emailView.setText(email);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if (valueEventListener != null) {
            databaseReference.removeEventListener(valueEventListener);
        }
        unbinder.unbind();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt(CURRENT_FRAGMENT, currentFragment);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SETTINGS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.hasExtra(UPDATED_PHOTO) && data.getStringExtra(UPDATED_PHOTO).equals(UPDATED_PHOTO)) {
                    // user profile photo was updated
                    String path = userId + STORAGE_IMAGES_PATH + STORAGE_MY_PROFILE_PHOTO;
                    Picasso.with(this).load(ImageUtils.getImageFile(this, path)).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(profilePhoto);
                }
            }
            setTitle(getString(R.string.my_profile));
            navigationView.setCheckedItem(R.id.nav_profile);
        }

        if (requestCode == CHAT_REQUEST_CODE) {
            setTitle(getString(R.string.my_profile));
            navigationView.setCheckedItem(R.id.nav_profile);
        }

        if (requestCode == REQUEST_SELECT_PLACE) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_main);
            // redirects to map fragment
            fragment.onActivityResult(requestCode, resultCode, data);

        }



    }
}
