package cz.mendelu.xcendel1.dogapp.alarm;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import cz.mendelu.xcendel1.dogapp.utils.DateUtils;

import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.REMINDER_ID;

/**
 * Created by admin on 06.12.2016.
 *
 * Snoozes notification for 5 mins
 */

public class SnoozeReceiver extends BroadcastReceiver {
    private static final String TAG = SnoozeReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.hasExtra(REMINDER_ID)) {
            int notificationId = intent.getIntExtra(REMINDER_ID, 0);

            Log.d(TAG, "Start snoozing");
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationId);
            new ReminderManager(context).setRepeatingAlarm(notificationId);


        }
    }
}
