package cz.mendelu.xcendel1.dogapp.models;

/**
 * Created by admin on 27.11.2016.
 *
 * Enum that presents ways how to filter a new dog friend. It can be name, breed or gender
 */

public enum FilterNewFriendEnum {
    FILTER_NAME("Jméno"),
    FILTER_DOG_BREED("Rasa"),
    FILTER_GENDER("Pohlaví");

    private String type;


    FilterNewFriendEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static String[] getValues() {

        FilterNewFriendEnum[] options = values();
        String[] optionsString = new String[options.length];

        for (int i = 0; i < options.length; i++) {
            optionsString[i] = options[i].getType();
        }

        return optionsString;
    }
}
