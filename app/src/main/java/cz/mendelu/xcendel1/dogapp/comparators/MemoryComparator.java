package cz.mendelu.xcendel1.dogapp.comparators;

import java.util.Comparator;

import cz.mendelu.xcendel1.dogapp.models.Memory;
import cz.mendelu.xcendel1.dogapp.utils.DateUtils;

/**
 * Created by admin on 08.12.2016.
 *
 * Comparator for memory according to time
 */

public class MemoryComparator implements Comparator<Memory> {
    @Override
    public int compare(Memory memory, Memory memory2) {
        return DateUtils.compareTwoDates(memory.getDate(), memory2.getDate(), MemoryComparator.class.getSimpleName());
    }
}
