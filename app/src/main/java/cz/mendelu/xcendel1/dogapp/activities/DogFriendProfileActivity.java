package cz.mendelu.xcendel1.dogapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.Collections;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.adapters.ViewPagerAdapter;
import cz.mendelu.xcendel1.dogapp.fragments.DogDiaryFragment;
import cz.mendelu.xcendel1.dogapp.fragments.DogFriendsFragment;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.utils.DateUtils;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import cz.mendelu.xcendel1.dogapp.utils.UserUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_EDIT;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_FRIEND_PROFILE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_MY_PROFILE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_DOG;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_DOG_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_DOG_FRIENDS;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOGS_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.UPDATED_DOG;

public class DogFriendProfileActivity extends AppCompatActivity {
    private static final String TAG = DogFriendProfileActivity.class.getSimpleName();

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.dog_photo)
    CircleImageView dogPhoto;
    @BindView(R.id.dog_name)
    TextView dogName;
    @BindView(R.id.dog_breed)
    TextView dogBreed;
    @BindView(R.id.hobbies)
    TextView hobbies;
    @BindView(R.id.weight)
    TextView weight;
    @BindView(R.id.age)
    TextView age;
    @BindView(R.id.gender)
    ImageView gender;

    @BindView(R.id.btt_status)
    RelativeLayout tvStatus;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    private static Dog dogFriend;
    private DogDiaryFragment dogDiaryFragment;
    private DogFriendsFragment dogFriendsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dog_friend_profile);

        ButterKnife.bind(this);

        if (getIntent().hasExtra(PARCEL_DOG)) {
            Bundle data = getIntent().getExtras();
            dogFriend = data.getParcelable(PARCEL_DOG);
            if (getIntent().hasExtra(PARAM_FRIEND_PROFILE)) {
                getSupportActionBar().setTitle(getString(R.string.my_dog_friend) + " " + dogFriend.getName());
            } else {
                getSupportActionBar().setTitle(getString(R.string.another_dog) + " " + dogFriend.getName());
            }

        }

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupData(false);
    }

    private void setupViewPager(final ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        dogDiaryFragment = DogDiaryFragment.newInstance(dogFriend.getId(), dogFriend.getUserId(), PARAM_FRIEND_PROFILE);
        dogFriendsFragment = DogFriendsFragment.newInstance(dogFriend.getId(), PARAM_FRIEND_PROFILE, dogFriend.getName());

        adapter.addFragment(dogDiaryFragment, getString(R.string.dog_diary_tab));
        adapter.addFragment(dogFriendsFragment, getString(R.string.dog_friends_tab));
        viewPager.setAdapter(adapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }


    private void setupData(boolean invalidate) {
        dogName.setText(dogFriend.getName());
        if (!dogFriend.getBreed().equals("")) {
            dogBreed.setVisibility(View.VISIBLE);
            dogBreed.setText(dogFriend.getBreed());
        }

        if (!dogFriend.getHobbies().equals("")) {
            hobbies.setVisibility(View.VISIBLE);
            hobbies.setText(dogFriend.getHobbies());
        }

        if (!dogFriend.getWeight().equals("")) {
            weight.setText(dogFriend.getWeight() + " kg");
        }

        if (!dogFriend.getStatus().equals("")) {
            ((TextView) tvStatus.getChildAt(1)).setText(dogFriend.getStatus());
        }

        try {
            age.setText(DateUtils.getDogAge(dogFriend.getBirthday()));
        } catch (ParseException e) {
            Log.e(TAG, "Error during parsing myDog age" + e);
        }

        if (dogFriend.isGender()) {
            gender.setImageResource(R.drawable.ic_gender_female);
        } else {
            gender.setImageResource(R.drawable.ic_gender_male);
        }

        String userId = ((FirebaseApplication) getApplication()).getFirebaseUserId();
        String imagePath = userId + STORAGE_IMAGES_DOGS_PATH + dogFriend.getId();
        ImageUtils.setImageView(DogFriendProfileActivity.this, imagePath, dogFriend.getPhoto(), dogPhoto, invalidate, SET_DOG_PHOTO);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.menu_more, menu);

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().setTitle(getString(R.string.another_dog) + " " + dogFriend.getName());
        Log.d(TAG, "onResume");
        viewPager.getCurrentItem();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_more) {
            View menuItemView = findViewById(R.id.action_more);
            PopupMenu popupMenu = DialogHelper.createPopupMenu(this, menuItemView, R.menu.popup_friend_profile_menu);

            popupMenu.show();
            registerPopupActions(popupMenu);
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return false;
    }

    private void registerPopupActions(PopupMenu popupMenu) {
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_unfriend:
                        ((FirebaseApplication) getApplication()).removeFriendship(DogFriendProfileActivity.this, MyDogProfileActivity.getDogId() , dogFriend.getId());
                        DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.success_unfriend));
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable(){
                            @Override
                            public void run(){
                                startActivity(new Intent(DogFriendProfileActivity.this, MyDogProfileActivity.class));
                            }
                        }, 2000);

                        return true;
                    case R.id.action_home:
                        startActivity(new Intent(DogFriendProfileActivity.this, MainActivity.class));
                    default:
                        return false;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        super.onBackPressed();
    }



}
