//package cz.mendelu.xcendel1.dogapp.activities;
//
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.RecyclerView;
//import android.widget.EditText;
//
//import com.google.firebase.database.DatabaseReference;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
//import cz.mendelu.xcendel1.dogapp.Interfaces;
//import cz.mendelu.xcendel1.dogapp.R;
//import cz.mendelu.xcendel1.dogapp.adapters.MessageAdapter;
//import cz.mendelu.xcendel1.dogapp.helpers.InitializationHelper;
//import cz.mendelu.xcendel1.dogapp.models.Chat;
//import cz.mendelu.xcendel1.dogapp.models.ChatPartner;
//import cz.mendelu.xcendel1.dogapp.models.Message;
//import cz.mendelu.xcendel1.dogapp.models.ModelFactory;
//
//import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_CHAT_PARTNER;
//
//
///**
// * A screen that shows detail of the specified chat
// */
//public class ChatDetailActivity extends AppCompatActivity implements Interfaces.ItemClickCallback {
//
//    private static final String TAG = ChatDetailActivity.class.getSimpleName();
//
//    @BindView(R.id.recycler_view)
//    RecyclerView recyclerView;
//    @BindView(R.id.input_enter_message)
//    EditText editText;
//
//    private MessageAdapter messageAdapter;
//    private List<Message> messagesList = new ArrayList<>();
//
//    private ChatPartner chatPartner;
//    private Chat chat;
//    private DatabaseReference databaseReference;
//    private String userId;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_conversation_detail);
//
//        if (getIntent().hasExtra(PARCEL_CHAT_PARTNER)) {
//            Bundle data = getIntent().getExtras();
//            chatPartner = data.getParcelable(PARCEL_CHAT_PARTNER);
//        }
////        } else if (getIntent().hasExtra(PARCEL_CHAT)) {
////            chat = getIntent().getExtras().getParcelable(PARCEL_CHAT);
////        }
//
//        userId = ((FirebaseApplication) getApplication()).getFirebaseUserId();
//
//
//        //Bitmap bitmap = BitmapFactory.decodeByteArray(chatPartner.getPhotoLink(), 0, chatPartner.getPhotoLink().length);
//
//        ButterKnife.bind(this);
//        messageAdapter = new MessageAdapter(messagesList);
//        messageAdapter.setItemClickCallback(this);
//
//        InitializationHelper.setupRecyclerView(this, messageAdapter, recyclerView);
//
//    }
//
//
//    @OnClick(R.id.btn_send_message)
//    public void sendMessage() {
//        if (chatPartner != null) {
//            Message message = ModelFactory.createMessage(userId, editText);
//            ChatPartner chatPartner1 = ModelFactory.createChatPartner(chatPartner.getId(), chatPartner.getName(), chatPartner.getPhoto());
//            ChatPartner chatPartner2 = ModelFactory.createChatPartner(userId, "petra", "");
//            Chat chat = ModelFactory.createChat(null, message.getText(), message.getTimestamp(), chatPartner1, chatPartner2);
//            Map<String, Boolean> chatMembers = new HashMap<>();
//            chatMembers.put(chatPartner.getId(), true);
//            chatMembers.put(userId, true);
//
//            editText.getText().clear();
//
//        } else {
//
//        }
//
//
//    }
//
//    @Override
//    public void onItemClick(int pos) {
//
//    }
//}
