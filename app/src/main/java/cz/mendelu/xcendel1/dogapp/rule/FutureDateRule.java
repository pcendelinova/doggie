package cz.mendelu.xcendel1.dogapp.rule;


import android.util.Log;

import com.mobsandgeeks.saripaar.AnnotationRule;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.mendelu.xcendel1.dogapp.annotation.FutureDate;
import cz.mendelu.xcendel1.dogapp.constants.SettingsConstants;
import cz.mendelu.xcendel1.dogapp.utils.DateUtils;

/**
 * Created by admin on 22.12.2016.
 */

public class FutureDateRule extends AnnotationRule<FutureDate, String> {
    public static final String TAG = FutureDateRule.class.getSimpleName();
    protected FutureDateRule(FutureDate futureDate) {
        super(futureDate);
    }

    @Override
    public boolean isValid(String dateString) {
        DateFormat dateFormat = new SimpleDateFormat(SettingsConstants.DATE_FORMAT_DMY);
        Date parsedDate = null;


        try {
            parsedDate = dateFormat.parse(dateString);
        } catch (ParseException ignored) {
            Log.e(TAG, "Error during parsing date");
        }


        return parsedDate != null && (DateUtils.isToday(dateString) || DateUtils.isAfterToday(dateString)) && dateString.equals(dateFormat.format(parsedDate));
    }


}
