package cz.mendelu.xcendel1.dogapp.models;

import android.graphics.Bitmap;
import android.support.design.widget.TextInputLayout;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.RadioGroup;

import java.io.ByteArrayOutputStream;

import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import cz.mendelu.xcendel1.dogapp.utils.ModelUtils;
import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * Created by Petra Cendelinova on 11/1/2016.
 *
 * It is model factory
 */

public class ModelFactory {

    /**
     * Creates a dog by given its properties
     * @param id
     * @param inputName
     * @param inputBirthday
     * @param inputBreed
     * @param inputHobbies
     * @param inputGender
     * @param inputWeight
     * @param imageResource
     * @param inputStatus
     * @return Dog
     */
    public static Dog createDog(String id, TextInputLayout inputName, TextInputLayout inputBirthday,
                                AutoCompleteTextView inputBreed, TextInputLayout inputHobbies,
                                RadioGroup inputGender, TextInputLayout inputWeight, String imageResource, TextInputLayout inputStatus) {

        String name = ModelUtils.getProperty(inputName);
        String birthday = ModelUtils.getProperty(inputBirthday);
        String dogBreed = inputBreed.getText().toString();
        String hobbies = ModelUtils.getProperty(inputHobbies);
        String weight = ModelUtils.getProperty(inputWeight);
        String status = ModelUtils.getProperty(inputStatus);

        int checkedGender = inputGender.getCheckedRadioButtonId();

        //false for male
        boolean gender = false;
        if (checkedGender == R.id.radio_female) {
            gender = true;
        }

        if (imageResource == null) {
            imageResource = "";
        }

        return new Dog(id, name, birthday, dogBreed, hobbies, imageResource, gender, weight, status);

    }

    /**
     * Creates a reminder from user inputs
     * @param inputName
     * @param inputDate
     * @param inputTime
     * @param inputNote
     * @param spinner
     * @return Reminder
     */
    public static Reminder createReminder(TextInputLayout inputName, TextInputLayout inputDate,
                                          TextInputLayout inputTime, TextInputLayout inputNote,
                                          MaterialSpinner spinner) {
        String name = ModelUtils.getProperty(inputName);
        String date = ModelUtils.getProperty(inputDate);
        String time = ModelUtils.getProperty(inputTime);
        String note = ModelUtils.getProperty(inputNote);
        String category = spinner.getSelectedItem().toString();

        return new Reminder(name, date, time, category, note);
    }

    /**
     * Creates a memory from user inputs
     * @param id
     * @param inputName
     * @param inputDate
     * @param inputPlace
     * @param inputNote
     * @param photo
     * @return Memory
     */
    public static Memory createMemory(String id, TextInputLayout inputName, TextInputLayout inputDate,
                                      TextInputLayout inputPlace, TextInputLayout inputNote, String photo) {
        String name = ModelUtils.getProperty(inputName);
        String date = ModelUtils.getProperty(inputDate);
        String place = ModelUtils.getProperty(inputPlace);
        String note = ModelUtils.getProperty(inputNote);

        return new Memory(id, name, place, date, photo, note);
    }

    /**
     * Creates a user by given properties
     * @param id
     * @param photo
     * @param email
     * @param name
     * @return User
     */
    public static User createUser(String id, String photo, String email, String name) {
        return new User(id, photo, email, name);
    }
}
