package cz.mendelu.xcendel1.dogapp.models;

/**
 * Created by admin on 17.12.2016.
 *
 * It will be use in next release
 */

public enum FilterFriendEnum {
    FILTER_MUTUAL("Společní kamarádi"),
    FILTER_OTHERS("Ostatní");

    private String type;

    FilterFriendEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static String[] getValues() {

        FilterFriendEnum[] options = values();
        String[] optionsString = new String[options.length];

        for (int i = 0; i < options.length; i++) {
            optionsString[i] = options[i].getType();
        }

        return optionsString;
    }
}

