package cz.mendelu.xcendel1.dogapp.fragments;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.activities.DogBreedDetailActivity;
import cz.mendelu.xcendel1.dogapp.adapters.DogBreedAdapter;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.helpers.InitializationHelper;
import cz.mendelu.xcendel1.dogapp.models.DogBreed;

import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_FAVOURITE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_DOG_BREED;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.DOG_BREED_ID;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOG_BREEDS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_FAVOURITE_DOG_BREEDS;

/**
 * Fragment of favourite dog breeds of the current user
 */
public class FavouriteDogBreedsFragment extends Fragment implements Interfaces.ItemClickCallback {
    private static final String TAG = FavouriteDogBreedsFragment.class.getSimpleName();

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_no_favourite)
    TextView emptyView;
    @BindString(R.string.no_data_retrieved)
    String errorMessage;
    @BindString(R.string.loading_data)
    String loadingData;

    private DogBreedAdapter dogBreedAdapter;
    private List<DogBreed> dogBreedList = new ArrayList<>();
    private List<String> dogBreedFavouriteIds = new ArrayList<>();
    private DatabaseReference databaseReference;
    private ValueEventListener valueEventListener;
    private ValueEventListener dogBreedsListener;
    private Unbinder unbinder;
    String userId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite_dog_breeds, container, false);
        unbinder = ButterKnife.bind(this, view);

        databaseReference = ((FirebaseApplication) getActivity().getApplication()).getFirebaseDatabase();
        userId = ((FirebaseApplication) getActivity().getApplication()).getFirebaseUserId();

        dogBreedAdapter = new DogBreedAdapter(getActivity(), dogBreedList);
        dogBreedAdapter.setItemClickCallback(this);
        InitializationHelper.setupRecyclerView(getContext(), dogBreedAdapter, recyclerView);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        new DownloadTask().execute();

    }

    @Override
    public void onItemClick(int pos) {
        DogBreed dogBreed = dogBreedList.get(pos);
        Intent intent = new Intent(getActivity(), DogBreedDetailActivity.class);
        intent.putExtra(PARCEL_DOG_BREED, dogBreed);
        intent.putExtra(PARAM_FAVOURITE, PARAM_FAVOURITE);
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (valueEventListener != null) {
            databaseReference.removeEventListener(valueEventListener);
        }
        DialogHelper.hideProgressDialog();
        unbinder.unbind();
    }

    public class DownloadTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            DialogHelper.showProgressDialog(getActivity(), getString(R.string.loading_data));
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dogBreedFavouriteIds.clear();
            valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.hasChildren()) {
                        if (emptyView != null) {
                            emptyView.setVisibility(View.VISIBLE);
                        }
                    } else {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            try {
                                HashMap<String, String> dogBreedId = (HashMap<String, String>) postSnapshot.getValue();
                                dogBreedFavouriteIds.add(dogBreedId.get(DOG_BREED_ID));
                            } catch (Exception ex) {
                                Log.e(TAG, "Getting dog breeds " + ex.getMessage());
                            }


                        }

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d(TAG, "onCancelled", databaseError.toException());
                    DialogHelper.showNoDataRetrieved(getActivity());
                }
            };


            databaseReference.child(FIREBASE_CHILD_FAVOURITE_DOG_BREEDS).child(userId).addValueEventListener(valueEventListener);

            dogBreedsListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    dogBreedList.clear();
                    if (!dataSnapshot.hasChildren()) {
                        emptyView.setVisibility(View.VISIBLE);
                    } else {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            DogBreed dogBreed = postSnapshot.getValue(DogBreed.class);
                            if (dogBreedFavouriteIds.contains(dogBreed.getId())) {
                                dogBreedList.add(dogBreed);
                            }
                        }
                        if (dogBreedList.isEmpty()) {
                            emptyView.setVisibility(View.VISIBLE);
                        }
                        dogBreedAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    DialogHelper.showNoDataRetrieved(getActivity());
                }
            };

            databaseReference.child(FIREBASE_CHILD_DOG_BREEDS).addListenerForSingleValueEvent(dogBreedsListener);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            dogBreedAdapter.setGenericList(dogBreedList);
            recyclerView.setVisibility(View.VISIBLE);
            DialogHelper.hideProgressDialog();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
