package cz.mendelu.xcendel1.dogapp.helpers;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.PopupMenu;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.constants.SettingsConstants;
import cz.mendelu.xcendel1.dogapp.models.FilterFriendEnum;
import cz.mendelu.xcendel1.dogapp.models.FilterMapPlace;
import cz.mendelu.xcendel1.dogapp.models.FilterNewFriendEnum;
import cz.mendelu.xcendel1.dogapp.utils.DateUtils;

/**
 * Created by Petra Cendelinova on 10/26/2016.
 *
 * A helper class for dialogs
 */

public class DialogHelper {

    private static ProgressDialog progressDialog;

    /**
     * Shows the progress dialog
     * @param context
     * @param titleMessage
     */
    public static void showProgressDialog(Context context, String titleMessage) {
        if (context != null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(titleMessage);
            progressDialog.setMessage(context.getResources().getString(R.string.progress_dialog_content));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

    }

    /**
     * Hides the progress dialog
     */
    public static void hideProgressDialog() {
        progressDialog.dismiss();
    }

    /**
     * Show alert dialog giving information that something went wrong
     * @param context
     * @param message
     */
    public static void showAlertDialog(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setTitle(R.string.error)
                .setPositiveButton(android.R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Creates a question dialog
     *
     * @param context
     * @param title
     * @param message
     * @param positiveListener
     */
    public static void showQuestionDialog(Context context, String title, String message, DialogInterface.OnClickListener positiveListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog questionDialog = builder.setMessage(message)
                .setTitle(title)
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(android.R.string.ok, positiveListener)
                .setMessage(message)
                .show();

    }

    /**
     * Shows the date picker and sets the chosen date to the edit text
     * @param context
     * @param textInputLayout
     */
    public static void showDatePicker(Context context, final TextInputLayout textInputLayout) {
        final Calendar myCalendar = Calendar.getInstance();
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat(SettingsConstants.DATE_FORMAT_DMY);


                textInputLayout.getEditText().setText(sdf.format(myCalendar.getTime()));
            }
        };


        new DatePickerDialog(context, dateSetListener, year, month, day).show();

    }

    /**
     * Shows the time picker and sets the chosen time to the edit text
     * @param context
     * @param timeInputLayout
     */
    public static void showTimePicker(final Context context, final TextInputLayout timeInputLayout, final TextInputLayout dateInputLayout) {
        final Calendar myCalendar = Calendar.getInstance();
        int year = myCalendar.get(Calendar.YEAR);
        int month = myCalendar.get(Calendar.MONTH);
        int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        final int hours = myCalendar.get(Calendar.HOUR_OF_DAY);
        final int minutes = myCalendar.get(Calendar.MINUTE);

        final String chosenDate = dateInputLayout.getEditText().getText().toString();


        TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                myCalendar.set(Calendar.MINUTE, minute);
                SimpleDateFormat sdf = new SimpleDateFormat(SettingsConstants.FORMAT_TIME);
                timeInputLayout.setError(null);
                if (chosenDate != null && !chosenDate.isEmpty()) {
                    DateTime date = DateUtils.convertStringToDate(chosenDate);
                    DateTime now = new DateTime();

                    if (DateUtils.isTheSameDay(date, now) && (now.getHourOfDay() >= hourOfDay)) {
                        if (now.getHourOfDay() == hourOfDay && now.getMinuteOfHour() >= minute) {
                            timeInputLayout.setError(context.getString(R.string.reminder_time_must_be_future));
                        } else if (now.getHourOfDay() > hourOfDay && now.getMinuteOfHour() <= minute) {
                            timeInputLayout.setError(context.getString(R.string.reminder_time_must_be_future));
                        }
                    }
                }

                timeInputLayout.getEditText().setText(sdf.format(myCalendar.getTime()));

            }
        };

        new TimePickerDialog(context, timeSetListener, hours, minutes,
                DateFormat.is24HourFormat(context)).show();
    }

    /**
     * Creates popup menu available in the more settings
     * @param context
     * @param menuItemView
     * @param type
     * @return
     */
    public static PopupMenu createPopupMenu(Context context, View menuItemView, int type) {
        PopupMenu popup = new PopupMenu(context, menuItemView);
        popup.getMenuInflater().inflate(type, popup.getMenu());

        return popup;

    }

    /**
     * Shows information message about removing reminders according to their amount
     * @param coordinatorLayout
     * @param size
     */
    public static void showMessageAboutReminders(View coordinatorLayout, int size) {
        if (size == 0) {
            Snackbar.make(coordinatorLayout, R.string.no_reminders_to_remove, Snackbar.LENGTH_LONG).show();
        } else if (size == 1) {
            Snackbar.make(coordinatorLayout, R.string.reminder_removed, Snackbar.LENGTH_LONG).show();
        } else if (size >= 2) {
            Snackbar.make(coordinatorLayout, R.string.reminders_removed, Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Shows an information message as Snackbar
     * @param coordinatorLayout
     * @param message
     */
    public static void showInformationMessage(View coordinatorLayout, String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Shows the share chooser
     * @param context
     * @param content
     */
    public static void showShareChooser(Context context, String content) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, content);
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.share_chooser)));
    }


    /**
     * Shows message about unsuccessful download
     * @param context
     */
    public static void showNoDataRetrieved(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.no_data_retrieved)
                .setTitle(R.string.error)
                .setPositiveButton(android.R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Creates an intent to share a photo
     * @param context
     * @param fileName
     * @param bitmap
     * @param infoAboutPhoto
     */
    public static void sharePhoto(Context context, String fileName, Bitmap bitmap, String infoAboutPhoto) {
        try {
            File file = new File(context.getCacheDir(), fileName + ".png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_TEXT, infoAboutPhoto);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/*");
            context.startActivity(Intent.createChooser(intent, context.getResources().getText(R.string.share_chooser)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /**
     * Shows filter options available on the Map fragment
     * @param context
     * @return
     */
    public static void showFilterMapOptions(Context context, final Interfaces.FilterPlacesCallback filterPlacesCallback) {
        final String[] options = FilterMapPlace.getValues();
        final FilterMapPlace[] allOptions = FilterMapPlace.values();
        final List<FilterMapPlace> selected = new ArrayList<>();
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.filter_map_types)
                .setMultiChoiceItems(options, null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                        if (isChecked) {
                            selected.add(allOptions[indexSelected]);
                        } else if (selected.contains(allOptions[indexSelected])) {
                            selected.remove(allOptions[indexSelected]);
                        }
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        filterPlacesCallback.filterByType(selected);
                    }
                })
                .setNegativeButton(context.getText(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).create();
        dialog.show();

    }

    /**
     * Shows options available to filter new dog friends, i.e. dog breed, name
     * @param context
     * @param filterOptionCallback
     */
    public static void showFilterNewFriendOptions(Context context, final Interfaces.FilterNewFriendOptionsCallback filterOptionCallback) {
        final String[] options = FilterNewFriendEnum.getValues();
        final FilterNewFriendEnum[] selected = {FilterNewFriendEnum.FILTER_NAME};
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.filter_new_friend)
                .setSingleChoiceItems(options, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String selectedOption = options[i];
                        if (selectedOption.equals(FilterNewFriendEnum.FILTER_DOG_BREED.getType())) {
                            selected[0] = FilterNewFriendEnum.FILTER_DOG_BREED;
                        } else if (selectedOption.equals(FilterNewFriendEnum.FILTER_GENDER.getType())) {
                            selected[0] = FilterNewFriendEnum.FILTER_GENDER;
                        } else {
                            selected[0] = FilterNewFriendEnum.FILTER_NAME;
                        }
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        filterOptionCallback.filterByType(selected[0]);
                    }
                })
                .setNegativeButton(context.getText(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
        dialog.show();
    }

    //TODO it will be used to expand the existing app later
    public static void showFilterFriends(Context context, final Interfaces.FilterTypeOfFriendCallback filterOptionCallback) {
        final String[] options = FilterFriendEnum.getValues();
        final FilterFriendEnum[] selected = {FilterFriendEnum.FILTER_OTHERS};
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.type_of_friend)
                .setSingleChoiceItems(options, 1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String selectedOption = options[i];
                        if (selectedOption.equals(FilterFriendEnum.FILTER_MUTUAL.getType())) {
                            selected[0] = FilterFriendEnum.FILTER_MUTUAL;
                        } else if (selectedOption.equals(FilterFriendEnum.FILTER_OTHERS.getType())) {
                            selected[0] = FilterFriendEnum.FILTER_OTHERS;
                        }
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        filterOptionCallback.filterByType(selected[0]);
                    }
                })
                .setNegativeButton(context.getText(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
        dialog.show();
    }
}

