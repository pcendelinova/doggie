package cz.mendelu.xcendel1.dogapp.adapters;

import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.xcendel1.dogapp.models.User;
//import cz.mendelu.xcendel1.dogapp.viewholders.UserViewHolder;

/**
 * Created by admin on 08.11.2016.
 * It will be user for implementing my own chat
 */

//public class UserAdapter extends BaseAdapter<User, UserViewHolder> implements Filterable{
//
//    private List<User> originalList;
//    private List<User> list;
//
//    public UserAdapter(List<User> genericList) {
//        super(genericList);
//        this.list = genericList;
//    }
//
//    @Override
//    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        return new UserViewHolder(parent, getItemClickCallback());
//    }
//
//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                List<User> filteredResults = null;
//                if (charSequence.length() == 0) {
//                    filteredResults = originalList;
//                } else {
//                    filteredResults = getFilteredResults(charSequence.toString().toLowerCase());
//                }
//
//                FilterResults results = new FilterResults();
//                results.values = filteredResults;
//
//                return results;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                list = (List<User>) filterResults.values;
//                setGenericList(list);
//                notifyDataSetChanged();
//            }
//        };
//    }
//
//    protected List<User> getFilteredResults(String constraint) {
//        List<User> results = new ArrayList<>();
//
//        for (User item : originalList) {
//            if (item.getName().toLowerCase().contains(constraint)) {
//                results.add(item);
//            }
//        }
//        return results;
//    }
//
//        @Override
//    public int getItemCount() {
//        return list.size();
//    }
//
//}



