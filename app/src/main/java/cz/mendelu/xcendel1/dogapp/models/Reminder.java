package cz.mendelu.xcendel1.dogapp.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Petra Cendelinova on 10/30/2016.
 */

public class Reminder implements Parcelable {
    private int id;
    private String name, category, note;
    private String date;
    private String time;

    public Reminder() {
    }

    protected Reminder(Parcel in) {
        id = in.readInt();
        name = in.readString();
        category = in.readString();
        note = in.readString();
        date = in.readString();
        time = in.readString();
    }

    public static final Creator<Reminder> CREATOR = new Creator<Reminder>() {
        @Override
        public Reminder createFromParcel(Parcel in) {
            return new Reminder(in);
        }

        @Override
        public Reminder[] newArray(int size) {
            return new Reminder[size];
        }
    };

    public Reminder(int id, String name, String date, String time, String category, String note) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.note = note;
        this.date = date;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Reminder{" +
                "name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", note='" + note + '\'' +
                ", date=" + date +
                ", time=" + time +
                '}';
    }

    public Reminder(String name, String date, String time, String category, String note) {
        this.name = name;
        this.category = category;
        this.note = note;
        this.date = date;
        this.time = time;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(category);
        parcel.writeString(note);
        parcel.writeString(date);
        parcel.writeString(time);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
