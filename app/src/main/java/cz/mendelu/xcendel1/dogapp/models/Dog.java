package cz.mendelu.xcendel1.dogapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

import cz.mendelu.xcendel1.dogapp.viewholders.FriendshipState;

/**
 * Created by Petra Cendelinova on 10/27/2016.
 *
 * Dog model class
 */

public class Dog extends GenericModel implements Parcelable {
    private String name, birthday, breed, hobbies, status, weight, userId;
    // ignore photo during downloading
    @Exclude
    private String photo;
    private boolean gender;
    private FriendshipState friendshipState;
    private boolean updatedPhoto;
    private boolean isMutualFriend;

    public Dog() {
    }

    public Dog(String id, String name, String birthday, String breed, String hobbies, String photo, boolean gender, String weight, String status) {
        super(id);
        this.name = name;
        this.birthday = birthday;
        this.breed = breed;
        this.hobbies = hobbies;
        this.photo = photo;
        this.gender = gender;
        this.weight = weight;
        this.status = status;
        this.updatedPhoto = false;
    }

    protected Dog(Parcel in) {
        super(in);
        name = in.readString();
        birthday = in.readString();
        breed = in.readString();
        hobbies = in.readString();
        gender = in.readByte() != 0;
        weight = in.readString();
        userId = in.readString();
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(name);
        dest.writeString(birthday);
        dest.writeString(breed);
        dest.writeString(hobbies);
        dest.writeByte((byte) (gender ? 1 : 0));
        dest.writeString(weight);
        dest.writeString(userId);
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Dog> CREATOR = new Creator<Dog>() {
        @Override
        public Dog createFromParcel(Parcel in) {
            return new Dog(in);
        }

        @Override
        public Dog[] newArray(int size) {
            return new Dog[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    @Exclude
    public String getPhoto() {
        return photo;
    }

    @Exclude
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public FriendshipState getFriendshipState() {
        return friendshipState;
    }

    public void setFriendshipState(FriendshipState friendshipState) {
        this.friendshipState = friendshipState;
    }

    public boolean isUpdatedPhoto() {
        return updatedPhoto;
    }

    public void setUpdatedPhoto(boolean updatedPhoto) {
        this.updatedPhoto = updatedPhoto;
    }

    public boolean isMutualFriend() {
        return isMutualFriend;
    }

    public void setMutualFriend(boolean mutualFriend) {
        isMutualFriend = mutualFriend;
    }
}
