package cz.mendelu.xcendel1.dogapp.comparators;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

import cz.mendelu.xcendel1.dogapp.models.DogBreed;

/**
 * Created by admin on 20.12.2016.
 *
 * Comparator for dog breeds including czech letters
 */

public class DogBreedComparator implements Comparator<DogBreed> {
    @Override
    public int compare(DogBreed dogBreed, DogBreed dogBreed2) {
        Collator collator = Collator.getInstance(new Locale("cs"));
        collator.setStrength(Collator.PRIMARY);
        return collator.compare(dogBreed.getName(), dogBreed2.getName());
    }
}
