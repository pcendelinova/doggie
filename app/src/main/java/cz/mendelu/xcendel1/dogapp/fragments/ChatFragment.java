//package cz.mendelu.xcendel1.dogapp.fragments;
//
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
//import com.applozic.mobicomkit.api.account.user.UserLoginTask;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.ValueEventListener;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import butterknife.Unbinder;
//import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
//import cz.mendelu.xcendel1.dogapp.Interfaces;
//import cz.mendelu.xcendel1.dogapp.R;
//import cz.mendelu.xcendel1.dogapp.activities.ChatDetailActivity;
//import cz.mendelu.xcendel1.dogapp.activities.NewMessageActivity;
//import cz.mendelu.xcendel1.dogapp.adapters.ChatAdapter;
//import cz.mendelu.xcendel1.dogapp.helpers.InitializationHelper;
//import cz.mendelu.xcendel1.dogapp.models.Chat;
//
//import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_CHAT_ID;
//import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_CHATS_MEMBERS;
//
//
//public class ChatFragment extends Fragment implements Interfaces.ItemClickCallback {
//    private static final String TAG = ChatFragment.class.getSimpleName();
//
//    @BindView(R.id.recycler_view)
//    RecyclerView recyclerView;
//    @BindView(R.id.tv_no_conversations)
//    TextView emptyView;
//    private ChatAdapter chatAdapter;
//    private List<Chat> chatList = new ArrayList<>();
//    private List<String> chatIds = new ArrayList<>();
//    private DatabaseReference databaseReference;
//    private Unbinder unbinder;
//    private ValueEventListener valueEventListener, valueEventListener2;
//
//
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//    }
//
//    @OnClick(R.id.fab)
//    public void createNewMessage() {
//        startActivity(new Intent(getActivity(), NewMessageActivity.class));
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_chat, container, false);
//        unbinder = ButterKnife.bind(this, view);
//
//
//
//        databaseReference = ((FirebaseApplication) getActivity().getApplication()).getFirebaseDatabase();
//        String userId = ((FirebaseApplication) getActivity().getApplication()).getFirebaseUserId();
//
//        valueEventListener = new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                String chatId = dataSnapshot.getKey();
//                chatIds.add(chatId);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        };
//
//        databaseReference.child(FIREBASE_CHILD_CHATS_MEMBERS).orderByChild(userId).equalTo(true).addListenerForSingleValueEvent(valueEventListener);
//
////        childEventListener2 = new ChildEventListener() {
////            @Override
////            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
////                String chatId = dataSnapshot.getKey();
////                chatIds.add(chatId);
////            }
////
////            @Override
////            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
////
////            }
////
////            @Override
////            public void onChildRemoved(DataSnapshot dataSnapshot) {
////
////            }
////
////            @Override
////            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
////
////            }
////
////            @Override
////            public void onCancelled(DatabaseError databaseError) {
////
////            }
////        };
////
////        databaseReference.child(FIREBASE_CHILD_CHATS_MEMBERS).orderByChild(userId).equalTo(true).addChildEventListener(childEventListener2);
////
////        childEventListener = new ChildEventListener() {
////            @Override
////            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
////                if (dataSnapshot.hasChildren()) {
////                    Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());
////                    Chat chat = dataSnapshot.getValue(Chat.class);
////                    chatAdapter.addItem(chat);
////                    chatAdapter.notifyDataSetChanged();
////                } else {
////                    emptyView.setVisibility(View.VISIBLE);
////                }
////
////
////            }
////
////            @Override
////            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
////                Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());
////
////            }
////
////            @Override
////            public void onChildRemoved(DataSnapshot dataSnapshot) {
////                Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());
////            }
////
////            @Override
////            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
////                Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());
////
////            }
////
////            @Override
////            public void onCancelled(DatabaseError databaseError) {
////                Log.w(TAG, "postComments:onCancelled", databaseError.toException());
////                DialogHelper.showNoDataRetrieved(getContext());
////            }
////        };
////        databaseReference.child(FIREBASE_CHILD_CHATS).addChildEventListener(childEventListener);
//
//        chatAdapter = new ChatAdapter(chatList);
//        chatAdapter.setItemClickCallback(this);
//        InitializationHelper.setupRecyclerView(getContext(), chatAdapter, recyclerView);
//
//        UserLoginTask.TaskListener listener = new UserLoginTask.TaskListener() {
//
//            @Override
//            public void onSuccess(RegistrationResponse registrationResponse, Context context) {
//                //After successful registration with Applozic server the callback will come here
//            }
//
//            @Override
//            public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
//                //If any failure in registration the callback  will come here
//            }};
//
////        User user = new User();
////        user.setUserId(userId); //userId it can be any unique user identifier
////        user.setDisplayName("prvni"); //displayName is the name of the user which will be shown in chat messages
////        user.setEmail("meail"); //optional
////        user.setImageLink("");//optional,pass your image link
////        new UserLoginTask(user, listener, this).execute((Void) null);
//
//
//        return view;
//    }
//
//
//    @Override
//    public void onItemClick(int pos) {
//        Chat chat = chatList.get(pos);
//        Intent intent = new Intent(getActivity(), ChatDetailActivity.class);
//        intent.putExtra(PARCEL_CHAT_ID, chat.getId());
////        intent.putExtra(PARCEL_CHAT_PARTNER, );
//        startActivity(intent);
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
////        if (childEventListener != null) {
////            databaseReference.removeEventListener(childEventListener);
////        }
//        unbinder.unbind();
//    }
//}
