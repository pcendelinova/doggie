package cz.mendelu.xcendel1.dogapp.activities;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.adapters.NewFriendAdapter;
import cz.mendelu.xcendel1.dogapp.comparators.DogComparator;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.helpers.InitializationHelper;
import cz.mendelu.xcendel1.dogapp.helpers.NotificationHelper;
import cz.mendelu.xcendel1.dogapp.models.ActionType;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.models.FilterNewFriendEnum;
import cz.mendelu.xcendel1.dogapp.viewholders.FriendshipState;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOGS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_FRIENDSHIPS_REQUESTS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.REQUESTED_ID;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.REQUESTING_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_FRIENDS_IDS;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_NAME;
import static cz.mendelu.xcendel1.dogapp.viewholders.FriendshipState.NEW_REQUEST_STATE;

public class NewFriendActivity extends AppCompatActivity implements SearchView.OnQueryTextListener,
        Interfaces.DogButtonClickCallback, Interfaces.FriendshipRequestCallback, Interfaces.ItemClickCallback,
        Interfaces.FilterNewFriendOptionsCallback {
    private static final String TAG = NewFriendActivity.class.getSimpleName();

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_no_new_friends)
    TextView emptyView;

    @BindString(R.string.loading_data)
    String loadingMessage;
    @BindString(R.string.success_request)
    String successMessage;
    @BindString(R.string.success_cancel)
    String cancelMessage;


    private NewFriendAdapter newFriendAdapter;
    private List<Dog> newFriendList = new ArrayList<>();
    private List<String> requestedIds = new ArrayList<>();
    private DatabaseReference databaseReference;
    private ValueEventListener valueEventListener;
    private ValueEventListener listenerPendingRequests;
    private String myDogId;
    private String dogName;
    private List<String> dogFriendIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_friend);

        ButterKnife.bind(this);

        if (getIntent().hasExtra(PARAM_DOG_ID) && getIntent().hasExtra(PARAM_DOG_FRIENDS_IDS)) {
            myDogId = getIntent().getExtras().getString(PARAM_DOG_ID);
            dogFriendIds = getIntent().getExtras().getStringArrayList(PARAM_DOG_FRIENDS_IDS);
            dogName = getIntent().getExtras().getString(PARAM_DOG_NAME);
        }

        DialogHelper.showProgressDialog(this, loadingMessage);
        databaseReference = ((FirebaseApplication) getApplication()).getFirebaseDatabase();

        listenerPendingRequests = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                requestedIds.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String requestedId = (String) postSnapshot.child(REQUESTED_ID).getValue();
                    requestedIds.add(requestedId);
                }

                databaseReference.child(FIREBASE_CHILD_DOGS).addValueEventListener(valueEventListener);
                Log.i(TAG, "onDataChange: new friends requests retrieved");

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i(TAG, "onCancelled: no data retrieved " + databaseError.getMessage());
            }
        };

        databaseReference.child(FIREBASE_CHILD_FRIENDSHIPS_REQUESTS).orderByChild(REQUESTING_ID).equalTo(myDogId).addValueEventListener(listenerPendingRequests);


        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                newFriendList.clear();
                if (!dataSnapshot.hasChildren()) {
                    emptyView.setVisibility(View.VISIBLE);
                } else {
                    //TODO can accept the request here too later
                    emptyView.setVisibility(View.VISIBLE);
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        for (DataSnapshot dogSnapshot : postSnapshot.getChildren()) {
                            Dog dog = dogSnapshot.getValue(Dog.class);
                            if (!dogFriendIds.contains(dog.getId()) && !myDogId.equals(dog.getId())) {
                                String photo = (String) dogSnapshot.child(FIREBASE_CHILD_PHOTO).getValue();
                                dog.setPhoto(photo);
                                if (requestedIds.contains(dog.getId())) {
                                    dog.setFriendshipState(FriendshipState.CANCEL_STATE);
                                } else {
                                    dog.setFriendshipState(NEW_REQUEST_STATE);
                                }
                                newFriendList.add(dog);
                                emptyView.setVisibility(View.GONE);
                            }
                        }
                        Collections.sort(newFriendList, new DogComparator());
                        newFriendAdapter.notifyDataSetChanged();
                    }


                    Log.i(TAG, "onDataChange: new dog friends retrieved");
                }


                DialogHelper.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i(TAG, "onCancelled: no data retrieved " + databaseError.getMessage());
            }
        };


        newFriendAdapter = new NewFriendAdapter(this, newFriendList);
        newFriendAdapter.setDogButtonClickCallback(this);
        newFriendAdapter.setFriendshipRequestCallback(this);
        newFriendAdapter.setItemClickCallback(this);

        InitializationHelper.setupRecyclerView(this, newFriendAdapter, recyclerView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_new_friend, menu);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @OnClick(R.id.fab)
    public void showFilterOptions() {
        DialogHelper.showFilterNewFriendOptions(this, this);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        newFriendAdapter.getFilter().filter(query);
        newFriendAdapter.setGenericList(newFriendList);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newFriendAdapter.getFilter().filter(newText);
        newFriendAdapter.setGenericList(newFriendList);
        return true;
    }

    @Override
    public void clickOnButton(int pos, ActionType actionType) {
        Dog dog = newFriendList.get(pos);
        switch (actionType) {
            case BARK:
                String userId = ((FirebaseApplication) getApplication()).getFirebaseUserId();
                NotificationHelper.sendDogNotification(this, dog.getUserId(), userId, dog.getId(), getString(R.string.bark_notification),
                        getString(R.string.bark_notification_message, dogName));
                break;
            case SEND_MESSAGE:
                Intent intent = new Intent(this, ConversationActivity.class);
                intent.putExtra(ConversationUIService.USER_ID, dog.getUserId());
                startActivity(intent);
                break;
            default:
                DialogHelper.showAlertDialog(this, getString(R.string.wrong_type_action));

        }
    }

    /**
     * Sends a friendship request
     * @param dog
     */
    private void sendFriendshipRequest(Dog dog) {
        ((FirebaseApplication) getApplication()).requestNewFriendship(FIREBASE_CHILD_FRIENDSHIPS_REQUESTS, myDogId, dog.getId());
        DialogHelper.showInformationMessage(coordinatorLayout, successMessage);
        NotificationHelper.sendDogNotification(this, dog.getUserId(), ((FirebaseApplication) getApplication()).getFirebaseUserId(),
                myDogId, getString(R.string.friendship_request_title), getString(R.string.friendship_request_body, dogName));
    }


    @Override
    public void requestClickCallback(int pos, FriendshipState friendshipState) {
        Dog dog = newFriendList.get(pos);
        switch (dog.getFriendshipState()) {
            case NEW_REQUEST_STATE:
                sendFriendshipRequest(dog);
                break;
            case CANCEL_STATE:
                String whereClause = myDogId + "_" + dog.getId();
                ((FirebaseApplication) getApplication()).cancelFriendshipRequest(whereClause);
                DialogHelper.showInformationMessage(coordinatorLayout, cancelMessage);
                newFriendAdapter.notifyDataSetChanged();
                requestedIds.remove(dog);
                break;
            default:
                sendFriendshipRequest(dog);
        }
    }

    @Override
    public void onItemClick(int pos) {
        DialogHelper.showAlertDialog(this, getString(R.string.no_permission_see_dog_profile));
    }

    @Override
    public void filterByType(final FilterNewFriendEnum filterType) {
        switch (filterType) {
            case FILTER_DOG_BREED:
                newFriendAdapter.setTypeFilter(FilterNewFriendEnum.FILTER_DOG_BREED);
                break;
            case FILTER_NAME:
                newFriendAdapter.setTypeFilter(FilterNewFriendEnum.FILTER_NAME);
                break;
            case FILTER_GENDER:
                newFriendAdapter.setTypeFilter(FilterNewFriendEnum.FILTER_GENDER);
                break;
            default:
        }

    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
