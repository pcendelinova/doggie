package cz.mendelu.xcendel1.dogapp.adapters;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.xcendel1.dogapp.models.FilterNewFriendEnum;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.models.Dog;

import cz.mendelu.xcendel1.dogapp.viewholders.NewFriendViewHolder;

import static cz.mendelu.xcendel1.dogapp.models.FilterNewFriendEnum.FILTER_NAME;

/**
 * Created by admin on 26.11.2016.
 */

public class NewFriendAdapter extends BaseAdapter<Dog, NewFriendViewHolder> implements Filterable {
    private static final String DOG_MALE = "pes";
    private static final String DOG_FEMALE = "fena";

    private Interfaces.DogButtonClickCallback dogButtonClickCallback;
    private Interfaces.FriendshipRequestCallback friendshipRequestCallback;

    private List<Dog> originalList;
    private List<Dog> list;
    private FilterNewFriendEnum typeFilter;
    private Activity activity;

    public NewFriendAdapter(Activity activity, List<Dog> genericList) {
        super(genericList);
        this.activity = activity;
        this.list = genericList;
        this.typeFilter = FILTER_NAME;
    }

    @Override
    public NewFriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewFriendViewHolder(activity, parent, getItemClickCallback(), getDogButtonClickCallback(), getFriendshipRequestCallback());
    }


    public Interfaces.DogButtonClickCallback getDogButtonClickCallback() {
        return dogButtonClickCallback;
    }

    public void setDogButtonClickCallback(Interfaces.DogButtonClickCallback dogButtonClickCallback) {
        this.dogButtonClickCallback = dogButtonClickCallback;
    }

    public Interfaces.FriendshipRequestCallback getFriendshipRequestCallback() {
        return friendshipRequestCallback;
    }

    public void setFriendshipRequestCallback(Interfaces.FriendshipRequestCallback friendshipRequestCallback) {
        this.friendshipRequestCallback = friendshipRequestCallback;
    }

    public FilterNewFriendEnum getTypeFilter() {
        return typeFilter;
    }

    public void setTypeFilter(FilterNewFriendEnum typeFilter) {
        this.typeFilter = typeFilter;
    }

    /**
     * Filters by different type such as dog breed, name, gender
     * @param typeFilter
     * @param dog
     * @param constraint
     * @return
     */
    private boolean filterByType(FilterNewFriendEnum typeFilter, Dog dog, String constraint) {
        boolean contains = false;
        switch (typeFilter){
            case FILTER_DOG_BREED:
                contains = dog.getBreed().toLowerCase().contains(constraint);
                break;
            case FILTER_NAME:
                contains = dog.getName().toLowerCase().contains(constraint);
                break;
            case FILTER_GENDER:
                boolean female = DOG_FEMALE.contains(constraint);
                boolean male = DOG_MALE.contains(constraint);
                if (female && dog.isGender()) {
                    contains = true;
                }

                if (male && !dog.isGender()) {
                    contains = true;
                }

                break;
            default: contains = dog.getName().toLowerCase().contains(constraint);
        }

        return contains;

    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (List<Dog>) results.values;
                setGenericList(list);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                originalList = getGenericList();
                List<Dog> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = originalList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    protected List<Dog> getFilteredResults(String constraint) {
        List<Dog> results = new ArrayList<>();
        for (Dog item : originalList) {
            if (filterByType(getTypeFilter(), item, constraint)) {
                results.add(item);
            }
        }
        return results;
    }



}
