package cz.mendelu.xcendel1.dogapp.alarm;

/**
 * Created by admin on 16.12.2016.
 */

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.REMINDER_ID;


public class ReminderManager {

    private Context context;
    private AlarmManager alarmManager;

    public ReminderManager(Context context) {
        this.context = context;
        alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
    }

    /**
     * Sets reminder for specified time
     */
    public void setReminder(int reminderId, long when) {

        Intent notificationIntent = new Intent(context, AlarmReceiver.class);
        notificationIntent.putExtra(REMINDER_ID,  Integer.toString(reminderId));

        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, reminderId, notificationIntent, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, when, alarmIntent);

    }

    /**
     * Sets repeating alarm
     * @param reminderId
     */

    public void setRepeatingAlarm(int reminderId) {

        Intent notificationIntent = new Intent(context, AlarmReceiver.class);
        notificationIntent.putExtra(REMINDER_ID, Integer.toString(reminderId));

        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, reminderId, notificationIntent, 0);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 5 * 60 * 1000 ,
                5 * 60 * 1000, alarmIntent);

    }
}
