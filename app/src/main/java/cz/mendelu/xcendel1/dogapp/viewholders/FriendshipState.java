package cz.mendelu.xcendel1.dogapp.viewholders;

import cz.mendelu.xcendel1.dogapp.R;

/**
 * Created by admin on 29.11.2016.
 */
public enum FriendshipState {
    NEW_REQUEST_STATE,
    //decline friendship request
    DECLINE_STATE,
    //cancel its own request
    CANCEL_STATE,
    FRIENDS_STATE;

}
