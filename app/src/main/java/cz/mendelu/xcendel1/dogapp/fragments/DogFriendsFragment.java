package cz.mendelu.xcendel1.dogapp.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.activities.DogFriendProfileActivity;
import cz.mendelu.xcendel1.dogapp.activities.NewFriendActivity;
import cz.mendelu.xcendel1.dogapp.adapters.DogFriendAdapter;
import cz.mendelu.xcendel1.dogapp.comparators.DogComparator;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.helpers.InitializationHelper;
import cz.mendelu.xcendel1.dogapp.helpers.NotificationHelper;
import cz.mendelu.xcendel1.dogapp.models.ActionType;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOGS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_FRIENDSHIPS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_FRIENDS_IDS;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_NAME;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_FRIEND_PROFILE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_MY_PROFILE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_TYPE_OF_PROFILE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_DOG;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOG_FRIENDS_PATH;


/**
 * Fragment contains dog friends
 */
public class DogFriendsFragment extends Fragment implements Interfaces.ItemClickCallback, Interfaces.DogButtonClickCallback {
    private static final String TAG = DogFriendsFragment.class.getSimpleName();

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_no_friends)
    TextView emptyView;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private DogFriendAdapter dogFriendAdapter;
    private List<Dog> myFriendsList = new ArrayList<>();
    private List<String> dogFriendIds = new ArrayList<>();
    private DatabaseReference databaseReference;
    private Unbinder unbinder;
    private ValueEventListener valueEventListenerForFriends;
    private ValueEventListener valueEventListener;
    private String dogId;
    private boolean isMyProfile = true;
    View view;
    String userId;
    String dogName;


    public static final DogFriendsFragment newInstance(String id, String typeOfProfile, String dogName) {
        DogFriendsFragment fragment = new DogFriendsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_TYPE_OF_PROFILE, typeOfProfile);
        bundle.putString(PARAM_DOG_ID, id);
        bundle.putString(PARAM_DOG_NAME, dogName);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String paramType = getArguments().getString(PARAM_TYPE_OF_PROFILE);
        isMyProfile = (paramType.equals(PARAM_MY_PROFILE)) ? true : false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_dog_friends, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (getArguments().getString(PARAM_DOG_ID) != null) {
            dogId = getArguments().getString(PARAM_DOG_ID);
            dogName = getArguments().getString(PARAM_DOG_NAME);
        }

        if (!isMyProfile) {
            setupFriendProfile();
        }
        databaseReference = ((FirebaseApplication) getActivity().getApplication()).getFirebaseDatabase();
        userId = ((FirebaseApplication) getActivity().getApplication()).getFirebaseUserId();

        databaseReference.child(FIREBASE_CHILD_FRIENDSHIPS).child(dogId).addValueEventListener(createValueEventListenerDogFriendIds(view));

        dogFriendAdapter = new DogFriendAdapter(getActivity(), myFriendsList);
        dogFriendAdapter.setItemClickCallback(this);
        dogFriendAdapter.setDogButtonClickCallback(this);


        InitializationHelper.setupRecyclerView(getContext(), dogFriendAdapter, recyclerView);

        return view;

    }



    private void setupFriendProfile() {
        fab.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(int pos) {
        Dog dogFriend = myFriendsList.get(pos);
        Intent intent = new Intent(getActivity(), DogFriendProfileActivity.class);
        intent.putExtra(PARCEL_DOG, dogFriend);
        if (isMyProfile) {
            intent.putExtra(PARAM_FRIEND_PROFILE, PARAM_FRIEND_PROFILE);
        }
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (valueEventListener != null && valueEventListenerForFriends != null) {
            databaseReference.removeEventListener(valueEventListener);
            databaseReference.removeEventListener(valueEventListenerForFriends);
        }
        unbinder.unbind();
    }

    @OnClick(R.id.fab)
    public void showNewFriends() {
        Intent intent = new Intent(getActivity(), NewFriendActivity.class);
        intent.putExtra(PARAM_DOG_ID, dogId);
        intent.putStringArrayListExtra(PARAM_DOG_FRIENDS_IDS, (ArrayList<String>) dogFriendIds);
        intent.putExtra(PARAM_DOG_NAME, dogName);
        startActivity(intent);
    }

    @Override
    public void clickOnButton(int pos, ActionType actionType) {
        Dog dogFriend = myFriendsList.get(pos);
        switch (actionType) {
            case BARK:
                //TODO sound woof woof
                NotificationHelper.sendDogNotification(getActivity(), dogFriend.getUserId(), userId, dogId, getString(R.string.bark_notification),
                        getString(R.string.bark_notification_message, dogName));
                DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.bark_sent));

                break;
            case INVITE_TO_WALK:
                NotificationHelper.sendDogNotification(getActivity(), dogFriend.getUserId(), userId, dogId, getString(R.string.walk_invitation), getString(R.string.walk_invitation_message));
                DialogHelper.showInformationMessage(coordinatorLayout, getString(R.string.invitation_sent));
                break;
            case SEND_MESSAGE:
                Intent intent = new Intent(getActivity(), ConversationActivity.class);
                intent.putExtra(ConversationUIService.USER_ID, dogFriend.getUserId());
                startActivity(intent);
                break;
            default:
                DialogHelper.showAlertDialog(getContext(), getString(R.string.wrong_type_action));

        }
    }

    public List<String> getDogFriendIds() {
        return dogFriendIds;
    }

    private ValueEventListener createValueEventListenerDogFriendIds(final View view) {
        DialogHelper.showProgressDialog(getContext(), getString(R.string.loading_data));
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChildren()) {
                    if (emptyView == null) {
                        emptyView = (TextView) view.findViewById(R.id.tv_no_friends);
                    }
                    emptyView.setVisibility(View.VISIBLE);
                    DialogHelper.hideProgressDialog();
                } else {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        dogFriendIds.add(postSnapshot.getKey());
                    }
                    databaseReference.child(FIREBASE_CHILD_DOGS).addListenerForSingleValueEvent(createValueEventListenerForDogFriends(view));

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        return valueEventListener;
    }

    private ValueEventListener createValueEventListenerForDogFriends(final View view) {
        valueEventListenerForFriends = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                myFriendsList.clear();
                if (dogFriendIds.isEmpty() || !dataSnapshot.hasChildren()) {
                    if (emptyView == null) {
                        emptyView = (TextView) view.findViewById(R.id.tv_no_friends);
                    }
                    emptyView.setVisibility(View.VISIBLE);
                } else {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        for (DataSnapshot dogSnapshot : postSnapshot.getChildren()) {
                            Dog dogFriend = dogSnapshot.getValue(Dog.class);
                            if (!dogFriendIds.isEmpty() && dogFriendIds.contains(dogFriend.getId()) && !dogFriend.getId().equals(dogId)) {
                                myFriendsList.add(dogFriend);
                                String imagePath = dogFriend.getUserId() + STORAGE_IMAGES_DOG_FRIENDS_PATH + dogFriend.getId();
                                if (!ImageUtils.checkIfImageExits(getActivity(), imagePath)) {
                                    String photo = (String) dogSnapshot.child(FIREBASE_CHILD_PHOTO).getValue();
                                    dogFriend.setPhoto(photo);
                                    ImageUtils.saveImageToExternalStorage(getActivity(), ImageUtils.decodeBitmap(dogFriend.getPhoto()),
                                            STORAGE_IMAGES_DOG_FRIENDS_PATH, dogFriend.getId() + ".jpg");

                                }
                            }
                        }
                        if (emptyView == null) {
                            emptyView = (TextView) view.findViewById(R.id.tv_no_friends);
                        }
                        emptyView.setVisibility(View.GONE);
                        Collections.sort(myFriendsList, new DogComparator());
                    }
                    dogFriendAdapter.notifyDataSetChanged();
                }

                DialogHelper.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        return valueEventListenerForFriends;
    }

    public void setDogFriendIds(List<String> dogFriendIds) {
        this.dogFriendIds = dogFriendIds;
    }

    public ValueEventListener getValueEventListenerForFriends() {
        return valueEventListenerForFriends;
    }

    public void setValueEventListenerForFriends(ValueEventListener valueEventListenerForFriends) {
        this.valueEventListenerForFriends = valueEventListenerForFriends;
    }


}
