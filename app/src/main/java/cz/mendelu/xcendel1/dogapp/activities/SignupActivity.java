package cz.mendelu.xcendel1.dogapp.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.exception.ConversionException;
import com.mvc.imagepicker.ImagePicker;

import java.util.Arrays;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import cz.mendelu.xcendel1.dogapp.utils.ModelUtils;
import cz.mendelu.xcendel1.dogapp.utils.MyTextWatcherUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cz.mendelu.xcendel1.dogapp.constants.SettingsConstants.PROFILE_PHOTO_SIZE;

/**
 * A screen that offers sign up to the app
 */
public class SignupActivity extends AppCompatActivity implements Validator.ValidationListener, View.OnTouchListener {

    private static final String TAG = SignupActivity.class.getSimpleName();
    private static final int REQUEST_IMAGE_CAPTURE = 111;
    private FirebaseAuth auth;
    private Validator validator;
    private List<TextInputLayout> inputLayoutList;
    Bitmap bitmap;

    @NotEmpty(messageResId = R.string.error_required_field)
    @BindView(R.id.input_layout_name) TextInputLayout inputLayoutName;

    @NotEmpty(messageResId = R.string.error_required_field)
    @Email(messageResId = R.string.error_invalid_email)
    @BindView(R.id.input_layout_email) TextInputLayout inputLayoutEmail;

    @NotEmpty(messageResId = R.string.error_required_field)
    @Password(min = 6, messageResId = R.string.error_invalid_password)
    @BindView(R.id.input_layout_password) TextInputLayout inputLayoutPassword;

    @NotEmpty(messageResId = R.string.error_required_field)
    @ConfirmPassword(messageResId = R.string.error_not_match_password)
    @BindView(R.id.input_layout_confirmed_password) TextInputLayout inputLayoutConfirmedPassword;

    @BindView(R.id.dog_profile_photo) CircleImageView photo;
    @BindView(R.id.btn_signup)
    AppCompatButton signUpButton;

    @BindString(R.string.choose_photo) String imagePickerMessage;
    @BindString(R.string.add_photo) String addPhotoTitle;

    private String profilePhoto = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        ButterKnife.bind(this);
        auth = ((FirebaseApplication) getApplication()).getFirebaseAuth();

        ((FirebaseApplication) getApplication()).checkUserLogin(SignupActivity.this);

        ImagePicker.setMinQuality(PROFILE_PHOTO_SIZE, PROFILE_PHOTO_SIZE);

        validator = new Validator(this);
        validator.setValidationListener(this);

        validator.registerAdapter(TextInputLayout.class,
                new ViewDataAdapter<TextInputLayout, String>() {
                    @Override
                    public String getData(TextInputLayout flet) throws ConversionException {
                        return flet.getEditText().getText().toString();
                    }
                }
        );

        inputLayoutList = Arrays.asList(inputLayoutName, inputLayoutEmail, inputLayoutPassword, inputLayoutConfirmedPassword);

        MyTextWatcherUtils.setTextWatcherListeners(inputLayoutList, validator);

        signUpButton.setEnabled(false);

        if (shouldAskPermissions()) {
            askPermissions();
        }

    }

    @OnClick(R.id.btn_signup)
    public void submitForm() {
        Log.d(TAG, "Submit sign up form");
        ((FirebaseApplication) getApplication()).createNewUser(SignupActivity.this, ModelUtils.getProperty(inputLayoutEmail),
                ModelUtils.getProperty(inputLayoutPassword), ModelUtils.getProperty(inputLayoutName), profilePhoto);

    }

    @OnClick(R.id.dog_profile_photo)
    public void addUserProfilePhoto() {
        ImagePicker.pickImage(this, imagePickerMessage);
    }

    @Override
    public void onValidationSucceeded() {
        signUpButton.setEnabled(true);
        signUpButton.setAlpha(1);
        MyTextWatcherUtils.clearAllInputs(inputLayoutList);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        MyTextWatcherUtils.clearAllInputs(inputLayoutList);
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof TextInputLayout) {
                ((TextInputLayout) view).setErrorEnabled(true);
                ((TextInputLayout) view).setError(message);
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
        if (bitmap != null) {
            photo.setImageBitmap(bitmap);
            profilePhoto = ImageUtils.encodeBitmap(bitmap);
        }

    }

    protected boolean shouldAskPermissions() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(23)
    protected void askPermissions() {
        String[] permissions = {
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE"
        };
        int requestCode = 200;
        requestPermissions(permissions, requestCode);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (view.getId() == R.id.dog_profile_photo) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                view.setAlpha(.5f);
            } else {
                view.setAlpha(1f);
            }
            return false;
        }

        return true;
    }

    @OnClick(R.id.btn_back)
    public void goBack() {
        onBackPressed();
    }




}
