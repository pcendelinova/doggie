package cz.mendelu.xcendel1.dogapp.constants;

/**
 * Created by Petra Cendelinova on 10/29/2016.
 */

public class ParcelConstants {
    public static final String PARCEL_REMINDER = "reminder";
    public static final String PARCEL_DOG = "dog";
    public static final String PARCEL_DOG_BREED = "dogBreed";
    public static final String PARCEL_MEMORY = "memory";

    public static final String DOG_BREED_TITLE = "dogBreedTitle";
    public static final String DOG_BREED_CONTENT = "dogBreedContent";

    public static final String PARAM_FAVOURITE = "favourite";
    public static final String PARAM_EDIT = "paramEdit";
    public static final String PARAM_DOG_ID = "dogId";
    public static final String PARAM_TYPE_OF_PROFILE = "typeOfProfile";
    public static final String PARAM_MY_PROFILE = "myProfile";
    public static final String PARAM_FRIEND_PROFILE = "friendProfile";
    public static final String PARAM_DOG_NAME = "dogName";
    public static final String PARAM_USER_ID = "userId";

    public static final String PARAM_DOG_FRIENDS_IDS = "friendIds";
    public static final String PARAM_MODEL_ID = "modelId";
    public static final String PARAM_REMINDER = "reminder";

    public static final String CURRENT_FRAGMENT = "currentFragmet";
    public static final String REMINDER_ID = "notificationId";


}
