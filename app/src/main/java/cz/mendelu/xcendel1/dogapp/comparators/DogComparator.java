package cz.mendelu.xcendel1.dogapp.comparators;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

import cz.mendelu.xcendel1.dogapp.models.Dog;

/**
 * Created by admin on 10.12.2016.
 *
 * Comparator for dogs including czech letters
 */

public class DogComparator implements Comparator<Dog> {
    @Override
    public int compare(Dog dog, Dog dog2) {
        Collator collator = Collator.getInstance(new Locale("cs"));
        collator.setStrength(Collator.PRIMARY);
        return collator.compare(dog.getName(), dog2.getName());
    }
}
