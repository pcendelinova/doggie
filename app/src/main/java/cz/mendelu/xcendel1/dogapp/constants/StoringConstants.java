package cz.mendelu.xcendel1.dogapp.constants;

/**
 * Created by admin on 12.12.2016.
 *
 * Storing constants for shared preferences and external storage
 */

public class StoringConstants {
    public static final String SHARED_PREFERENCES = "sharedPreferences";
    public static final String SHARED_PREFERENCES_NAME = "name";
    public static final String SHARED_PREFERENCES_EMAIL = "email";
    public static final String SHARED_PREFERENCES_DOG_FRIENDS = "dogFriendsOf";
    public static final String SHARED_PREFERENCES_PHOTO_LINK = "photoLink";
    public static final String SHARED_PREFERENCES_MY_DOGS = "myDogs";
    public static final String STORAGE_IMAGES_PATH = "/images/";
    public static final String STORAGE_IMAGES_MEMORIES_PATH = "/images/memories/";
    public static final String STORAGE_IMAGES_DOGS_PATH = "/images/mydogs/";
    public static final String STORAGE_IMAGES_DOG_FRIENDS_PATH = "/images/friends/";
    public static final String STORAGE_MY_PROFILE_PHOTO = "profilePhoto.jpg";
    public static final String UPDATED_PHOTO = "updatedPhoto";
    public static final String UPDATED_DOG = "updatedDog";
    public static final String UPDATED_MEMORY = "updatedMemory";
    public static final String SET_DOG_PHOTO = "dogPhoto";
    public static final String SET_MEMORY_PHOTO = "memoryPhoto";


}
