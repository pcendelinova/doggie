package cz.mendelu.xcendel1.dogapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RadioGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.mobsandgeeks.saripaar.annotation.Checked;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Optional;
import com.mobsandgeeks.saripaar.annotation.Pattern;
import com.mobsandgeeks.saripaar.exception.ConversionException;
import com.mvc.imagepicker.ImagePicker;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.constants.SettingsConstants;
import cz.mendelu.xcendel1.dogapp.annotation.PastDate;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.models.Dog;
import cz.mendelu.xcendel1.dogapp.models.ModelFactory;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;
import cz.mendelu.xcendel1.dogapp.utils.MyTextWatcherUtils;
import cz.mendelu.xcendel1.dogapp.utils.UserUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOGS;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_DOG_BREEDS;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_EDIT;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_DOG;
import static cz.mendelu.xcendel1.dogapp.constants.SettingsConstants.PROFILE_PHOTO_SIZE;
import static cz.mendelu.xcendel1.dogapp.constants.SettingsConstants.SPLASH_DISPLAY_LENGTH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SET_DOG_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.SHARED_PREFERENCES_MY_DOGS;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_DOGS_PATH;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.UPDATED_DOG;

/**
 * A screen that creates a new user's dog
 */
public class NewDogActivity extends AppCompatActivity implements Validator.ValidationListener, View.OnTouchListener {

    private static final String TAG = NewDogActivity.class.getSimpleName();

    @NotEmpty(messageResId = R.string.error_required_field)
    @BindView(R.id.input_layout_name)
    TextInputLayout inputLayoutName;

    @BindView(R.id.input_breed)
    AutoCompleteTextView inputBreed;

    @Optional
    @Pattern(regex = "^\\s*(?=.*[1-9])\\d*(?:.\\d{1,2})?\\s*", messageResId = R.string.weight_must_be_digit)
    @BindView(R.id.input_layout_weight)
    TextInputLayout inputLayoutWeight;

    @BindView(R.id.input_layout_hobbies)
    TextInputLayout inputLayoutHobbies;
    @BindView(R.id.input_layout_status)
    TextInputLayout inputLayoutStatus;

    @Optional
    @PastDate(messageResId = R.string.birthday_must_be_in_past, dateFormat = SettingsConstants.DATE_FORMAT_DMY)
    @BindView(R.id.input_layout_birthday)
    TextInputLayout inputLayoutBirthday;

    @BindView(R.id.input_layout_gender)
    TextInputLayout inpuLayoutGender;

    @BindView(R.id.dog_profile_photo)
    CircleImageView dogPhoto;

    @Checked(messageResId = R.string.gender_is_mandatory)
    @BindView(R.id.radio_gender)
    RadioGroup inputGender;

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.btn_add_dog)
    AppCompatButton addButton;

    @BindString(R.string.success_save_dog)
    String successSave;
    @BindString(R.string.success_edit_dog)
    String successEdit;
    @BindString(R.string.image_picker)
    String imagePickerMessage;
    @BindString(R.string.form_has_erros)
    String errorMessage;

    private DatabaseReference databaseReference;
    private ArrayAdapter<String> adapter;
    private String imageResource;
    private List<TextInputLayout> inputLayoutList;
    private Validator validator;
    boolean edit = false;
    private Dog dog;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_dog);

        ButterKnife.bind(this);

        databaseReference = ((FirebaseApplication) getApplication()).getFirebaseDatabase();

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        databaseReference.child(FIREBASE_CHILD_DOG_BREEDS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String name = postSnapshot.child("name").getValue(String.class);
                    adapter.add(name);
                }
                adapter.notifyDataSetChanged();
                inputBreed.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "onCancelled", databaseError.toException());
            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);

        validator.registerAdapter(TextInputLayout.class,
                new ViewDataAdapter<TextInputLayout, String>() {
                    @Override
                    public String getData(TextInputLayout flet) throws ConversionException {
                        return flet.getEditText().getText().toString();
                    }
                }
        );

        Validator.registerAnnotation(PastDate.class);

        inputLayoutList = Arrays.asList(inputLayoutName, inputLayoutWeight, inputLayoutBirthday);

        MyTextWatcherUtils.setTextWatcherListeners(inputLayoutList, validator);

        ImagePicker.setMinQuality(PROFILE_PHOTO_SIZE, PROFILE_PHOTO_SIZE);

        addButton.setAlpha(.5f);
        addButton.setEnabled(false);

        inputGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                validator.validate();
            }
        });

        if (getIntent().hasExtra(PARAM_EDIT) && getIntent().hasExtra(PARCEL_DOG)) {
            Bundle bundle = getIntent().getExtras();
            dog = bundle.getParcelable(PARCEL_DOG);
            prepareData(dog);
            edit = true;
            setTitle(getString(R.string.edit_dog));
        }

        dogPhoto.setOnTouchListener(this);
        addButton.setOnTouchListener(this);

    }

    @OnClick(R.id.dog_profile_photo)
    public void pickImage() {
        ImagePicker.pickImage(NewDogActivity.this, imagePickerMessage);
    }

    @OnFocusChange({R.id.input_layout_birthday, R.id.input_birthday})
    public void showCalendar() {
        DialogHelper.showDatePicker(NewDogActivity.this, inputLayoutBirthday);
    }

    @OnClick({R.id.input_layout_birthday, R.id.input_birthday})
    public void showCalendarOnClick() {
        DialogHelper.showDatePicker(NewDogActivity.this, inputLayoutBirthday);
    }



    @OnClick(R.id.btn_add_dog)
    public void saveDog() {
        String dogId = null;
        Dog createdDog = ModelFactory.createDog(null, inputLayoutName, inputLayoutBirthday, inputBreed, inputLayoutHobbies, inputGender,
                inputLayoutWeight, imageResource, inputLayoutStatus);
        if (edit) {
            createdDog.setId(dog.getId());
            createdDog.setUpdatedPhoto(true);
            createdDog.setUserId(dog.getUserId());
            ((FirebaseApplication) getApplication()).editExistingRecord(createdDog, FIREBASE_CHILD_DOGS);
            DialogHelper.showInformationMessage(coordinatorLayout, successEdit);
            dogId = dog.getId();
            setTitle(getString(R.string.edit_dog));
            dog = createdDog;
        } else {
            createdDog.setUserId(((FirebaseApplication) getApplication()).getFirebaseUserId());
            dogId = ((FirebaseApplication) getApplication()).saveNewRecord(createdDog, FIREBASE_CHILD_DOGS);
            DialogHelper.showInformationMessage(coordinatorLayout, successSave);
            Set<String> myDogIds = UserUtils.getMyDogs(this, createdDog.getUserId());
            myDogIds.add(dogId);

            UserUtils.saveMyDogs(this, createdDog.getUserId(), myDogIds);

        }

        if (bitmap != null && dogId != null) {
            String imageName = dogId + ".jpg";
            ImageUtils.saveImageToExternalStorage(this, bitmap, STORAGE_IMAGES_DOGS_PATH, imageName);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent profileIntent = new Intent(NewDogActivity.this, MainActivity.class);
                startActivity(profileIntent);

            }
        }, SPLASH_DISPLAY_LENGTH);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
        if (bitmap != null) {
            dogPhoto.setImageBitmap(bitmap);
            imageResource = ImageUtils.encodeBitmap(bitmap);
        }

    }

    @Override
    public void onValidationSucceeded() {
        inpuLayoutGender.setError(null);
        addButton.setEnabled(true);
        addButton.setAlpha(1);
        MyTextWatcherUtils.clearAllInputs(inputLayoutList);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        inpuLayoutGender.setError(null);
        MyTextWatcherUtils.clearAllInputs(inputLayoutList);
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof TextInputLayout) {
                ((TextInputLayout) view).setErrorEnabled(true);
                ((TextInputLayout) view).setError(message);
            } else if (view instanceof RadioGroup) {
                inpuLayoutGender.setEnabled(true);
                inpuLayoutGender.setError(message);
            }
        }
    }


    private void prepareData(Dog myDog) {
        inputLayoutName.getEditText().setText(myDog.getName());
        inputLayoutBirthday.getEditText().setText(myDog.getBirthday());
        inputLayoutHobbies.getEditText().setText(myDog.getHobbies());
        inputLayoutWeight.getEditText().setText(myDog.getWeight());
        inputBreed.setText(myDog.getBreed());

        String imagePath = myDog.getUserId() + STORAGE_IMAGES_DOGS_PATH + myDog.getId();
        if (myDog.isUpdatedPhoto()) {
            ImageUtils.setImageView(NewDogActivity.this, imagePath, myDog.getPhoto(), dogPhoto, true, SET_DOG_PHOTO);
        } else {
            ImageUtils.setImageView(NewDogActivity.this, imagePath, myDog.getPhoto(), dogPhoto, false, SET_DOG_PHOTO);
        }

        if (myDog.isGender()) {
            AppCompatRadioButton female = (AppCompatRadioButton) findViewById(R.id.radio_female);
            female.setChecked(true);
        } else {
            AppCompatRadioButton male = (AppCompatRadioButton) findViewById(R.id.radio_male);
            male.setChecked(true);
        }

        addButton.setText(getString(R.string.edit_dog));
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home && edit) {
            Intent intent = getIntent();
            intent.putExtra(UPDATED_DOG, dog);
            setResult(Activity.RESULT_OK, intent);
            finish();

            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (view.getId() == R.id.dog_profile_photo) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                view.setAlpha(.5f);
            } else {
                view.setAlpha(1f);
            }
            return false;
        } else if (view.getId() == R.id.btn_add_dog) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                view.setAlpha(.5f);
            } else {
                view.setAlpha(1f);
            }
            return false;
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }


}
