package cz.mendelu.xcendel1.dogapp.viewholders;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.models.Reminder;
import cz.mendelu.xcendel1.dogapp.models.ReminderCategoryEnum;
import cz.mendelu.xcendel1.dogapp.utils.DateUtils;

/**
 * Created by admin on 14.11.2016.
 */

public class ReminderViewHolder extends BaseViewHolder<Reminder> {
    @BindView(R.id.reminder_name) TextView reminderName;
    @BindView(R.id.icon) ImageView reminderIcon;
    @BindView(R.id.reminder_date) TextView reminderDate;
    @BindView(R.id.cont_item_root) View container;
    @BindView(R.id.reminder_checkbox) CheckBox reminderCheckbox;
    Interfaces.ReminderCheckBoxCallBack reminderCheckBoxCallBack;

    public ReminderViewHolder(ViewGroup parent, Interfaces.ItemClickCallback itemClickCallback, Interfaces.ReminderCheckBoxCallBack reminderCheckBoxCallBack) {
        super(parent, R.layout.reminder_list_row, itemClickCallback);
        this.reminderCheckBoxCallBack = reminderCheckBoxCallBack;
    }

    @Override
    protected void onBind(Reminder item, int position) {
        reminderName.setText(item.getName());
        int resourceImage = ReminderCategoryEnum.getReminderResource(item.getCategory(), true);
        reminderIcon.setImageResource(resourceImage);
        reminderDate.setText(DateUtils.createDateStringForReminder(item.getDate(), item.getTime()));
        reminderCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reminderCheckBoxCallBack.addCheckBox(getAdapterPosition());
            }
        });

    }

}
