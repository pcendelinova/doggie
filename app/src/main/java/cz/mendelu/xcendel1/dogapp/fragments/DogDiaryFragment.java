package cz.mendelu.xcendel1.dogapp.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.mendelu.xcendel1.dogapp.FirebaseApplication;
import cz.mendelu.xcendel1.dogapp.Interfaces;
import cz.mendelu.xcendel1.dogapp.R;
import cz.mendelu.xcendel1.dogapp.activities.MemoryDetailActivity;
import cz.mendelu.xcendel1.dogapp.activities.NewMemoryActivity;
import cz.mendelu.xcendel1.dogapp.adapters.MemoryAdapter;
import cz.mendelu.xcendel1.dogapp.comparators.MemoryComparator;
import cz.mendelu.xcendel1.dogapp.helpers.DialogHelper;
import cz.mendelu.xcendel1.dogapp.models.Memory;
import cz.mendelu.xcendel1.dogapp.utils.ImageUtils;

import static com.facebook.FacebookSdk.getApplicationContext;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_PHOTO;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_DOG_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_MY_PROFILE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_TYPE_OF_PROFILE;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARAM_USER_ID;
import static cz.mendelu.xcendel1.dogapp.constants.ParcelConstants.PARCEL_MEMORY;
import static cz.mendelu.xcendel1.dogapp.constants.FirebaseConstants.FIREBASE_CHILD_MEMORIES;
import static cz.mendelu.xcendel1.dogapp.constants.StoringConstants.STORAGE_IMAGES_MEMORIES_PATH;

/**
 * Fragment for memories of specified dg
 */
public class DogDiaryFragment extends Fragment implements Interfaces.ItemClickCallback {

    private static final String TAG = DogDiaryFragment.class.getSimpleName();
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_no_memories)
    TextView emptyView;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private MemoryAdapter memoryAdapter;
    private List<Memory> memoryList = new ArrayList<>();
    private DatabaseReference databaseReference;
    private Unbinder unbinder;
    private ValueEventListener valueEventListener;
    private String dogId;
    private String userId;
    private boolean isMyProfile = true;

    public static final DogDiaryFragment newInstance(String dogId, String userId, String typeOfProfile) {
        DogDiaryFragment f = new DogDiaryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_TYPE_OF_PROFILE, typeOfProfile);
        bundle.putString(PARAM_DOG_ID, dogId);
        bundle.putString(PARAM_USER_ID, userId );
        f.setArguments(bundle);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String paramType = getArguments().getString(PARAM_TYPE_OF_PROFILE);
        isMyProfile = (paramType.equals(PARAM_MY_PROFILE)) ? true : false;
        userId =  getArguments().getString(PARAM_USER_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dog_diary, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (getArguments().getString(PARAM_DOG_ID) != null) {
            dogId = getArguments().getString(PARAM_DOG_ID);
        }

        emptyView = (TextView) view.findViewById(R.id.tv_no_memories);

        if (!isMyProfile) {
            fab.setVisibility(View.GONE);
        }

        databaseReference = ((FirebaseApplication) getActivity().getApplication()).getFirebaseDatabase();
//        DialogHelper.showProgressDialog(getContext(), getString(R.string.loading_data) );
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                memoryList.clear();
                if (!dataSnapshot.hasChildren()) {
                    if (emptyView == null) {
                        emptyView = (TextView) view.findViewById(R.id.tv_no_memories);
                    }
                    memoryList.clear();
                    memoryAdapter.notifyDataSetChanged();
                    emptyView.setVisibility(View.VISIBLE);
                } else {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Memory memory = postSnapshot.getValue(Memory.class);
                        String imagePath = userId + STORAGE_IMAGES_MEMORIES_PATH + memory.getId();
                        if(!ImageUtils.checkIfImageExits(getContext(), imagePath)) {
                            //it downloads a photo only when it is not stored locally
                            String photo = (String) postSnapshot.child(FIREBASE_CHILD_PHOTO).getValue();
                            memory.setPhoto(photo);
                            ImageUtils.saveImageToExternalStorage(getActivity(), ImageUtils.decodeBitmap(memory.getPhoto()),
                                    STORAGE_IMAGES_MEMORIES_PATH, memory.getId() + ".jpg");
                        }

                        memoryList.add(memory);
                    }
                    if (emptyView == null) {
                        emptyView = (TextView) view.findViewById(R.id.tv_no_memories);
                    }
                    emptyView.setVisibility(View.GONE);
                    memoryAdapter.notifyDataSetChanged();
                    Collections.sort(memoryList, new MemoryComparator());
                }

                Log.i(TAG, "onDataChange: new dog friends retrieved");

//                DialogHelper.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        databaseReference.child(FIREBASE_CHILD_MEMORIES).child(userId).child(dogId).addValueEventListener(valueEventListener);

        memoryAdapter = new MemoryAdapter(getActivity(), memoryList);
        memoryAdapter.setItemClickCallback(this);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(memoryAdapter);


        return view;
    }

    @Override
    public void onItemClick(int pos) {
        Memory memory = memoryList.get(pos);
        Intent intent = new Intent(getActivity(), MemoryDetailActivity.class);
        intent.putExtra(PARCEL_MEMORY, memory);
        intent.putExtra(PARAM_DOG_ID, dogId);
        startActivity(intent);
    }

    @OnClick(R.id.fab)
    public void addNewMemory() {
        Intent intent = new Intent(getActivity(), NewMemoryActivity.class);
        intent.putExtra(PARAM_DOG_ID, dogId);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (valueEventListener != null) {
            databaseReference.removeEventListener(valueEventListener);
        }
        unbinder.unbind();

    }

    public List<String> getDogMemoriesIds() {
        List<String> memoriesIds = new ArrayList<>(memoryList.size());
        for (Memory memory : memoryList) {
            memoriesIds.add(memory.getId());
        }

        return memoriesIds;
    }
}
